package com.tykhe.client.jackpot

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.{Label, Skin}
import com.tykhe.client.components.meter.GenerateFont
import com.typesafe.config.Config
import org.tykhe.slot.paytable.Win

import scala.collection.mutable

case class Jackpots(_id: String, _name: String, _accumulated: Float)

case class JackpotLabels(prop: (String, JackpotLabel))

case class JackpotLabel(prop: (Config, Skin)) {
  val label = new Label(prop._1.getString("value"), prop._2)
  val labelStyle = GenerateFont.fontStyle(prop._1.getString("fontName"), prop._1.getInt("fontSize"), Color.WHITE)
  label.setStyle(labelStyle)
  label.setTouchable(Touchable.disabled)
  label.setBounds(prop._1.getInt("x"), prop._1.getInt("y"), prop._1.getInt("width"), prop._1.getInt("height"))
}

object LoadJP {
  var maximumDenom = 0

  def _load(configs: Config): Array[Jackpots] = {
    val cfgList = configs.getConfigList("jackpots")
    maximumDenom = configs.getInt("maximumDenom")
    var jp = new Array[Jackpots](cfgList.size())
    for (i <- 0 to cfgList.size() - 1) {
      val cfg = cfgList.get(i)
      jp(i) = Jackpots(cfg.getString("_id"), cfg.getString("_name"), cfg.getInt("_accumulated").toFloat / maximumDenom)
    }
    jp
  }
}

object TotalBet {
  var denom = 10
  var coin = 1
  var line = 1

  def setBet(_denom: Int, _coin: Int, _line: Int) = {
    denom = _denom
    coin = _coin
    line = _line
  }

  def total = denom * coin * line
}

object Jackpots {
  var progrssiveJackpots = new mutable.HashMap[String, Double]()
  var defaultJPvalue = Array.empty[Win]
  var jps = Array.empty[Jackpots]

  def loadPJP() = {
    defaultJPvalue.foreach { z =>
      progrssiveJackpots.update(z.name, z.amount)
    }
  }

  def update() = jps.foreach { z =>
    val amountDec = progrssiveJackpots.apply(z._name) + (z._accumulated + (TotalBet.total * 1 / LoadJP.maximumDenom))
    progrssiveJackpots(z._name) = "%2f".format(amountDec).toDouble
  }

  def reset(name: String) = progrssiveJackpots(name) = defaultJPvalue.filter(_.name == name).head.getAmount

  def getAmount(name: String): Float = progrssiveJackpots.find(_._1 == name) match {
    case Some(x) => "%2f".format(x._2).toFloat
    case None => 0.0f
  }

}
