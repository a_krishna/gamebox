package com.tykhe.client.debug

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.{Batch, BitmapFont}
import com.badlogic.gdx.graphics.profiling.GLProfiler
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.typesafe.config.Config

class DebugLayer(config: Config) extends Actor {
  var font: BitmapFont = new BitmapFont()
  var chars: Array[Char] = new Array[Char](100)
  var (xcoord, ycoord): (Float, Float) = (0f, 0f)
  var builder: StringBuilder = new StringBuilder()
  setPosition(config.getLong("x"), config.getLong("y"))
  //  setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
  println("Debug LAyer===" + config.getBoolean("flag"))
  addListener(new ClickListener() {
    override def clicked(event: InputEvent, newx: Float, newy: Float): Unit = {
      xcoord = newx
      ycoord = newy
    }
  })
  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    if (config.getBoolean("flag")) {
      builder.setLength(0);
      builder.append("fps:");
      builder.append(Gdx.graphics.getFramesPerSecond());
      builder.append("\njheap:");
      builder.append(Gdx.app.getJavaHeap());
      builder.append(s"\n(x, y): ($xcoord, $ycoord)");
      builder.append(s"\nVertexCount: ${GLProfiler.vertexCount.count}")
      builder.append(s"\nShaders Switches: ${GLProfiler.shaderSwitches}")
      builder.append(s"\nDrawCalls: ${GLProfiler.drawCalls}")
      builder.append(s"\nTextureBinding: ${GLProfiler.textureBindings}")
      builder.append(s"\nCalls: ${GLProfiler.calls}")
//      font.drawMultiLine(batch, builder, getX(), getY() + Gdx.graphics.getHeight());
      GLProfiler.reset()
    }
  }
}