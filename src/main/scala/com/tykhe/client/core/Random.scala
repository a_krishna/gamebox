package com.tykhe.client.core

import tykhe.random.Random

sealed  trait RequestRandom
case class DrawANumber(value:Int) extends RequestRandom
case class Shuffle(value:Seq[Any]) extends RequestRandom
case class PrepareStops(value:Array[Int]) extends RequestRandom

trait ResponseRandom
case class NumberDrawn(value:Int) extends ResponseRandom
case class Shuffled(value:Seq[Any]) extends ResponseRandom
case class StopsPrepared(value:Array[Int]) extends ResponseRandom


object ProcessRandom {
  private  val random=Random()
  def generateRandom(requestRandom: RequestRandom):ResponseRandom = requestRandom match {
    case DrawANumber(value) =>NumberDrawn(random.rng.bounded(value))
    case Shuffle(group) =>  Shuffled(random.shuffled(group)(0,group.length))
    case PrepareStops(bound)=> StopsPrepared( bound.foldLeft(Array.empty[Int])((z,a)=> z ++ Array(random.rng.bounded(a))) )
  }

  def reelStop(value:Array[Int]):Array[String]=generateRandom(PrepareStops(value)) match {
    case StopsPrepared(s)=> s.foldLeft(Array.empty[String])((z,a)=> z ++ Array((a).toString))
  }
}
