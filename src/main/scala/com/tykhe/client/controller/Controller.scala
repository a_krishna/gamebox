package com.tykhe.client.controller

import akka.actor.{Props, ActorLogging, Actor}
import com.badlogic.gdx.Application
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.scenes.scene2d.{Group, Event, EventListener}
import com.tykhe.client.components.GAMESTATE
import com.tykhe.client.components.buttons.{ButtonHandler,  ButtonDown}
import com.tykhe.client.device.DeviceExtension
import com.tykhe.client.http.NemesisController
import com.tykhe.client.main._
import com.tykhe.client.model.Model
import com.tykhe.client.poker.components.{PokerMain, PokerApp, PokerGame}
import com.tykhe.client.poker.components.button.ButtonLayout
import com.tykhe.client.slot.APPCARDSTATE.{AUTHENTICATION, AUTHENTICATED, INSERTCARD}
import com.tykhe.client.slot.{APPSTATE, SlotMain, SlotApp, SlotGame}
import com.tykhe.client.utils._
import com.typesafe.config.ConfigFactory

class Controller(game:String) extends Actor with ActorLogging{
  val nemesisController=context.actorOf(Props[NemesisController],"nemesis-controller")
  val deviceExtension=context.actorOf(Props[DeviceExtension],"device-extension")
  val terminalID=context.system.settings.config.getString("nemesis.terminal.prop.game-id")
  val lwjglCfg= LwjglConf(context.system.settings.config.getInt("nemesis.terminal.prop.game-width"),context.system.settings.config.getInt("nemesis.terminal.prop.game-height"),context.system.settings.config.getBoolean("nemesis.terminal.prop.game-fullscreen"))
  val cursorState=context.system.settings.config.getBoolean("nemesis.terminal.prop.game-cursor")
  var uiApplication:Option[Application]= None
  val application= game match {
    case "SLOT"=>new SlotApp(SlotMain.appConfig.getConfig("slot"),this)
    case "POKER"=>new PokerApp(PokerMain.appConfig.getConfig("poker"),this)
    case "HOME_FRUIT"=>new MainMenu(
      Game.pokerConfig.getConfig("poker"),
      Seq( (SlotGameVariant.FRUIT,ConfigFactory.load("fruit_slot").getConfig("slot"))),
      this,context.system.settings.config.getString("nemesis.terminal.prop.game-viewport"))
    case "HOME_CASENO"=>new MainMenu(
      Game.pokerConfig.getConfig("poker"),
      Seq( (SlotGameVariant.CASENO,ConfigFactory.load("caseno_slot").getConfig("slot"))),
      this,context.system.settings.config.getString("nemesis.terminal.prop.game-viewport"))
    case  _=>new MainMenu(
      Game.pokerConfig.getConfig("poker"),
      Seq( (SlotGameVariant.FRUIT,ConfigFactory.load("fruit_slot").getConfig("slot")),(SlotGameVariant.CASENO,ConfigFactory.load("caseno_slot").getConfig("slot"))),
      this,context.system.settings.config.getString("nemesis.terminal.prop.game-viewport"))
  }
  val numM="""[0-9]""".r
  var slotGameS:Array[SlotGame]=null
  var slotGame:SlotGame=null
  var pokerGame:PokerGame=null
  var controllerFlag=false
  var state:GAMESTATE=GAMESTATE.HOME

   def slotGameHandler()=slotGameS.foreach{sG=>
     sG.addCaptureListener(new EventListener {
     override def handle(event: Event): Boolean = event match {
       case GameStateChange(gameState)=>
            gameState match {
              case GAMESTATE.HOME=> sG.removeCaptureListener(CardLayoutCaptureListener)
                sG.removeActor(sG.cardLayout)
              case GAMESTATE.SLOTGAME=>CardLayoutState.state match {
                case INSERTCARD=>sG.addActor(sG.cardLayout)
                  sG.addCaptureListener(CardLayoutCaptureListener)
                case _=>sG.addCaptureListener(CardLayoutCaptureListener)
              }
              case _=>
            }
         log.info("GAME HANDLER "+gameState)
         state=gameState
         slotGame=sG
         true
       case _ =>false
     }
   })}

  def pokerGameHandler() = pokerGame.addCaptureListener(new EventListener {
    override def handle(event: Event): Boolean = event match {
      case GameStateChange(gameState)=> gameState match {
        case GAMESTATE.HOME=>pokerGame.removeCaptureListener(CardLayoutCaptureListener)
          pokerGame.removeActor(pokerGame.cardLayout)
        case GAMESTATE.POKERGAME=>CardLayoutState.state match {
          case INSERTCARD=>
            pokerGame.addActor (pokerGame.cardLayout)
          case _=>
        }
          pokerGame.addCaptureListener (CardLayoutCaptureListener)
        case _=>
      }
        log.info("GAME HANDLER "+gameState)
        state=gameState
        true
      case _=>false
    }
  })

  object  CardLayoutCaptureListener extends EventListener{
    override def handle(event:Event):Boolean= event match {
      case ButtonHandler(msg, xEvent, passwordTextFiled) =>
        numM.findFirstIn(msg) match {
          case Some(z) => xEvent match {
            case ButtonDown(bState) =>
              passwordTextFiled.count + 1 > 4 match {
                case true =>
                case false => passwordTextFiled.textField.setText(passwordTextFiled.textField.getText + z)
                  passwordTextFiled.count += 1
              }
            case _ =>
          }
          case None => msg match {
            case "clear" => xEvent match {
              case ButtonDown(bState) =>
                passwordTextFiled.count - 1 < 0 match {
                  case true =>
                    passwordTextFiled.textField.setText("")
                  case false =>
                    passwordTextFiled.count -= 1
                    passwordTextFiled.textField.setText(passwordTextFiled.textField.getText.substring(0, passwordTextFiled.count))
                }
              case _ =>
            }
            case "ok" => xEvent match {
              case ButtonDown(bState) => passwordTextFiled.textField.getText.equals(Model.paymentCard.password.get) match {
                case true => Model.paymentCard.balance > 0 match {
                  case true => self ! CoinIn
                  case false => self ! Imbalance
                }
                case false => Model.paymentCard.balance > 0 match {
                  case true => self ! InvalidPin
                  case false => self ! Imbalance
                }
              }
                passwordTextFiled.count = 0
                passwordTextFiled.textField.setText("")
              case _ =>
            }
            case _ =>
          }
        }
        true
      case CASHOUT => Model.paymentCard != null && Model.paymentCard.balance > 0 match {
        case true => self ! CoinOut
        case false =>
      }
        true
      case _ => false
    }
  }

  def detected(cardNum:String)={
    self ! CardDetected(cardNum)
  }

  def sendCardRequest(state:APPSTATE,cardNum:String)= {
    log.info(state + "" + cardNum)
    state match {
      case INSERTCARD => nemesisController ! SendCardRequest(Prop.uriMapper("view"), CardRequest(StringBuilder.generateTransactionId, cardNum, terminalID))
      case _ =>
    }
  }

  def credited(buttonLayout: ButtonLayout,group:Group,cardLayout: CardLayout)={
    buttonLayout.cashOut()
    group.addActor(cardLayout)
  }

  def debited(buttonLayout: ButtonLayout,group: Group,cardLayout: CardLayout)={
    cardLayout.msgPanel.addSubMsg(Prop.msgMapper("insert"))
    cardLayout.showPanel(cardLayout.msgPanel.value._1)
    group.removeActor(cardLayout)
    buttonLayout.cashIn()
  }
  def error(state:APPSTATE,group: Group,cardLayout: CardLayout,msg:String)={
   state match {
     case AUTHENTICATED=> group.removeActor(cardLayout)
     case _=>
   }
    cardLayout.msgPanel.addSubMsg(Prop.msgMapper(msg))
    cardLayout.showPanel(cardLayout.msgPanel.value._1)
  }
  def retrive(cardLayout: CardLayout,group: Group,card:PaymentCard)={
    card.password match {
      case Some(x) =>
        Model.paymentCard=card
        cardLayout.showPanel(cardLayout.pinFunctionalPanel.value._1)
        state match {
          case GAMESTATE.SLOTGAME=> group.asInstanceOf[SlotGame].bottomPanel.buttonLayout.disableMenus()
          case GAMESTATE.POKERGAME=>group.asInstanceOf[PokerGame].buttonLayout.disableMenus()
          case _=> CardLayoutState.state=INSERTCARD
        }
      case None=>cardLayout.msgPanel.addSubMsg(Prop.msgMapper("card"))
        CardLayoutState.state=INSERTCARD
    }
  }

  def receive={
    case CardDetected(cardNum)=>
      log.info("CARD RECV"+controllerFlag)
      controllerFlag match {
      case true => state match {
        case GAMESTATE.SLOTGAME => sendCardRequest(CardLayoutState.state,cardNum)
        case GAMESTATE.POKERGAME =>sendCardRequest(CardLayoutState.state,cardNum)
        case GAMESTATE.HOME =>
      }
      case false =>
    }
    case CoinIn => nemesisController ! SendCardTransaction(Prop.uriMapper("debit"),ProcessTransaction(StringBuilder.generateTransactionId,Model.paymentCard.cardId,Model.paymentCard.balance,terminalID))
    case CoinOut =>nemesisController ! SendCardTransaction(Prop.uriMapper("credit"),ProcessTransaction(StringBuilder.generateTransactionId,Model.paymentCard.cardId,Model.paymentCard.balance,terminalID))
    case Credited(transaction) =>log.info("COIN OUT")
      Model.paymentCard=null
      state match {
        case GAMESTATE.SLOTGAME =>
          CardLayoutState.state=INSERTCARD
          credited(slotGame.bottomPanel.buttonLayout,slotGame,slotGame.cardLayout)
        case GAMESTATE.POKERGAME =>
          CardLayoutState.state=INSERTCARD
          credited(pokerGame.buttonLayout,pokerGame,pokerGame.cardLayout)
        case GAMESTATE.HOME =>
      }
    case DevicesRegisted =>
      log.info("DEVICE REGISTERED")
      controllerFlag=true
    case Debited(transaction) =>log.info("COIN IN")
      state match {
        case GAMESTATE.SLOTGAME =>
          CardLayoutState.state=AUTHENTICATED
          debited(slotGame.bottomPanel.buttonLayout,slotGame,slotGame.cardLayout)
        case GAMESTATE.POKERGAME =>
          CardLayoutState.state=AUTHENTICATED
          debited(pokerGame.buttonLayout,pokerGame,pokerGame.cardLayout)
        case GAMESTATE.HOME =>
      }
    case Failed =>
      state match {
        case GAMESTATE.SLOTGAME => error(CardLayoutState.state,slotGame,slotGame.cardLayout,"insert")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.POKERGAME =>error(CardLayoutState.state,pokerGame,pokerGame.cardLayout,"insert")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.HOME =>
      }
    case ConnectionTimedOut =>
      state match {
        case GAMESTATE.SLOTGAME => error(CardLayoutState.state,slotGame,slotGame.cardLayout,"timed")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.POKERGAME =>error(CardLayoutState.state,pokerGame,pokerGame.cardLayout,"timed")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.HOME =>
      }
    case Initialize =>
       nemesisController ! Initialize
    case Initialized =>
      log.info("Initialized")
      deviceExtension ! RegisterDevices
     uiApplication=Some(new LwjglApplication(application,lwjglCfg.cfg))
    case InvalidCard=>
      state match {
        case GAMESTATE.SLOTGAME => error(CardLayoutState.state,slotGame,slotGame.cardLayout,"card")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.POKERGAME =>error(CardLayoutState.state,pokerGame,pokerGame.cardLayout,"card")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.HOME =>
      }
    case InvalidPin=>
      state match {
        case GAMESTATE.SLOTGAME => error(CardLayoutState.state,slotGame,slotGame.cardLayout,"pin")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.POKERGAME =>error(CardLayoutState.state,pokerGame,pokerGame.cardLayout,"pin")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.HOME =>
      }
    case Imbalance=>
      state match {
      case GAMESTATE.SLOTGAME => error(CardLayoutState.state,slotGame,slotGame.cardLayout,"balance")
        CardLayoutState.state=INSERTCARD
      case GAMESTATE.POKERGAME =>error(CardLayoutState.state,pokerGame,pokerGame.cardLayout,"balance")
        CardLayoutState.state=INSERTCARD
      case GAMESTATE.HOME =>
    }
    case Mismatch=>
      state match {
        case GAMESTATE.SLOTGAME => error(CardLayoutState.state,slotGame,slotGame.cardLayout,"mismatch")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.POKERGAME =>error(CardLayoutState.state,pokerGame,pokerGame.cardLayout,"mismatch")
          CardLayoutState.state=INSERTCARD
        case GAMESTATE.HOME =>
      }
    case Retrieved(card)=>
      log.info("CONTROLLER"+card)
      state match {
        case GAMESTATE.SLOTGAME =>
          CardLayoutState.state = AUTHENTICATION
          retrive(slotGame.cardLayout,slotGame,card)
        case GAMESTATE.POKERGAME =>
          CardLayoutState.state = AUTHENTICATION
          retrive(pokerGame.cardLayout,pokerGame,card)
        case GAMESTATE.HOME =>
      }
    case _ =>
  }

  override def postStop(): Unit = {
    log.info("SHUTTING DOWN UI ")
    uiApplication.foreach(_.exit())
  }
}

