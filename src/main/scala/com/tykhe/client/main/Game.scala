package com.tykhe.client.main

import java.awt.{Dimension, Toolkit}
import java.nio.IntBuffer

import com.badlogic.gdx.Application.ApplicationType
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.graphics.profiling.GLProfiler
import com.badlogic.gdx.utils.viewport.{ScreenViewport, StretchViewport}
import com.badlogic.gdx.{ApplicationAdapter, Gdx}
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.{OrthographicCamera, GL20}
import com.badlogic.gdx.scenes.scene2d.{Event, Stage}
import com.tykhe.client.components.GAMESTATE
import com.tykhe.client.controller.Controller
import com.tykhe.client.slot.SlotGame
import com.tykhe.client.utils.Prop
import com.typesafe.config.{Config, ConfigFactory}
import org.lwjgl.{BufferUtils, LWJGLException}
import org.lwjgl.input.{Cursor, Mouse}

class MainMenu(pokerConfig: Config, slotConfigs: Seq[(SlotGameVariant,Config)],controller: Controller,view:String) extends ApplicationAdapter {
  var stage: Stage = null
  var homeScreen: HomeScreen = null
  var emptyCursor:Cursor=null

  override def create() = {
    val viewport = view match {
      case "STRETCH" => new StretchViewport(pokerConfig.getInt("screenX"),pokerConfig.getInt("screenY"))
      case _ => new ScreenViewport()
    }
    stage = new Stage(viewport, new PolygonSpriteBatch())
    Gdx.input.setInputProcessor(stage)
    homeScreen = new HomeScreen(pokerConfig,slotConfigs)
    controller.slotGameS=homeScreen.slotGames.foldLeft(Array.empty[SlotGame])((z,a)=>z ++ Array(a._2))
    controller.pokerGame=homeScreen.pokerGame
    controller.slotGameHandler()
    controller.pokerGameHandler()
    stage.addActor(homeScreen)
    setHWCursorVisible(controller.cursorState)
  }
  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act(Gdx.graphics.getDeltaTime())
    stage.draw()
  }

  override def resize(width:Int,height:Int)={
    stage.getViewport.update(width, height, true)
  }

  @throws(classOf[LWJGLException])
  def setHWCursorVisible(visible: Boolean): Unit = {
    if (Gdx.app.getType() != ApplicationType.Desktop
      && Gdx.app.isInstanceOf[LwjglApplication])
      return
    if (emptyCursor == null) {
      if (Mouse.isCreated()) {
        var min: Int = org.lwjgl.input.Cursor.getMinCursorSize()
        var tmp: IntBuffer = BufferUtils.createIntBuffer(min * min)
        emptyCursor = new Cursor(min, min, min / 2,
          min / 2, 1, tmp, null)
      } else {
        throw new LWJGLException(
          "Could not create empty cursor before Mouse object is created")
      }
    }
    if (Mouse.isInsideWindow()) Mouse.setNativeCursor(if (visible) emptyCursor else null)
  }
}

case class LwjglConf(width:Int,height:Int,fullscreen:Boolean){
  val cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration()
  cfg.title="TERMINAL"
  cfg.fullscreen=fullscreen
  cfg.width=width
  cfg.height=height
}
object Game {
  val pokerConfig: Config = ConfigFactory.load("poker")
}