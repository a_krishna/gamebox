package com.tykhe.client.main

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Group, Actor}
import scala.collection.JavaConversions._

case class Container(_x: Float, _y: Float, childActors: Seq[Actor]) extends Group {
  setPosition(_x,_y)
  childActors.foreach(relocate)
  childActors.foreach(addActor)

  override def addActor(actor: Actor): Unit = {
    relocate(actor)
    super.addActor(actor)
  }

  def relocate(actor: Actor): Unit = {
    val name = actor.getClass.getSimpleName
    println(s"relocating $name from ${actor.getX} ${actor.getY}")
    actor.moveBy(getX, getY)
    println(s"relocated $name to ${actor.getX} ${actor.getY}")
    actor match {
      case group: Group => for (child <- group.getChildren.iterator()) relocate(child)
      case _ =>
    }
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
  }
}
