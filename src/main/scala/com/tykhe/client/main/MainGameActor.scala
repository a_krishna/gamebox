package com.tykhe.client.main

import akka.actor.{Props, ActorSystem}
import com.tykhe.client.controller.Controller
import com.tykhe.client.utils.Initialize
import scala.concurrent.Await
import scala.concurrent.duration._

object MainGameActor extends App{
  val system = ActorSystem("SLOT-APPLICATION")
  val selection=system.settings.config.getString("nemesis.terminal.prop.game-selection")
  val controller = system.actorOf(Props(new Controller(selection)),"controller")
  controller ! Initialize
  Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
    override def run(): Unit = {
      system.log.info(s"SHUTTING DOWN ACTOR SYSTEM : $system")
      Await.ready(system.terminate(),5.seconds)
    }
  }))
}
