package com.tykhe.client.main

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Event, Group, InputEvent}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.tykhe.client.components.{SLOTGAMESTATE, GAMESTATE}
import com.tykhe.client.components.buttons.PortraitLoader
import com.tykhe.client.main.SlotGameVariant.{CASENO, FRUIT}
import com.tykhe.client.model.Model
import com.tykhe.client.poker.components.PokerGame
import com.tykhe.client.roulette.Roulette
import com.tykhe.client.slot.APPCARDSTATE.{AUTHENTICATED, AUTHENTICATION, INSERTCARD}
import com.tykhe.client.slot.SlotGame
import com.tykhe.client.utils.{CardLayoutState, CardLayout}
import com.typesafe.config.Config

import scala.collection.mutable.ListBuffer

sealed trait SlotGameVariant
object SlotGameVariant{
  case object FRUIT extends SlotGameVariant
  case object CASENO extends SlotGameVariant
}

case class GameStateChange(state:GAMESTATE) extends Event
object CASHOUT extends Event

class HomeScreen(pokerConfig: Config, slotConfigs:Seq[(SlotGameVariant,Config)]) extends Group {
  val pokerLogo: PortraitLoader = new PortraitLoader(pokerConfig.getString("logo.path"))
  val slotLogo:Seq[(SlotGameVariant,PortraitLoader)]=slotConfigs.foldLeft(Seq.empty[(SlotGameVariant,PortraitLoader)])((z,a)=> z++ Seq( (a._1,new PortraitLoader(a._2.getString("logo.path")))))
  val separator:Seq[(SlotGameVariant,PortraitLoader)]=slotConfigs.foldLeft(Seq.empty[(SlotGameVariant,PortraitLoader)])((z,a)=> z++ Seq((a._1,new PortraitLoader(pokerConfig.getString("separator.path")))))
  val pokerGame: PokerGame = new PokerGame(pokerConfig)
  val slotGames: Seq[(SlotGameVariant,SlotGame)] =slotConfigs.foldLeft(Seq.empty[(SlotGameVariant,SlotGame)])((z,a)=> z++Seq((a._1,new SlotGame(a._2))))
  val cardLayout=CardLayout()
  val posX=300
  pokerGame.setHome(this)
  slotGames.foreach(_._2.setHome(this))
  var flag = false
  slotConfigs.length==1 match {
    case true=>pokerLogo.setBounds(pokerConfig.getLong("logo.x")+posX, pokerConfig.getLong("logo.y"), pokerConfig.getLong("logo.width"), pokerConfig.getLong("logo.height"))
      separator.head._2.setPosition(pokerConfig.getInt("separator.x")+posX,pokerConfig.getInt("separator.y"))
      slotGames.foreach(_._2.barrel.med_speed=3)
    case false=>pokerLogo.setBounds(pokerConfig.getLong("logo.x"), pokerConfig.getLong("logo.y"), pokerConfig.getLong("logo.width"), pokerConfig.getLong("logo.height"))
      separator.find(_._1==CASENO).head._2.setPosition(pokerConfig.getInt("separator.x"),pokerConfig.getInt("separator.y"))
      slotGames.foreach(_._2.barrel.med_speed=1)
  }
  slotConfigs.foreach{conf=>
    conf._1 match {
      case FRUIT=> slotConfigs.length==1 match {
        case true=>slotLogo.find(_._1==conf._1).head._2.setBounds(conf._2.getLong("logo.x")+posX, conf._2.getLong("logo.y"), conf._2.getLong("logo.width"), conf._2.getLong("logo.height"))
        case false=>
          slotLogo.find(_._1==conf._1).head._2.setBounds(conf._2.getLong("logo.x"), conf._2.getLong("logo.y"), conf._2.getLong("logo.width"), conf._2.getLong("logo.height"))
          separator.find(_._1==FRUIT).head._2.setPosition(conf._2.getInt("separator.x"),conf._2.getInt("separator.y"))
      }
      case CASENO=>slotConfigs.length==1 match {
        case true=>slotLogo.find(_._1==conf._1).head._2.setBounds(conf._2.getLong("logo.x")-posX, conf._2.getLong("logo.y"), conf._2.getLong("logo.width"), conf._2.getLong("logo.height"))
        case false=>
          slotLogo.find(_._1==conf._1).head._2.setBounds(conf._2.getLong("logo.x"), conf._2.getLong("logo.y"), conf._2.getLong("logo.width"), conf._2.getLong("logo.height"))
      }
    }
  }
  pokerGame.cardLayout=cardLayout
  slotGames.foreach(_._2.cardLayout=cardLayout)
  addActor(pokerLogo)
  separator.foreach(s=>addActor(s._2))
  slotLogo.foreach(l=> addActor(l._2))
  slotGames.foreach(_._2.stopBackground)
  pokerLogo.addListener(new ClickListener {
    override def clicked(event: InputEvent, x: Float, y: Float) {
      println("Clicked on Poker")
      clearChildren()
      CardLayoutState.state match {
          case INSERTCARD=>
            pokerGame.buttonLayout.cashOut()
//            pokerGame.cardLayout.showPanel(pokerGame.cardLayout.msgPanel.value._1)
          case AUTHENTICATION=> pokerGame.cardLayout.showPanel(pokerGame.cardLayout.pinFunctionalPanel.value._1)
          case AUTHENTICATED=>pokerGame.cardLayout.showPanel("NONE")
          pokerGame.buttonLayout.changeSlotGameState(SLOTGAMESTATE.FREE)
        }
      addActor(pokerGame)
      Model.paymentCard !=null match {
        case true=> pokerGame.buttonLayout.cashMeter.setAmount(Model.paymentCard.balance.toInt)
        case false=>pokerGame.buttonLayout.cashMeter.setAmount(0)
      }
      pokerGame.fire(GameStateChange(GAMESTATE.POKERGAME))
    }
  })
  slotLogo.foreach{z=>
    z._2.addListener(new ClickListener {
    override def clicked(event: InputEvent, x: Float, y: Float) {
      println("Clicked on Slot")
      clearChildren()
      val sG=slotGames.find(_._1==z._1).head._2
       val cL=sG.cardLayout
       CardLayoutState.state match {
         case INSERTCARD=>
           sG.bottomPanel.buttonLayout.cashOut()
//           cL.showPanel(cL.msgPanel.value._1)
         case AUTHENTICATION=>cL.showPanel(cL.pinFunctionalPanel.value._1)
         case AUTHENTICATED=>cL.showPanel("NONE")
           sG.bottomPanel.buttonLayout.changeSlotGameState(SLOTGAMESTATE.FREE)
       }
       sG.setJackpots()
       addActor(sG)
       Model.paymentCard !=null match {
         case true=>sG.bottomPanel.buttonLayout.cashMeter.setAmount(Model.paymentCard.balance.toInt)
         case false=>sG.bottomPanel.buttonLayout.cashMeter.setAmount(0)
       }
      sG.fire(GameStateChange(GAMESTATE.SLOTGAME))
    }})}


  def drawHome() {
    println("Clear Children")
    clearChildren()
    slotGames.foreach(_._2.stopBackground)
    slotGames.foreach(_._2.fire(GameStateChange(GAMESTATE.HOME)))
    pokerGame.fire(GameStateChange(GAMESTATE.HOME))
    addActor(pokerLogo)
    separator.foreach(s=>addActor(s._2))
    slotLogo.foreach(l=> addActor(l._2))
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
  }
}