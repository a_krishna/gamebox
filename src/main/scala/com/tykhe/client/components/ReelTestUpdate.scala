package com.tykhe.client.components

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.ui.{Button, SelectBox, Skin}
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent
import com.badlogic.gdx.scenes.scene2d.utils.{ChangeListener, ClickListener}
import com.badlogic.gdx.scenes.scene2d.{Actor, Group, InputEvent, ui}
import com.badlogic.gdx.utils
import com.tykhe.client.poker.components.button.ButtonLayout
import com.tykhe.client.slot.components.Barrel
import com.typesafe.config.Config

/**
 * Created by aravind on 25/3/15.
 */
class ReelStopBox(skin:Skin,config:Config) extends Group{
  val sb=new SelectBox[String](skin);
  val datas:utils.Array[String]=new utils.Array[String]()
  for(i <- 0 to config.getStringList("data").size()-1){
    datas.add(config.getStringList("data").get(i))
  }
  sb.setItems(datas)
  var reelName=sb.getItems.get(0)
  config.getStringList("data")
  sb.setSelected(sb.getItems.get(0))
  sb.setBounds(config.getLong("x"),config.getLong("y"),config.getLong("width"),config.getLong("height"))
  addActor(sb)
  sb.addListener(new ChangeListener() {
    override def changed(event: ChangeEvent, actor: Actor) ={
      reelName=sb.getSelected
    }
  })
  override def draw(batch:Batch,parentAlpha:Float)=super.draw(batch,parentAlpha)
}

class ReelTestUpdate(skin: Skin,config:Config,buttonLayout: ButtonLayout) extends Group{
  val reelsStopBox:Array[ReelStopBox]=new Array[ReelStopBox](config.getInt("size"))
  val postButton:Button=new ui.Button(skin)
  var reelStopName=""
  postButton.add(config.getString("button.name"))
  postButton.setBounds(config.getLong("button.x"),config.getLong("button.y"),config.getLong("button.width"),config.getLong("button.height"))
  for( i <- 0 to reelsStopBox.size-1){
    reelsStopBox(i)=new ReelStopBox(skin,config.getConfig("reel"+i))
    addActor(reelsStopBox(i))
  }
  addActor(postButton)
  postButton.addListener(new ClickListener(){
    override def clicked(event:InputEvent,x:Float,y:Float): Unit ={
      reelsStopBox.foreach(reels=>reelStopName+=reels.reelName+",")
      println(reelStopName.dropRight(1))
//      buttonLayout.slotGame.barrel.
      reelStopName=""
    }
  })
  override def draw(batch:Batch,parentAlpha:Float)=super.draw(batch,parentAlpha)
}
