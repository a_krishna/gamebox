package com.tykhe.client.components.buttons

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{Color, Texture}
import com.badlogic.gdx.graphics.g2d.{Batch, BitmapFont}
import com.badlogic.gdx.scenes.scene2d.Actor
import com.tykhe.client.components.LineTemp
import com.tykhe.client.main.SlotGameVariant

class PortraitLoader(filePath: String) extends Actor {

  var texture: Texture = new Texture(Gdx.files.internal(filePath))
  var flag: Boolean = false;
  var debugFlag = false
  var box: LineTemp = null
  var boundCorrection = true
  var label: BitmapFont = new BitmapFont
  label.setColor(Color.WHITE)
  var text: String = ""
  var state:SlotGameVariant=null

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
    if (!getFlag) {
      batch.draw(texture, getX(), getY())
      if (debugFlag) box.drawRect()
    }
    if (!boundCorrection) {
      batch.draw(texture, getX(), getY())
      label.draw(batch, text, getX() + GameSetup.labelPos._1, getY() - GameSetup.labelPos._2) //18 to 7 for laptop resolution
    }
  }



  def getFlag: Boolean = flag
  def setFlag(flag: Boolean): Unit = this.flag = flag
}