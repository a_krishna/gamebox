package com.tykhe.client.components.meter

import java.util.ArrayList

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.{Label, Skin}
import com.typesafe.config.Config

class Meter(config: Config, skin: Skin) extends Group {
  val labels=new Array[Label](config.getInt("size"))
  val labelStyle=GenerateFont.fontStyle(config.getString("fontName"),config.getInt("fontSize"),Color.WHITE)
  val charArray=Array[Char](0)
  var credits: Int = 0
  var image = new StatefulLabel(skin, config.getStringList("states").asInstanceOf[ArrayList[String]])
  image.setPosition(config.getLong("meterX"), config.getLong("meterY"))
  addActor(image)
  prepareMeter()
  def prepareMeter()={
    var sumX=config.getLong("x")
    for(i<- 0 to labels.length-1) {
      charArray(0)=getChar(0)
      val label = new Label(charArray, skin)
      label.setStyle(labelStyle)
      label.setPosition(sumX, config.getLong("y"))
      label.setColor(1,1,0.1f,0.1f)
      label.setText(charArray)
      labels.update(i, label)
      addActor(label)
      sumX += config.getInt("padX")
    }
  }
  val disabledColor = new Color(1, 1, 0.1f, 0.1f)
  def setAmount(amt: Int) = {
    amt < 0 match {
      case true =>
      case false =>
        credits = amt
        labels.foreach(label => {
          label.setText("0")
          label.setColor(disabledColor)
        })
        amt.toString.reverse.toCharArray.zip(labels.reverse).foreach(p => {
          p._2.setText(p._1.toString)
          p._2.setColor(Color.WHITE)
        })
    }
  }
  def prepareText(amt:Array[Int])(f:Int=>Color)={
    amt.foreach(e=>labels.foldRight(e){(z,a)=> charArray(0)= getChar(a % 10);z.setText(charArray);z.setColor(f(a));a/10})
  }
  def reset() = labels.foreach{label=>label.setColor(1,1,0.1f,0.1f);charArray(0)=getChar(0);label.setText(charArray)}

  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)

  def getChar(num:Int):Char={
    num match {
      case 0=>'0'
      case 1=>'1'
      case 2=>'2'
      case 3=>'3'
      case 4=>'4'
      case 5=>'5'
      case 6=>'6'
      case 7=>'7'
      case 8=>'8'
      case 9=>'9'
    }
  }
}