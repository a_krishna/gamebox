package com.tykhe.client.components.meter

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
case class FontBorder(color:Color,width:Float,straight:Boolean,glyphCount:Int)
case class FontShadow(color: Color,offsetX:Int,offsetY:Int,glyphCount:Int)
case class FontProp(fontName:String,size:Int,color: Color)
object GenerateFont {
  def normalFontStyle(font:FontProp) =  new LabelStyle(fontGenerator(font.fontName).generateFont(normalFontParameter(new FreeTypeFontParameter(),font.size)),font.color)

  def decoratedFontStyle(font:(FontProp,(FontBorder,FontShadow))) = new LabelStyle(fontGenerator(font._1.fontName).generateFont(decoratedFontParameter(font._2,font._1.size)),font._1.color)

  def fontStyle(fontName:String,size:Int,color: Color):LabelStyle={
    new LabelStyle(fontGenerator(fontName).generateFont(normalFontParameter(new FreeTypeFontParameter(),size)),color)
  }

  def fontGenerator(fontName:String):FreeTypeFontGenerator=new FreeTypeFontGenerator(Gdx.files.internal(fontName))
  
  def normalFontParameter(fontParameter:FreeTypeFontParameter,size:Int):FreeTypeFontParameter={
    fontParameter.size=size
    fontParameter
  }
  def decoratedFontParameter(fp:(FontBorder,FontShadow),size:Int)={
    val fontParameter=new FreeTypeFontParameter
    fontParameter.borderColor=fp._1.color
    fontParameter.borderGlyphCount=fp._1.glyphCount
    fontParameter.borderStraight=fp._1.straight
    fontParameter.borderWidth=fp._1.width
    fontParameter.shadowColor=fp._2.color
    fontParameter.shadowGlyphCount=fp._2.glyphCount
    fontParameter.shadowOffsetX=fp._2.offsetX
    fontParameter.shadowOffsetY=fp._2.offsetY
    fontParameter.size=size
    fontParameter
  }

}
