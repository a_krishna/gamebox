package com.tykhe.client.components.meter

import java.util.ArrayList

import com.badlogic.gdx.scenes.scene2d.{Event, EventListener}
import com.badlogic.gdx.scenes.scene2d.ui.{ImageButton, Label, Skin}

import scala.collection.immutable.HashMap

class StatefulLabel(skin: Skin, styles: ArrayList[String]) extends ImageButton(skin.get(styles.get(0), classOf[com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle])) {
  var state: String = null
  var states: HashMap[String, com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle] = HashMap()
  var label: Label = new Label(" ", skin)
  this.setSkin(skin)
  initalize
  def initalize = {
    println("I am in intialize")
    styles.toArray().foreach(style => states += (style.toString() -> skin.get(style.toString(), classOf[com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle])))
    addListener(new LabelStateManager)
  }
  class LabelStateManager extends EventListener {
    override def handle(event: Event): Boolean = {
      if (event.isInstanceOf[ChangeLabelState]) {
        state = event.asInstanceOf[ChangeLabelState].newState
        setStyle(states.get(state).get)
      }
      return false
    }
  }
}