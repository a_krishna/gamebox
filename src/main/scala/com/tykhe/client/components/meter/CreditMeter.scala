package com.tykhe.client.components.meter

import java.lang.Boolean

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Group, InputEvent}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Array
import com.tykhe.client.components.buttons.PortraitLoader

class CreditMeter extends Group {
  var textures = new Array[PortraitLoader]()
  var flag: Boolean = false
  var tmp: Int = 0

  def setPosition(x: Float, y: Float, width: Float, height: Float): Unit = setBounds(x, y, width, height)
  def init(files: List[String]) {
    files.foreach(file => addTextureButton(file))
    textures.get(tmp).setFlag(false)
    addListener(new ClickListener {
      override def clicked(event: InputEvent, x: Float, y: Float) {
        if (tmp == files.size - 1) flag = true;
        if (flag) if (tmp == 0) flag = false
        for (i <- 0 to (files.length - 1)) {
          var temp = if (!flag) tmp + 1 else tmp - 1
          if (temp != i) textures.get(i).setFlag(true) else textures.get(i).setFlag(false)
        }
        tmp = if (!flag) tmp + 1 else tmp - 1
      }
    })
  }
  def addTextureButton(file: String) = {
    val texture = new PortraitLoader(file)
    texture.setBounds(getX(), getY(), getWidth(), getHeight())
    texture.setFlag(true)
    textures.add(texture)
    addActor(texture)
  }
  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)
}