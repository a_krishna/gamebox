package com.tykhe.client.components.meter

import com.badlogic.gdx.scenes.scene2d.{Actor, Group}
import com.badlogic.gdx.scenes.scene2d.ui.{SelectBox, Skin}
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent
import com.tykhe.client.poker.components.button.ButtonLayout

/**
 * Created by aravind on 25/3/15.
 */
class ReelStopDebugger(skin:Skin,buttonLayout: ButtonLayout) extends  Group{
  val sb=new SelectBox[String](skin);
  sb.setItems("S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14")
  sb.setSelected("S1")
  sb.setWidth(50)
  sb.setHeight(50)
  sb.setPosition(500,200);
  val sb1=new SelectBox[String](skin);
  sb1.setItems("S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14")
  sb1.setSelected("S1")
  sb1.setWidth(50)
  sb1.setHeight(50)
  sb1.setPosition(570,200);
  val sb2=new SelectBox[String](skin);
  sb2.setItems("S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14")
  sb2.setSelected("S1")
  sb2.setWidth(50)
  sb2.setHeight(50)
  sb2.setPosition(640,200);
  addActor(sb)
  addActor(sb1)
  addActor(sb2)
  sb.addListener(new ChangeListener() {
    override def changed(event: ChangeEvent, actor: Actor) ={
      println(sb.getSelected)
    }
  })
  sb1.addListener(new ChangeListener() {
    override def changed(event: ChangeEvent, actor: Actor) ={
      println(sb1.getSelected)
    }
  })
  sb2.addListener(new ChangeListener() {
    override def changed(event: ChangeEvent, actor: Actor) ={
      println(sb2.getSelected)
    }
  })

}
