package com.tykhe.client.components.meter

class Meters {
  var credits, denom: Float = 0f

  def setMeter(credits: Float, denom: Float) {
    this.credits = credits
    this.denom = denom
  }
  def winAmount(winPay: Float): Float = {
    return (credits * denom) * winPay
  }

}