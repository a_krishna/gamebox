package com.tykhe.client.components.sound

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.scenes.scene2d.Actor

class Sound(path: String) extends Actor {
  var pitch: Float = 1.0f
  var volume: Float = 1.0f
  var pan: Float = 0
  var flag: Boolean = false
  var sound: com.badlogic.gdx.audio.Sound = Gdx.audio.newSound(Gdx.files.internal(path));
  var music: Music = Gdx.audio.newMusic(Gdx.files.internal(path))

  def setSoundSetting(volume: Float, pitch: Float, pan: Float): Unit = {
    this.pitch = pitch
    this.volume = volume
    this.pan = pan
  }

  def playSound(): Unit = {
    val soundId: Long = sound.play
    sound.setVolume(soundId, volume);
    sound.setPitch(soundId, pitch);
    sound.setPan(soundId, pan, volume);
    if (flag) sound.loop()
  }

  def stopSound() = {
    sound.stop()
    music.stop()
  }

  def playMusic() = {
    music.setVolume(volume)
    music.setPan(pan, pitch)
    music.play()
  }

  def compilitionHandler() = {
  }

  trait SoundLisenter extends Music.OnCompletionListener {

    override def onCompletion(music: Music) = {
    }
  }

}