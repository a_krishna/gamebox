package com.tykhe.client.components.buttons

import com.badlogic.gdx.scenes.scene2d.Event


case class ButtonPress(stateName:String) extends Event
case class ButtonUp(state:String) extends Event
case class ButtonDown(state:String) extends Event
case class ChangeButtonState(newState:String) extends Event
case class ButtonHandle(valueType:String,event: Event) extends Event
