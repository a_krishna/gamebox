package com.tykhe.client.components.buttons

import com.typesafe.config.Config

object GameSetup {

  var labelPos = (20, 7)
  var inlineAnimationTime=2f

  def setLabelPos(config: Config) = {
    labelPos = (config.getInt("labelX"), config.getInt("labelY"))
    inlineAnimationTime=config.getLong("inlineAnimationTime")
  }
}
