package com.tykhe.client.components.buttons

import java.util.ArrayList

import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.{Actor, Event, EventListener, InputEvent, InputListener}
import com.badlogic.gdx.scenes.scene2d.ui.{ImageButton, Skin}

import scala.collection.immutable.HashMap

class StatefulImageButton(skin: Skin, styles: ArrayList[String]) extends ImageButton(skin.get(styles.get(0), classOf[com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle])) {
  var state: String = null
  var states: HashMap[String, com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle] = HashMap()
  println("IMageButton State =" + styles.get(0));
  state = styles.get(0)
  this.setSkin(skin)
  initalize

  def initalize = {
    println("I am in intialize "+styles.get(0).split("_").head)
    styles.toArray().foreach(style => states += (style.toString() -> skin.get(style.toString(), classOf[com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle])))
    if(styles.get(0).split("_").head.equals("gamble")||styles.get(0).split("_").head.equals("bonus")){
      addListener(new ClickManager)
    }else{
      addListener(new InputManager)
    }

    addListener(new ButtonStateManager)
  }
  class ClickManager extends ClickListener{
    override def  clicked(event:InputEvent,x:Float,y:Float):Unit={
    super.clicked(event, x, y)
      System.out.println("Button CLicked "+state)
      fire(ButtonPress(state))
    }
  }
  class InputManager extends InputListener {
    override def touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Unit = {
      super.touchUp(event, x, y, pointer, button)
      System.out.println("touchUp: " + state);
      fire(ButtonUp(state));
    }
    override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
      System.out.println("touchDown: " + state);
      fire(ButtonDown(state));
      return true;
    }
  }

  class ButtonStateManager extends EventListener {
    override def handle(event: Event): Boolean = {
      if (event.isInstanceOf[ChangeButtonState]) {
        state = event.asInstanceOf[ChangeButtonState].newState
        setStyle(states.get(state).get)
        return true
      }
      return false
    }
  }

}