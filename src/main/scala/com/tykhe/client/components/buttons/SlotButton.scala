package com.tykhe.client.components.buttons

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.{Interpolation, Rectangle}
import com.badlogic.gdx.scenes.scene2d.{Group, InputEvent}
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

class SlotButton extends Group {
  private var padding: Float = 0
  var flag: Boolean = false
  var cullingPosition: Float = 0

  def setPosition(x: Float, y: Float, width: Float, height: Float): Unit = setBounds(x, y, width, height)

  def init(files: List[String]) {
    println("Size of files" + files.size)
    var tmp: Int = 0;
    var paddingY = getY()
    for (file <- files) {
      val texture: PortraitLoader = new PortraitLoader(file)
      texture.setBounds(getX(), paddingY, getWidth(), getHeight())
      addActor(texture)
      paddingY += getPadding
    }
    setBounds(getX(), getY(), getPadding, getPadding * files.size)
//    setCullingArea(new Rectangle(getX(), getY(), getWidth(), 0));
    addListener(new ClickListener {
      override def clicked(event: InputEvent, x: Float, y: Float) {
        if (tmp == files.size - 1) flag = true
        if (flag) if (tmp == 0) flag = false
        addAction(Actions.moveBy(getX(), getPaddingY, 1, Interpolation.fade))
        event.getTarget().getParent().setCullingArea(new Rectangle(0, 0, getWidth(), getCullingPostion()));
        tmp = if (flag == false) tmp + 1 else tmp - 1
        println(tmp + " " + getY())
      }
    })
  }
  def getPaddingY(): Float = {
    if (flag == false) {
      return getX() - getPadding
    } else {
      return getX() + getPadding
    }
  }
  def getCullingPostion(): Float = {
    if (flag == false) {
      cullingPosition = cullingPosition + getPadding
      return cullingPosition
    } else {
      cullingPosition = cullingPosition - getPadding
      return cullingPosition
    }
  }
  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)
  def getPadding(): Float = padding
  def setPadding(padding: Float): Unit = this.padding = padding
}