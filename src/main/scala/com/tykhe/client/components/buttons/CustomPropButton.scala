package com.tykhe.client.components.buttons

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d._
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.{ImageButton, Label, Skin, TextField}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.tykhe.client.utils.CustomImageLoader

class StateFullButton(style: (Skin, Array[(String, ButtonStyle)])) extends ImageButton(style._1.get(style._2.head._1, classOf[ImageButton.ImageButtonStyle])) {
  var state = style._2.head._1

  addListener(InputManager)

  addListener(ButtonStateManager)

  def getState: Array[String] = style._2.foldLeft("")((z, a) => a._1 + "," + z).split(",")

  def changeState(newState: String) = {
    state = newState
    setStyle(style._2.filter(_._1 == newState).head._2)
  }


  object InputManager extends InputListener {
    override def touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) = {
      fire(ButtonUp(state))
    }

    override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
      fire(ButtonDown(state))
      true
    }
  }

  object ButtonStateManager extends EventListener {
    override def handle(event: Event): Boolean = {
      event match {
        case ChangeButtonState(newState) =>
          changeState(newState)
          true
        case _ => false
      }
    }
  }

}

case class PasswordTextField(textField: TextField) extends Group {
  var count = 0
  addActor(textField)
  textField.setPasswordMode(true)
  textField.setPasswordCharacter('*')

  object ClickManager extends ClickListener {
    override def clicked(event: InputEvent, x: Float, y: Float) = {
      textField.setText("")
    }
  }

  addListener(ClickManager)

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}

case class StateHandleButton(value: (String, StateFullButton)) extends Group {
  addActor(value._2)
  value._2.addListener(new EventListener {
    override def handle(event: Event): Boolean = event match {
      case ButtonUp(state) =>
        event.getTarget.fire(ChangeButtonState(value._2.getState.tail.head))
        true
      case ButtonDown(state) => event.getTarget.fire(ChangeButtonState(value._2.getState.head))
        fire(ButtonHandle(value._1, event))
        true
      case _ => false
    }
  })

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}

class StateLabelHandleButton(prop: (String, StateFullButton, Label)) extends StateHandleButton(prop._1, prop._2) {
  addActor(prop._3)
  prop._3.setTouchable(Touchable.disabled)
  prop._3.setPosition(value._2.getX + value._2.getWidth / 2.35f, value._2.getY + value._2.getHeight / 3.5f)

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}

case class MessagePanel(value: (String, CustomImageLoader, Array[(String, CustomImageLoader)])) extends Group {
  var refName = value._3.take(1).head._1
  addActor(value._2)
  addActor(value._3.take(1).head._2)

  def addSubMsg(name: String) = {
    removeActor(value._3.find(_._1 == refName).get._2)
    addActor(value._3.find(_._1 == name).get._2)
    refName = name
  }

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}

case class ButtonHandler(msg: String, event: Event, passwordTextField: PasswordTextField) extends Event

case class PinPanel(value: (String, PasswordTextField, Array[StateLabelHandleButton], Array[StateHandleButton])) extends Group {

  addActor(value._2)
  value._3.foreach(z => addActor(z))
  value._4.foreach(z => addActor(z))
  addCaptureListener(new EventListener {
    override def handle(event: Event): Boolean = event match {
      case ButtonHandle(msg, event) =>
        fire(ButtonHandler(msg, event, value._2))
        true
      case _ => false
    }
  })

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}