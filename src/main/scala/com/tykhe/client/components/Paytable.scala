package com.tykhe.client.components

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Group, Actor}
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.tykhe.client.components.buttons.PortraitLoader
import com.tykhe.client.poker.components.button.StateImageButton
import com.typesafe.config.Config

/**
 * Created by aravind on 23/3/15.
 */
class Paytable(config:Config,backButton:StateImageButton) extends Group{
  var flag=false;
  var payTable: PortraitLoader = new PortraitLoader(config.getString("path"))
  setBounds(config.getLong("x"), config.getLong("y"), payTable.texture.getWidth(), payTable.texture.getHeight())
  payTable.setPosition(getX(), getY())

  def removePaytable()={
    flag=false
      removeActor(backButton)
  }
  def addPaytable()={
    flag=true
    addActor(backButton)
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    if(flag) {
      batch.draw(payTable.texture, payTable.getX(), payTable.getY())
    }
    super.draw(batch, parentAlpha)
  }
}
