package com.tykhe.client.model

import com.tykhe.client.utils.PaymentCard

object Model {
  var paymentCard:PaymentCard=null

  def updateAmount(amount:BigDecimal)=paymentCard=PaymentCard(paymentCard.cardId,amount,paymentCard.password)
}
