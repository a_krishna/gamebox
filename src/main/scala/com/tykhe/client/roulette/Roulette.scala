package com.tykhe.client.roulette

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.tykhe.client.components.buttons.PortraitLoader
import com.tykhe.client.main.HomeScreen
import com.tykhe.client.poker.components.button.{StateImageButton, ButtonLayoutSkin}
import com.typesafe.config.Config

/**
 * Created by aravind on 23/3/15.
 */
class Roulette(config: Config) extends Group {
  val skin = new ButtonLayoutSkin(config.getConfig("layoutSkin"))
  val backGround = new PortraitLoader(config.getString("background.path"))
  val payTable = new PortraitLoader(config.getString("payTable.path"))
  var homeScreen: HomeScreen = null
  val menuButton = new CustomButton(config.getObject("menuButton").toConfig(), skin.skin, this)
  val helpButton = new CustomButton(config.getObject("helpButton").toConfig(), skin.skin, this)
  val backButton = new CustomButton(config.getObject("backButton").toConfig(), skin.skin, this)
  backGround.setPosition(config.getLong("background.x"), config.getLong("background.y"))
  payTable.setPosition(config.getLong("payTable.x"), config.getLong("payTable.y"))
  addActor(backGround)
  addActor(menuButton)
  addActor(helpButton)


  def addPaytable(): Unit = {
    addActor(payTable)
    addActor(backButton)
  }

  def removePaytable(): Unit = {
    removeActor(payTable)
    removeActor(backButton)
  }

  def setHome(home: HomeScreen) = {
    this.homeScreen = home
  }

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)

}
