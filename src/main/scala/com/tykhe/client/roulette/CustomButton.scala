package com.tykhe.client.roulette

import java.util.ArrayList

import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.{Event, EventListener, Group}
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.tykhe.client.components.buttons.{ButtonUp, ChangeButtonState, ButtonDown, StatefulImageButton}
import com.typesafe.config.Config

/**
 * Created by aravind on 23/3/15.
 */
class CustomButton(config: Config, skin: Skin,roulette: Roulette) extends  Group{
  var states = config.getStringList("states").asInstanceOf[ArrayList[String]]
  var statefullImageButton = new StatefulImageButton(skin, states)
  var label: BitmapFont = new BitmapFont
  var text: String = ""
  var xText=0;
  var yText=10;
  var stateFlag=false
  setPosition(config.getLong("x"), config.getLong("y"))
  println("ImageButton width= " + getWidth() + " height=" + getHeight())
  addActor(statefullImageButton)
  (config.getString("type") match {
    case "MENU" => mainmenuHandle
    case "HELP" => helpHandle
    case "BACK" =>  backHandle
  })
  def helpHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag){

            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              event.getTarget().fire(new ChangeButtonState(states.get(1)))
            }
            return true
          }else{
            return false
          }
        } else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) {
            roulette.addPaytable()
            event.getTarget().fire(new ChangeButtonState(states.get(0)))
          }
          return true
        }
        return false
      }
    })
  }
  def backHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag){

            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              event.getTarget().fire(new ChangeButtonState(states.get(1)))
            }
            return true
          }else{
            return false
          }
        } else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) {
            roulette.removePaytable()
            event.getTarget().fire(new ChangeButtonState(states.get(0)))
          }
          return true
        }
        return false
      }
    })
  }
  def mainmenuHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag){

            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              event.getTarget().fire(new ChangeButtonState(states.get(1)))
            }
            return true
          }else{
            return false
          }
        } else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) {
            roulette.homeScreen.drawHome()
            event.getTarget().fire(new ChangeButtonState(states.get(0)))
          }
          return true
        }
        return false
      }
    })
  }
}

