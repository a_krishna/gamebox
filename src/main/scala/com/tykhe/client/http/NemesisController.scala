package com.tykhe.client.http

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.badlogic.gdx.utils.Json
import com.tykhe.client.utils._

class StatusResponse{
  var status:String=null
  var message:String=null
}

class NemesisController() extends Actor with ActorLogging{
  val nemesisHttpClient = context.actorOf(Props[ NemesisCardClient],"http-client")
  val json=new Json()
  var actorRef:ActorRef=null
  def receive={
    case Initialize =>
      log.info(this.getClass.getName+" Intialized")
      actorRef =sender()
      sender() ! Initialized
    case SendCardRequest(path,card)=>
      nemesisHttpClient ! SendCardRequest(path,card)
    case SendAccountRequest(path,card)=>
      nemesisHttpClient ! SendAccountRequest(path,card)
    case SendCardTransaction(path,transaction)=>
      nemesisHttpClient ! SendCardTransaction(path,transaction)
    case ReceivedError(msg)=>
      msg.contains("{") match {
        case true =>
          val temp=msg.split(""","details""").head
          val jsonObject=json.fromJson(classOf[StatusResponse],temp.substring(temp.indexOf("{"),temp.toCharArray.length)+msg.last)
          jsonObject.message match {
            case "mismatch"=>actorRef ! Mismatch
            case "authentication failed"=>actorRef ! Failed
            case "invalid card"=>actorRef ! InvalidCard
            case "invalid pin"=>actorRef ! InvalidPin
            case "imbalance"=>actorRef ! Imbalance
            case "error"=>actorRef ! ConnectionTimedOut
            case _=>actorRef ! ConnectionTimedOut
          }
        case false=>actorRef ! ConnectionTimedOut
      }
    case ReceivedPayemntCard(msg,card)=>
      msg match {
        case "succeeded"=> actorRef ! Succeeded(card)
        case "password updated"=> actorRef ! Updated(card)
        case "retrieved"=>
          log.info("NC RECV"+msg)
          actorRef !  Retrieved(card)
        case _=>log.info(" RPC "+msg)
      }
    case ReceivedCardTransaction(msg,transaction)=>
      msg match {
        case "charged"=>actorRef ! Charged(transaction)
        case "debited"=>actorRef ! Debited(transaction)
        case "credited"=>actorRef ! Credited(transaction)
        case _=>log.info(" RCT "+msg)
      }
  }
}