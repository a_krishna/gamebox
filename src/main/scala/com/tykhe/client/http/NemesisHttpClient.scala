package com.tykhe.client.http

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorLogging, Actor}
import akka.util.Timeout
import com.tykhe.client.utils._

import spray.client.pipelining._
import spray.http.{Uri, HttpMethods, HttpRequest, HttpEntity}
import spray.httpx.marshalling._
import spray.json.{JsonFormat, DefaultJsonProtocol}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}


object JsonMapperProtocol extends DefaultJsonProtocol{
  implicit val account = jsonFormat4(Account)
  implicit val paymentCard = jsonFormat3(PaymentCard)
  implicit val cardRequest = jsonFormat3(CardRequest)
  implicit val processTransaction= jsonFormat4(ProcessTransaction)
  implicit  def response[T: JsonFormat] = jsonFormat3(Response.apply[T])
}

class NemesisCardClient() extends Actor with ActorLogging{
  var actorRef:ActorRef=null
  val host = "http://"+context.system.settings.config.getString("nemesis.terminal.payment-server")

  import  JsonMapperProtocol._
  import spray.httpx.SprayJsonSupport._


  implicit  val executionContext=ExecutionContext.global

  implicit  val timeout = Timeout.apply(scala.concurrent.duration.pairLongToDuration((2500l,TimeUnit.MILLISECONDS)))

  val pipelinePaymentCard = sendReceive ~> unmarshal[Response[PaymentCard]]

  val pipelineTransaction = sendReceive ~> unmarshal[Response[ProcessTransaction]]

  def processCardReq= (msg:(String,HttpEntity)) => {
    val req=HttpRequest(HttpMethods.POST,Uri.apply(host+msg._1),entity = msg._2)
    log.info("SendCardReq "+req)
    pipelinePaymentCard{req}
  }

  def processTransactionReq= (msg:(String,HttpEntity)) => {
    val req=HttpRequest(HttpMethods.POST,Uri.apply(host+msg._1),entity = msg._2)
    log.info("SendTransactionReq "+req)
    pipelineTransaction{req}
  }

  def receive={
    case SendCardRequest(path,value)=>
      actorRef=sender()
      processCardReq((path,marshal(value).right.toOption.get))  onComplete{
        case Success(response)=>log.info("Received MSG "+response)
          actorRef  ! ReceivedPayemntCard(response.message,response.details)
        case Failure(error)=>
          log.error("Received BAD REQUEST "+error)
          actorRef  ! ReceivedError(error.getMessage)
      }
    case SendAccountRequest(path,value)=>
      actorRef=sender()
      processCardReq((path,marshal(value).right.toOption.get)) onComplete{
        case Success(response)=>log.info("Received MSG "+response)
          actorRef  ! ReceivedPayemntCard(response.message,response.details)
        case Failure(error)=>
          log.error("Received BAD REQUEST "+error)
          actorRef  ! ReceivedError(error.getMessage)
      }
    case SendCardTransaction(path,value)=>

      processTransactionReq((path,marshal(value).right.toOption.get)) onComplete{
        case Success(response)=>log.info("Received MSG "+response)
          actorRef  ! ReceivedCardTransaction(response.message,response.details)
        case Failure(error)=>
          log.error("Received BAD REQUEST "+error)
          actorRef  ! ReceivedError(error.getMessage)
      }
  }
}
