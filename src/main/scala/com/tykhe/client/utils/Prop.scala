package com.tykhe.client.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{Color, Texture}
import com.badlogic.gdx.graphics.g2d.{TextureAtlas, Batch}
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.{Label, TextField, Button, Skin}
import com.tykhe.client.components.buttons._
import com.tykhe.client.components.meter.{FontShadow, FontBorder, FontProp, GenerateFont}

case class CustomImageLoader(value:(TextureAtlas.AtlasRegion,Float,Float)) extends Actor{
  override def draw(batch: Batch,parentAlpha:Float)= {
    super.draw(batch, parentAlpha)
    batch.draw(value._1,value._2,value._3)
  }
}
object Prop {
  var skinName="buttonskin/pinpanel.json"
  var bebasNeue="font/BebasNeue.ttf"
  var width=1680
  var height=1050
  val msgMapper= Map("pin"->"invalid_pin","mismatch"->"invalid_card","balance"->"insufficient_fund","card"->"invalid_card","timed"->"timed_out","insert"->"insert_card")
  val uriMapper = Map("view"->"/card/retrieve","credit"->"/card/credit","debit"->"/card/debit")
  val MESSAGE= List("message_panel","insert_card","invalid_card","invalid_pin","timed_out","insufficient_fund")
  val CAL_BUTTONS=List("1","2","3","4","5","6","7","8","9","0")
  val pinPadding=List(-179,0,179).zip(Seq.fill(3){148}) ++ List(-179,0,179).zip(Seq.fill(3){0}) ++ List(-179,0,179).zip(Seq.fill(3){-148}) ++ List((0,-296))
  val pinPanelSkin=new Skin(Gdx.files.internal(skinName))
  val calNumFont=GenerateFont.decoratedFontStyle( FontProp(bebasNeue,40,Color.BLACK),(FontBorder(Color.BLUE,1,false,1),FontShadow(Color.CLEAR,0,0,0))  )
  val labelButtons :String => Array[(String,Button.ButtonStyle)] = z=>   Array(z+ "_up",z+"_down").foldLeft(Array.empty[(String,ButtonStyle)])((z,a)=> z ++ Array( (a, pinPanelSkin.get(a,classOf[ImageButtonStyle]) )))

  val messagePanel= {
    val panels=loadMessagePanel()
    MessagePanel("message_panel",panels.head._2,panels.drop(1))
  }
  val pinPanel= PinPanel("pinPanel",PasswordTextField(loadTextField((0,296),"")),loadCalNumber,loadButtonNumber)

  def loadMessagePanel()= {
    MESSAGE.foldLeft(Array.empty[(String,CustomImageLoader)])((t,s)=>t++ Array(loadSkinPanel(s)))
  }
  def loadSkinPanel(value:String)={
    val msg_panel=pinPanelSkin.getAtlas.findRegion(value)
    (value,CustomImageLoader(msg_panel,width/2-msg_panel.getRegionWidth/2,height/2-msg_panel.getRegionHeight/2))
  }

  def loadCalNumber= CAL_BUTTONS.zip(pinPadding).foldLeft(Array.empty[StateLabelHandleButton])((g,v)=>g ++ Array(new StateLabelHandleButton(v._1,loadStateFullButton("cal_button",v._2),new Label(v._1,calNumFont))))

  def loadButtonNumber= Array(StateHandleButton("clear",loadStateFullButton("clear",(-179,-296))),StateHandleButton("ok",loadStateFullButton("ok",(179,-296))))

  def loadStateFullButton(name:String,padding:(Int,Int))={
    val reg= pinPanelSkin.getAtlas.findRegion(labelButtons(name).head._1)
    val statButton = new StateFullButton(pinPanelSkin,labelButtons(name))
    statButton.setPosition((width/2-reg.getRegionWidth/2)+padding._1, (height/2-reg.getRegionHeight/2)+padding._2)
    statButton
  }

  def loadTextField(padding:(Int,Int),text:String) ={
    val reg= pinPanelSkin.getAtlas.findRegion("cal_label")
   val textField=new TextField(text,pinPanelSkin)
    textField.setAlignment(1)
    textField.setBounds((width/2-reg.getRegionWidth/2)+padding._1, (height/2-reg.getRegionHeight/2)+padding._2,reg.getRegionWidth,reg.getRegionHeight)
    textField
  }
}
