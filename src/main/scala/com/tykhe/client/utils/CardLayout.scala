package com.tykhe.client.utils

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.tykhe.client.slot.{APPCARDSTATE, APPSTATE}

object CardLayoutState{
  var state:APPSTATE=APPCARDSTATE.INSERTCARD
}

case class CardLayout() extends Group{
  var refPanel:String=null

  val msgPanel=Prop.messagePanel
  val pinFunctionalPanel=Prop.pinPanel

 def showPanel(panel:String)={
    panel match {
      case "message_panel"=>refPanel match {
        case "pinPanel"=>removeActor(pinFunctionalPanel)
          addActor(msgPanel)
        case "message_panel" =>
        case _=>addActor(msgPanel)
      }
      case "pinPanel"=>refPanel match {
        case "message_panel"=> removeActor(msgPanel)
          addActor(pinFunctionalPanel)
        case "pinPanel" =>
        case _=>addActor(pinFunctionalPanel)
      }
      case _=>
    }
   refPanel=panel
 }

  override def draw(batch:Batch,parentAlpha:Float)=super.draw(batch,parentAlpha)
}
