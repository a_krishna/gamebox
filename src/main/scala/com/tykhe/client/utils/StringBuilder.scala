package com.tykhe.client.utils

import java.security.SecureRandom

object StringBuilder {
  val random = (0 to 18).toArray

  var transNo=10

  def generate = random.foldLeft("")((z, a) => z + new SecureRandom().nextInt(10))

  def generateCard = random.drop(3).foldLeft("")((z, a) => z + new SecureRandom().nextInt(10))

  def generateTransactionId = "TER-"+java.util.UUID.randomUUID.toString
}
