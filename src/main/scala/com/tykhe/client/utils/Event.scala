package com.tykhe.client.utils


case class ProcessTransaction(transactionId:String,cardId:String,amount:BigDecimal,processBy:String)
case class Account(transactionId:String,cardId:String,password:Option[String],processBy:String)
case class PaymentCard(cardId: String, balance: BigDecimal, password:Option[String]= None)
case class CardRequest(transactionId:String,cardId:String,issuedBy:String)
case class Response[T](status:String,message:String,details:T)

trait Event
trait EventRequest extends Event
object Initialize extends EventRequest
case class SendCardRequest(path:String,cardRequest: CardRequest) extends EventRequest
case class SendAccountRequest(path:String,account: Account) extends EventRequest
case class SendCardTransaction(path:String,processTransaction: ProcessTransaction) extends EventRequest

trait EventResponse extends Event
object Initialized extends EventResponse
case class ReceivedError(message:String) extends EventResponse
case class ReceivedPayemntCard(message:String,paymentCard: PaymentCard) extends EventResponse
case class ReceivedCardTransaction(message:String,processTransaction: ProcessTransaction) extends EventResponse
object ConnectionTimedOut extends EventResponse

trait RetrieveCard
case class Retrieved(paymentCard: PaymentCard) extends RetrieveCard with EventResponse
object Mismatch extends RetrieveCard with EventResponse

trait Deposit
case class Charged(processTransaction: ProcessTransaction) extends Deposit with EventResponse
case class Credited(processTransaction: ProcessTransaction) extends Deposit with EventResponse
object InvalidCard extends Deposit with EventResponse

trait Redeem
case class Debited(processTransaction: ProcessTransaction) extends Redeem with EventResponse
object Imbalance extends Redeem with EventResponse

trait Credit
object CoinIn  extends Credit with EventRequest
object CoinOut extends Credit with EventRequest

trait UpdatePassword
case class Updated(paymentCard: PaymentCard) extends UpdatePassword with EventResponse
object InvalidPin extends UpdatePassword with EventResponse

trait Authenticate
case class Succeeded(paymentCard: PaymentCard) extends Authenticate with EventResponse
object Failed extends Authenticate with EventResponse

trait DEEvent
object RegisterDevices extends DEEvent with EventRequest
object RegisterCRT extends DEEvent with EventRequest
object RegisterUBA extends DEEvent with EventRequest
object RegisterPAYCHECK extends DEEvent with EventRequest
object CRTRegistered extends DEEvent with  EventResponse
object UBAVRegistered extends DEEvent with  EventResponse
object PAYCHECKRegistered extends DEEvent with  EventResponse
object DevicesRegisted extends DEEvent with EventResponse


trait CRTEVENT
object InitializeCRT extends CRTEVENT with EventRequest
case class CRTOperation(cmd:String) extends CRTEVENT with EventRequest
case class CardDetected(cardNumber:String) extends CRTEVENT with EventRequest
object ResetCRT extends CRTEVENT with EventRequest
object CRTReset extends CRTEVENT with EventResponse
object EnabledCRT extends CRTEVENT with EventResponse
object DisabledCRT extends CRTEVENT with EventResponse
object CRTPowerDown extends CRTEVENT with EventResponse

trait FunctionalEvent
case class EventDeposit(amt:BigDecimal) extends FunctionalEvent with EventRequest
object EventWithdraw extends FunctionalEvent with EventRequest
case class EventUpdatePassword(pin:String) extends FunctionalEvent with EventRequest
object EventRetrieveCard extends FunctionalEvent with EventRequest
case class EventAuthenticate(pin:String) extends FunctionalEvent with EventRequest





