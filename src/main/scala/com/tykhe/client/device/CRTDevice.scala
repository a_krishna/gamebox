package com.tykhe.client.device

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.{ExecutorService, Executors, TimeUnit}

import akka.actor.{Actor, ActorLogging, ActorRef, _}
import com.tykhe.client.utils._
import nemesis.device.crt.Error.POWER_DOWN
import nemesis.device.crt.MagCard.Data
import nemesis.device.crt.Operation.Status
import nemesis.device.crt.Positive._
import nemesis.device.crt.Status.RELEASED
import nemesis.device.crt._
import nemesis.device.usb._

import scalaz.concurrent.Strategy
import scalaz.stream.{Process, async, wye}

class CRTDevice extends Actor with ActorLogging {
  val reg = """[A-Z]|[a-z]""".r
  var controller: ActorRef = null
  implicit val executor = Executors.newFixedThreadPool(4)
  implicit val strategy = Strategy.Executor

  val scheduler = Executors.newScheduledThreadPool(1)
  val descriptors = Seq(driver.descriptor)

  val select = MagCard.Select.TRACK_1
  val auto = MagCard.Auto.ON

  val commands = Map(
    "s" -> Command(Operation.Status),
    "i" -> Command(Operation.Reset(locked = false)),
    "a" -> MagCard.Configure(select = select, auto = MagCard.Auto.ON),
    "r" -> MagCard.Read(select = select),
    "c" -> Command(MagCard.Clear))

  val process = ports(scheduler, descriptors)
    .take(1)
    .flatMap(_.open(reads = 4))
    .map(driver.open)
    .flatMap(_.run(w, wye.Request.Both))
    .map(self ! _)

  val initializer = Process.emitAll(Seq(
    Command(Operation.Reset(locked = false)),
    MagCard.Configure(auto = MagCard.Auto.ON, select = MagCard.Select.TRACK_1),
    LED.Switch(LED.Color.RED, LED.State.OFF),
    LED.Switch(LED.Color.BLUE, LED.State.ON)))

  val q = async.unboundedQueue[Command]
  val w = (initializer ++ q.dequeue)

  override def preStart(): Unit = {
    process.run.runAsync{
      case result => log.info(s"device process completed with result: $result")
    }
  }

  def shutdown(executor: ExecutorService) = {
    executor.shutdown()
    executor.awaitTermination(2, TimeUnit.SECONDS)
  }

  override def postStop()={
    log.info("SHUTTING DOWN CRT")
    shutdown(scheduler)
    shutdown(executor)
  }

  def receive = {
    case InitializeCRT =>
      controller = sender()
    case CRTOperation(msg) => q.enqueueOne(commands(msg)).run
    case Data(op, status, tracks) => tracks.head match {
      case Left(err) => log.info("" + err)
      case Right(card) if card.split("\\^").head != null =>
        log.info(card)
        controller ! CardDetected(reg.replaceAllIn(card.split("\\^").head, ""))
    }
    case Success(op, status) => log.info(op + " " + status)
      controller != null match {
        case true => op match {
          case Status => status match {
            case RELEASED => controller ! CRTRegistered
            case _ =>
          }
          case _ => controller ! EnabledCRT
        }
        case false =>
      }
    case Reset(op, status, value) => log.info(op + " " + status)
      status match {
        case RELEASED => controller ! CRTReset
        case _ =>
      }
    case Negative(_, _) => log.info("NEGATIVE RESPONSE")
      controller != null match {
        case true =>
          controller ! DisabledCRT
        case false =>
      }
    case Serial(status, num) => log.info("Serial " + status)
    case POWER_DOWN => controller ! CRTPowerDown
    case Error => controller ! DisabledCRT
    case _ => log.info("DEFAULT CRT ")
  }
}
