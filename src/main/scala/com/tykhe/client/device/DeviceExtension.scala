package com.tykhe.client.device

import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import com.tykhe.client.utils._

class DeviceExtension  extends Actor with ActorLogging{
  val crtRef=context.actorOf(Props[CRTDevice],"extension-crt")
  var controller:ActorRef=null
  def  receive={
    case RegisterDevices=>
      controller=sender()
      self ! RegisterCRT
    case RegisterCRT=>
      crtRef ! InitializeCRT
    case RegisterUBA=> //TODO
    case RegisterPAYCHECK=> //TODO
    case CRTRegistered=>
      log.info("CRT REGISTERED")
      controller ! DevicesRegisted
    case UBAVRegistered=> //TODO
    case PAYCHECKRegistered=> //TODO
    case CRTReset=>
      log.info("RESET")
    case ResetCRT=>crtRef ! CRTOperation("i")
    case EnabledCRT=>log.info("ENABLE")
      self ! CRTRegistered
    case DisabledCRT=>crtRef ! CRTOperation("a")
    case CRTPowerDown =>
      self ! ResetCRT
      self ! DisabledCRT
    case CardDetected(cardNum)=>
      log.info(this.getClass.getSimpleName+""+cardNum)
      controller ! CardDetected(cardNum)
    case _=>
  }
}
