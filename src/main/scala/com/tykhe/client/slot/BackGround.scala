package com.tykhe.client.slot

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.tykhe.client.components.sound.Sound

import scala.collection.mutable.ArrayBuffer

object BoxConfig{
  def prepareBoxes(middleBackgroundConfig: MiddleBackgroundConfig):ArrayBuffer[ArrayBuffer[Box]]={
    val boxes:ArrayBuffer[ArrayBuffer[Box]]=ArrayBuffer.fill(middleBackgroundConfig.size,middleBackgroundConfig.size)(new Box)
    var padX=middleBackgroundConfig.x
    boxes.foreach{z=>
      var padY=middleBackgroundConfig.y
      z.foreach { y=>
        y.texture=new Texture(Gdx.files.internal(middleBackgroundConfig.texturePath))
        y.sound=new Sound(middleBackgroundConfig.soundPath)
        y.setPosition(padX,padY)
        y.enable
        padY+=middleBackgroundConfig.paddingHeight
      }
      padX+=middleBackgroundConfig.paddingWidth
  }
    boxes
  }
}

case class MiddleBackgroundConfig(x:Float,y:Float,paddingHeight:Float,paddingWidth:Float,size:Int,texturePath:String,soundPath:String)

class Box() extends Actor{
  var flag=false
  var texture:Texture=null
  var sound:Sound=null

  override  def draw(batch:Batch,parentAlpha:Float)={
    super.draw(batch,parentAlpha)
    flag match {
      case true =>batch.draw(texture,getX,getY)
      case false =>
    }
  }

  def enable= flag=true

  def disable={
    flag=false
    sound.playSound()
  }
}

class BoxHandler(middleBackgroundConfig: MiddleBackgroundConfig){
  val boxes:ArrayBuffer[ArrayBuffer[Box]]=BoxConfig.prepareBoxes(middleBackgroundConfig)
}