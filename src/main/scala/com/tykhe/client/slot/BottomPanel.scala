package com.tykhe.client.slot

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.tykhe.client.components.GAMESTATE
import com.tykhe.client.components.buttons.PortraitLoader
import com.tykhe.client.poker.components.button.ButtonLayout
import com.tykhe.cliet.util.ScreenShotFactory
import com.typesafe.config.Config

class BottomPanel(config: Config, slotGame: SlotGame) extends Group {
  var bottomPanel: PortraitLoader = new PortraitLoader(config.getString("path"))
  var enterFlag,shiftFlag=false
  bottomPanel.setPosition(config.getLong("x"), config.getLong("y"))
  var buttonLayout = new ButtonLayout(config.getObject("buttonLayout").toConfig())
  buttonLayout.setGameState(slotGame, GAMESTATE.SLOTGAME)
  addActor(buttonLayout)
  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    batch.draw(bottomPanel.texture, bottomPanel.getX(), bottomPanel.getY())
    super.draw(batch, parentAlpha)
    if(Gdx.input.isKeyPressed(Keys.ENTER)){
      if(!enterFlag){
        println("Enter Pressed")
        enterFlag=true;
        buttonLayout.dealButton.buttonDeckEvent
        shiftFlag=false;
      }
    }else if(Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT)){
      if(!shiftFlag){
        shiftFlag=true;
        println("SHIFT PRESSED")
        ScreenShotFactory.saveScreenshot()
        enterFlag=false;
      }
    }
  }
}