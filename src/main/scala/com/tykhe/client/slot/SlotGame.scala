package com.tykhe.client.slot

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Event, EventListener, Group}
import com.tykhe.client.components.Paytable
import com.tykhe.client.components.buttons.PortraitLoader
import com.tykhe.client.components.sound.Sound
import com.tykhe.client.debug.DebugLayer
import com.tykhe.client.jackpot._
import com.tykhe.client.main.HomeScreen
import com.tykhe.client.slot.components.Barrel
import com.tykhe.client.utils.CardLayout
import com.typesafe.config.{Config, ConfigFactory}
import spine.secondary.game.{BonusCompleteDataEvent, SecondaryGames}
import spine.{SpineAnimationCollection, SpineAnimationState}

trait APPSTATE

object APPCARDSTATE {

  case object INSERTCARD extends APPSTATE

  case object AUTHENTICATION extends APPSTATE

  case object AUTHENTICATED extends APPSTATE

}

class SlotGame(config: Config) extends Group {
  val spineCollection = new SpineAnimationCollection(config)
  val spineScene = spineCollection.loadScene(config.getString("slotBackground.animationName"))
  var slotBackground = new PortraitLoader(config.getString("slotBackground.path"))
  slotBackground.setPosition(config.getLong("slotBackground.x"), config.getLong("slotBackground.y"))
  var backgroundSound = new Sound(config.getString("slotBackground.sound"))
  backgroundSound.setSoundSetting(config.getLong("slotBackground.volume"), config.getLong("slotBackground.pitch"), config.getLong("slotBackground.pan"))
  var cardLayout: CardLayout = null
  var jackpotLabels = Array.empty[JackpotLabels]
  val bonusGame: SecondaryGames = new SecondaryGames()
  for (i <- 0 to config.getObjectList("bonus").size() - 1) {
    val entryObject = config.getObjectList("bonus").get(i).entrySet()
    val ref = entryObject.iterator().next()
    bonusGame.addSecondaryGame(ref.getKey, config.getConfig(ref.getValue.unwrapped().toString), spineCollection.loadScene(ref.getKey))
  }
  bonusGame.addEventListener
  backgroundSound.flag = true
  var payLineState: String = "ABOVE"
  var slotPayTable: SlotPayTable = new SlotPayTable(config.getConfig("slotPayTable"))
  var barrel = new Barrel(config)
  var homeScreen: HomeScreen = null
  var debugLayer: DebugLayer = new DebugLayer(config.getConfig("debug"))
  var boxHandler: BoxHandler = null

  addActor(slotBackground)
  addActor(spineScene.spineAnimation)
  config.getBoolean("slotBackground.middleLayer") match {
    case true => val boxLayer = new PortraitLoader(config.getString("slotBackground.middlePath.path"))
      boxLayer.setPosition(config.getLong("slotBackground.middlePath.x"), config.getLong("slotBackground.middlePath.y"))
      addActor(boxLayer)
    case false =>
  }
  config.getBoolean("slotBackground.sub.rootFlag") match {
    case true =>
      boxHandler = new BoxHandler(MiddleBackgroundConfig(config.getLong("slotBackground.sub.x"), config.getLong("slotBackground.sub.y"), config.getLong("slotBackground.sub.paddingHeight"), config.getLong("slotBackground.sub.paddingWidth"), config.getInt("slotBackground.sub.size"), config.getString("slotBackground.sub.texturePath"), config.getString("slotBackground.sub.soundPath")))
      boxHandler.boxes.foreach(z => z.foreach(x => addActor(x)))
    case false =>
  }
  barrel.setBoxHandler(boxHandler)
  addActor(barrel)
  barrel.setPayLineGroup("ABOVE", this)
  addActor(new TopPanel(config.getConfig("toppanel")))
  val bottomPanel = new BottomPanel(config.getConfig("bottompanel"), this)
  addActor(bottomPanel)
  barrel.initSlotTextField(this)
  Jackpots.defaultJPvalue = bottomPanel.buttonLayout.payTable.wins.filter(_.typeOfWin == "Progressive")
  Jackpots.jps = LoadJP._load(ConfigFactory.load("jackpots.conf"))
  Jackpots.loadPJP()
  val payTable = new Paytable(config.getConfig("slotPayTable"), bottomPanel.buttonLayout.back)
  addActor(slotPayTable)
  val jackpotsConf = config.getConfigList("jackpots")
  jackpotLabels = new Array[JackpotLabels](jackpotsConf.size())
  for (i <- 0 to jackpotsConf.size() - 1) {
    val prop = jackpotsConf.get(i)
    val jpLabel = JackpotLabel(prop.getConfig("prop"), bottomPanel.buttonLayout.buttonLayoutSkin.skin)
    jackpotLabels(i) = JackpotLabels((prop.getString("name"), jpLabel))
    addActor(jpLabel.label)
  }
  setJackpots()
  playAnimation
  stopBackground()
  bonusGame.addListener(new EventListener {
    override def handle(event: Event): Boolean = {
      if (event.isInstanceOf[BonusCompleteDataEvent]) {
        println("Bonus Completed" + event.asInstanceOf[BonusCompleteDataEvent].bonusName + " " + event.asInstanceOf[BonusCompleteDataEvent].amountWin)
        removeBonusRound()
        barrel.bonusCompeleteEvent(event.asInstanceOf[BonusCompleteDataEvent].amountWin.toInt)
        return true
      }
      return false
    }
  })

  def playAnimation = spineScene.spineAnimation.STATE = SpineAnimationState.PLAY

  def pauseAnimation = spineScene.spineAnimation.STATE = SpineAnimationState.PAUSE

  def stopAnimation = spineScene.spineAnimation.STATE = SpineAnimationState.STOP

  def addBonusRound(name: String): Unit = {
    bonusGame.addBonusActor(name)
    addActorBefore(slotPayTable, bonusGame)
    //    addActorBefore(payTable,bonusGame)
    //    addActor(bonusGame)
    stopBackground()
    pauseAnimation
  }

  def removeBonusRound(): Unit = {
    bonusGame.removeBonusActor()
    removeActor(bonusGame)
    playAnimation
  }

  def playBackground() = {
    backgroundSound.playSound
  }

  def stopBackground() = {
    backgroundSound.stopSound
  }

  def updateJackpots() = {
    Jackpots.update()
    Jackpots.progrssiveJackpots.foreach { jPJ =>
      println(" [ JP ] " + jPJ._1)
      val jpL = jackpotLabels.find(_.prop._1 == jPJ._1)
      jpL match {
        case Some(x) => x.prop._2.label.setText("%2f".format(jPJ._2 * TotalBet.denom * TotalBet.coin).toDouble.toString)
        case None => println("JP  NONE " + jackpotLabels.size)
      }
    }
  }

  def onDenomChangeUpdateJackpots() = {
    Jackpots.progrssiveJackpots.foreach { jPJ =>
      val jpL = jackpotLabels.find(_.prop._1 == jPJ._1)
      jpL match {
        case Some(x) => x.prop._2.label.setText("%2f".format(jPJ._2 * TotalBet.denom * TotalBet.coin).toDouble.toString)
        case None => println("JP  NONE " + jackpotLabels.size)
      }
    }
  }

  def setJackpots() = {
    TotalBet.denom = bottomPanel.buttonLayout.denom.buttonValue
    TotalBet.coin = bottomPanel.buttonLayout.coin.buttonValue
    onDenomChangeUpdateJackpots()
  }

  def setHome(home: HomeScreen) = {
    this.homeScreen = home
  }

  def addPayTable() = {
    //    addActor(payTable)
    //    payTable.addPaytable()
  }

  def removePayTable() = {
    //    removeActor(payTable)
    //    payTable.removePaytable()
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)

}