package com.tykhe.client.slot

import java.awt.{Dimension, Toolkit}
import java.lang.Boolean
import java.nio.IntBuffer

import com.badlogic.gdx.Application.ApplicationType
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics._
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.StretchViewport
import com.badlogic.gdx.{ApplicationAdapter, Gdx}
import com.tykhe.client.components.GAMESTATE
import com.tykhe.client.controller.Controller
import com.tykhe.client.main.GameStateChange
import com.tykhe.client.model.Model
import com.tykhe.client.slot.APPCARDSTATE.{AUTHENTICATED, AUTHENTICATION, INSERTCARD}
import com.tykhe.client.utils.{PaymentCard, CardLayoutState, CardLayout}
import com.typesafe.config.{Config, ConfigFactory}
import org.lwjgl.input.{Cursor, Mouse}
import org.lwjgl.{BufferUtils, LWJGLException}

class SlotApp(config: Config, controller: Controller) extends ApplicationAdapter {
  var stage: Stage = null
  var emptyCursor: Cursor = null
  var slotGame: SlotGame = null

  override def create() = {
    stage = new Stage(new StretchViewport(config.getInt("screenX"), config.getInt("screenY")), new PolygonSpriteBatch())
    Gdx.input.setInputProcessor(stage)
    val cardlayout = CardLayout()
    slotGame = new SlotGame(config)
    stage.addActor(slotGame)
    Model.paymentCard=PaymentCard("5241780002550254",2000,Some("1234"))
    CardLayoutState.state=AUTHENTICATED
    slotGame.cardLayout = cardlayout
    controller.slotGameS = Array(slotGame)
    controller.slotGameHandler()
    CardLayoutState. state match {
      case INSERTCARD=>slotGame.cardLayout.showPanel(slotGame.cardLayout.msgPanel.value._1)
      case AUTHENTICATION=> slotGame.cardLayout.showPanel(slotGame.cardLayout.pinFunctionalPanel.value._1)
      case AUTHENTICATED=>slotGame.cardLayout.showPanel("NONE")
    }
    Model.paymentCard !=null match {
      case true=> slotGame.bottomPanel.buttonLayout.cashMeter.setAmount(Model.paymentCard.balance.toInt)
      case false=>slotGame.bottomPanel.buttonLayout.cashMeter.setAmount(0)
    }
    slotGame.fire(GameStateChange(GAMESTATE.SLOTGAME))
    setHWCursorVisible(false)
    //    GLProfiler.enable()
  }

  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act(Gdx.graphics.getDeltaTime)
    stage.draw()
  }

  override def resize(width: Int, height: Int) = {
    stage.getViewport.update(width, height)
  }


  @throws(classOf[LWJGLException])
  def setHWCursorVisible(visible: Boolean): Unit = {
    if (Gdx.app.getType() != ApplicationType.Desktop
      && Gdx.app.isInstanceOf[LwjglApplication])
      return;
    if (emptyCursor == null) {
      if (Mouse.isCreated()) {
        var min: Int = org.lwjgl.input.Cursor.getMinCursorSize();
        var tmp: IntBuffer = BufferUtils.createIntBuffer(min * min);
        emptyCursor = new Cursor(min, min, min / 2,
          min / 2, 1, tmp, null);
      } else {
        throw new LWJGLException(
          "Could not create empty cursor before Mouse object is created");
      }
    }
    if (Mouse.isInsideWindow()) Mouse.setNativeCursor(if (visible) emptyCursor else null)
  }
}

object SlotMain {
  val appConfig: Config = ConfigFactory.load("caseno_slot.conf")
  val cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration()
  val screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize()
  cfg.title = "Slots"
  cfg.vSyncEnabled = true
  cfg.width = screenDimension.getWidth.toInt
  cfg.height = screenDimension.getHeight.toInt
  // cfg.useGL20 = true;
  cfg.fullscreen = appConfig.getBoolean("slot.fullscreen")
}
