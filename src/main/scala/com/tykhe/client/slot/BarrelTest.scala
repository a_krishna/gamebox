package com.tykhe.client.slot

import java.awt.{Dimension, Toolkit}

import com.badlogic.gdx.{ApplicationAdapter, Gdx}
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.Stage
import com.typesafe.config.{Config, ConfigFactory}

class BarrelHolder(config: Config) extends ApplicationAdapter {
  var stage: Stage = null

  override def create() = {
    stage = new Stage
    Gdx.input.setInputProcessor(stage)
    stage.addActor(new SlotGame(config))
  }
  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act(Gdx.graphics.getDeltaTime())
    stage.draw()
  }
}

object BarrelTest {

  def main(args: Array[String]): Unit = {
    var appConfig: Config = ConfigFactory.load("fruit_slot.conf")
    println(appConfig.toString())
    var cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration();
    var screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize();
    cfg.title = "Buttons";
    cfg.vSyncEnabled = true;
    // cfg.width = screenDimension.width;
    // cfg.height = screenDimension.height;
    // cfg.useGL20 = true;
    if (!cfg.fullscreen) {
      cfg.width = 1280
      cfg.height = 1024
    }
    new LwjglApplication(new BarrelHolder(appConfig.getConfig("slot")), cfg)
  }

}