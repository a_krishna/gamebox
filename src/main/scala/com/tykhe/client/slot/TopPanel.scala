package com.tykhe.client.slot

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.tykhe.client.components.buttons.PortraitLoader
import com.typesafe.config.Config

class TopPanel(config: Config) extends Group {

  var topPanel: PortraitLoader = new PortraitLoader(config.getString("path"))
  topPanel.setPosition(config.getLong("x"), config.getLong("y"))
  addActor(topPanel)

  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)
}