package com.tykhe.client.slot

import java.util.{ArrayList, List}

import com.tykhe.client.components.meter.Meters

class SlotMeter extends Meters {
  var totalMeter: Float = 0
  var payLine: List[PayLineWin] = null

  def newSpin(credits: Float, denom: Float) = {
    setMeter(credits, denom)
    payLine = new ArrayList
  }

  def addWinAmount(winPay: Float, line: String,lineNum:Int) {
    payLine.add(new PayLineWin(line, winAmount(winPay),lineNum))
    totalMeter += winAmount(winPay)
  }
  def getAmountByName(name:String):Float= {
    for (i <- 0 to payLine.size()-1) {
      if (payLine.get(i).getLine.equals(name)) {
        return payLine.get(i).getAmount
      }
    }
    return 0f
  }
  }
class PayLineWin(line: String, amount: Float,lineNum:Int) {
  def getLine = line
  def getAmount = amount
  def getLineNum= lineNum
}