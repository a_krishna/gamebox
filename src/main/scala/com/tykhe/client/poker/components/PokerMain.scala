package com.tykhe.client.poker.components

import java.awt.{Dimension, Toolkit}
import java.lang.Boolean
import java.nio.IntBuffer

import com.badlogic.gdx.Application.ApplicationType
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.{ApplicationAdapter, Gdx}
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.{Camera, GL20}
import com.badlogic.gdx.math.{Rectangle, Vector2}
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.{StretchViewport, Viewport}
import com.tykhe.client.components.GAMESTATE
import com.tykhe.client.controller.Controller
import com.tykhe.client.main.GameStateChange
import com.tykhe.client.model.Model
import com.tykhe.client.slot.APPCARDSTATE.{AUTHENTICATED, AUTHENTICATION, INSERTCARD}
import com.tykhe.client.utils.{PaymentCard, CardLayoutState, CardLayout}
import com.typesafe.config.{Config, ConfigFactory}
import org.lwjgl.{BufferUtils, LWJGLException}
import org.lwjgl.input.{Cursor, Mouse}

class PokerApp(config: Config,controller: Controller) extends ApplicationAdapter {
  val VIRTUAL_WIDTH: Int = 1920
  val VIRTUAL_HEIGHT: Int = 1080
  val ASPECT_RATIO: Float = VIRTUAL_WIDTH.toFloat / VIRTUAL_HEIGHT.toFloat
  var viewPort: Rectangle = null
  var stage: Stage = null
  var emptyCursor: Cursor = null
  var camera: Camera = null
  override def create() = {
    stage =new Stage(new StretchViewport(config.getInt("screenX"),config.getInt("screenY")),new PolygonSpriteBatch())
    Gdx.input.setInputProcessor(stage)
    val cardLayout=CardLayout()
    val pokerGame=new PokerGame(config)
    stage.addActor(pokerGame)
    Model.paymentCard=PaymentCard("5241780002550254",2000,Some("1234"))
    CardLayoutState.state=AUTHENTICATED
    pokerGame.cardLayout=cardLayout
    pokerGame.cardLayout.setPosition(0,1050)
    controller.pokerGame=pokerGame
    controller.pokerGameHandler() 
    CardLayoutState. state match {
      case INSERTCARD=>pokerGame.cardLayout.showPanel(pokerGame.cardLayout.msgPanel.value._1)
      case AUTHENTICATION=> pokerGame.cardLayout.showPanel(pokerGame.cardLayout.pinFunctionalPanel.value._1)
      case AUTHENTICATED=>pokerGame.cardLayout.showPanel("NONE")
    }
    Model.paymentCard !=null match {
      case true=> pokerGame.buttonLayout.cashMeter.setAmount(Model.paymentCard.balance.toInt)
      case false=>pokerGame.buttonLayout.cashMeter.setAmount(0)
    }
    pokerGame.fire(GameStateChange(GAMESTATE.POKERGAME))
    setHWCursorVisible(false)
  }
  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act()
    stage.draw()
  }
  @throws(classOf[LWJGLException])
  def setHWCursorVisible(visible: Boolean): Unit = {
    if (Gdx.app.getType() != ApplicationType.Desktop
      && Gdx.app.isInstanceOf[LwjglApplication])
      return 
    if (emptyCursor == null) {
      if (Mouse.isCreated()) {
        var min: Int = org.lwjgl.input.Cursor.getMinCursorSize()
        var tmp: IntBuffer = BufferUtils.createIntBuffer(min * min)
        emptyCursor = new Cursor(min, min, min / 2,
          min / 2, 1, tmp, null)
      } else {
        throw new LWJGLException(
          "Could not create empty cursor before Mouse object is created")
      }
    }
    if (Mouse.isInsideWindow()) Mouse.setNativeCursor(if (visible) emptyCursor else null)
  }
}
object PokerMain {
  var appConfig: Config = ConfigFactory.load("poker")
  var cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration()
  var screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize()
  cfg.title = "Buttons"
  cfg.vSyncEnabled = true
  cfg.width = screenDimension.width
  cfg.height = screenDimension.height
  // cfg.useGL20 = true
  cfg.fullscreen = appConfig.getBoolean("poker.fullscreen")
}
