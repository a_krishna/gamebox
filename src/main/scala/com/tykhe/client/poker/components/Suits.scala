package com.tykhe.client.poker.components

import java.util.ArrayList

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Event, EventListener, Group, InputEvent}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.tykhe.client.components.buttons.PortraitLoader
import com.tykhe.client.components.sound.Sound
import com.typesafe.config.Config

class Suits(cards: ArrayList[Card], config: Config) extends Group {
  var paddingPosy: Float = 0
  var cardPos = 0
  var cardSound: Sound = new Sound(config.getString("cardsound"))
  var clickSound: Sound = new Sound(config.getString("clicksound"))
  var flag = false
  var deck = DECK.DEFAULT.toString()
  var hold: PortraitLoader = null
  var card: String = null
  var originalPosY: Float = 0
  def init() = {
    hold = new PortraitLoader(config.getString("path"))
    hold.setFlag(true)
    hold.setBounds(config.getLong("x"), config.getLong("y"), config.getLong("width"), config.getLong("height"))
    addActor(hold)
    originalPosY = getY()
    addListener(new StateManager)
    addListener(new ClickListener {
      override def clicked(event: InputEvent, x: Float, y: Float) {
        if (deck.equals(DECK.OPEN.toString())) fireEvent(DECK.HOLD.toString()) else if (deck.equals(DECK.HOLD.toString())) fireEvent(DECK.OPEN.toString())
      }
    })
  }
  def fireEvent(deck: String) = {
    deck match {
      case "OPEN" =>
        hold.setFlag(true); clickSound.playSound; setY(originalPosY + paddingPosy)
      case "HOLD" =>
        hold.setFlag(false); clickSound.playSound; setY(originalPosY)
      case "DRAW" =>
        hold.setFlag(true); setY(originalPosY)
      case _ => throw new RuntimeException("State unchnaged")
    }
    fire(new ChangeState(deck))
  }
  def moveDown() = {
    if (getY() != originalPosY) {
      cardSound.playSound
      setY(originalPosY)
    }
  }
  def moveUp() {
    cardSound.playSound
    setY(originalPosY + paddingPosy)
  }
  def searchCard(symobol: String) = {
    println(symobol + "   " + deck)
    card = symobol
    if (deck.equals(DECK.DEFAULT.toString())) reset else if (deck.equals(DECK.OPEN.toString())) for (i <- 0 to cards.size() - 1) if (cards.get(i).name.equalsIgnoreCase(symobol)) cardPos = i
  }
  def reset() = {
    card = null
    cardPos = 0
    hold.setFlag(true)
  }
  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
    batch.draw(cards.get(cardPos).image, getX(), getY())
  }

  class StateManager extends EventListener {

    override def handle(event: Event): Boolean = {
      if (event.isInstanceOf[ChangeState]) {
        deck = event.asInstanceOf[ChangeState].newState
        return true
      }
      return false
    }
  }

  class ChangeState(deck: String) extends Event {
    var newState = deck
  }

}