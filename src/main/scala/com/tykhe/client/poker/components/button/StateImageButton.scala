package com.tykhe.client.poker.components.button

import java.util.ArrayList

import com.badlogic.gdx.graphics.g2d.{Batch, BitmapFont}
import com.badlogic.gdx.scenes.scene2d.{Event, EventListener, Group}
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.tykhe.client.components.{GAMESTATE, SLOTGAMESTATE}
import com.tykhe.client.components.buttons._
import com.tykhe.client.model.Model
import com.typesafe.config.Config

class StateImageButton(config: Config, skin: Skin, buttonLayout: ButtonLayout) extends Group {
  var states = config.getStringList("states").asInstanceOf[ArrayList[String]]
  var statefullImageButton = new StatefulImageButton(skin, states)
  var label: BitmapFont = new BitmapFont
  var text: String = ""
  var xText=0;
  var yText=10;
  var stateFlag=false
  var mainFunctionalState=false
  setPosition(config.getLong("x"), config.getLong("y"))
  println("ImageButton width= " + getWidth() + " height=" + getHeight())
  addActor(statefullImageButton)
  if (states.size() > 2) dealHandle else (config.getString("type") match {
    case "SPIN" => spinHandle
    case "CASH" => cashHandle
    case "MENU" => mainmenuHandle
    case "HELP" => helpHandle
    case "BACK" =>  backHandle()
    case "BONUS" =>
      text = "Bonus"; xText=GameSetup.labelPos._1;defaultmenuHandle(config.getString("type"))// xText=10 for laptop screen resolution
    case "GAMBLE" =>
      text = "Gamble"; xText=GameSetup.labelPos._1;defaultmenuHandle(config.getString("type"))//xText=5 for laptop screen resolution
    case "LINES" => text = "Lines"; xText=15;yText=5;defaultmenuHandle(config.getString("type")) //xText=15 for laptop screen resolution
  })
  def dealHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if (!mainFunctionalState) {
            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              buttonLayout.creditState() match {
                case true =>
                  event.getTarget().fire(new ChangeButtonState(states.get(1)))
                case false =>
              }
            } else if (event.asInstanceOf[ButtonDown].state.equals(states.get(2))) event.getTarget().fire(new ChangeButtonState(states.get(3)))
            return true
          }
        }else if (event.isInstanceOf[ButtonUp]) {
            if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) {
              buttonLayout.playDeel
              buttonLayout.dealPressed
              event.getTarget().fire(new ChangeButtonState(states.get(2)))
            } else if (event.asInstanceOf[ButtonUp].state.equals(states.get(3))) {
              buttonLayout.playDraw
              buttonLayout.drawPressed
              event.getTarget().fire(new ChangeButtonState(states.get(0)))
            }
            return true
          }
          return false
      }
    })
  }
  def cashHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag && !mainFunctionalState){
            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              Model.paymentCard !=null && Model.paymentCard.balance > 0 match {
                case true=>
                  buttonLayout.attachListenerToCard()
                  event.getTarget().fire(new ChangeButtonState(states.get(1)))
                case false =>
              }
            }
            return true
          }else{
            return false
          }
        } else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) event.getTarget().fire(new ChangeButtonState(states.get(0)))
          return true
        }
        return false
      }
    })
  }
  def spinHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag && !mainFunctionalState){
            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              buttonLayout.creditState() match {
                case true=>
                  event.getTarget().fire(new ChangeButtonState(states.get(1)))
                  buttonLayout.spin(event)
                case false=>
              }
            }
            return true
          }else{
            return false
          }
        } else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1)))
            return true
        }
        return false
      }
    })
  }
  def mainmenuHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag && !mainFunctionalState ){

            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              event.getTarget().fire(new ChangeButtonState(states.get(1)))
            }
            return true
          }else{
            return false
          }
        }
        else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) {
            buttonLayout.mainMenu
            event.getTarget().fire(new ChangeButtonState(states.get(0)))
          }
          return true
        }
        return false
      }
    })
  }

  def helpHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag && !mainFunctionalState){
            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              event.getTarget().fire(new ChangeButtonState(states.get(1)))
            }
            return true
          }else{
            return false
          }
        } else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) {
            if(buttonLayout.state==GAMESTATE.SLOTGAME){
              buttonLayout.slotGame.addPayTable()
            }else if(buttonLayout.state==GAMESTATE.POKERGAME){
                buttonLayout.pokerDeck.pokerGame.addPayTable()
            }
            event.getTarget().fire(new ChangeButtonState(states.get(0)))
          }
          return true
        }
        return false
      }
    })
  }

  def backHandle() {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonDown]) {
          if(!stateFlag){

            if (event.asInstanceOf[ButtonDown].state.equals(states.get(0))) {
              event.getTarget().fire(new ChangeButtonState(states.get(1)))
            }
            return true
          }else{
            return false
          }
        } else if (event.isInstanceOf[ButtonUp]) {
          if (event.asInstanceOf[ButtonUp].state.equals(states.get(1))) {
            if(buttonLayout.state==GAMESTATE.SLOTGAME){
              buttonLayout.slotGame.removePayTable()
            }
            else if(buttonLayout.state==GAMESTATE.POKERGAME){
              buttonLayout.pokerDeck.pokerGame.removePayTable()
            }
            event.getTarget().fire(new ChangeButtonState(states.get(0)))
          }
          return true
        }
        return false
      }
    })
  }


  def defaultmenuHandle(buttonType:String) {
    statefullImageButton.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if (event.isInstanceOf[ButtonPress]) {
          if(!stateFlag){
            if (event.asInstanceOf[ButtonPress].stateName.equals(states.get(0))) {
              event.getTarget().fire(new ChangeButtonState(states.get(1)))
              buttonType match {
                case "GAMBLE"=>buttonLayout.GAMBLE_STATE=TOGGLESTATE.OFF
                case "BONUS"=>buttonLayout.BONUS_STATE=TOGGLESTATE.OFF
                case _=>
              }
              return true
            }else if (event.asInstanceOf[ButtonPress].stateName.equals(states.get(1))) {
              event.getTarget().fire(new ChangeButtonState(states.get(0)))
              buttonType match {
                case "GAMBLE"=>buttonLayout.GAMBLE_STATE=TOGGLESTATE.ON
                case "BONUS"=>buttonLayout.BONUS_STATE=TOGGLESTATE.ON
                case _=>
              }
              return true
            }
          }else{
            return false
          }
        }
        return false
      }
    })
  }

  def buttonDeckEvent={
    if(!stateFlag){
      val event:Event=new Event()
      statefullImageButton.fire(new ButtonDown(states .get(0)))
    }
  }
  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
    label.draw(batch, text, getX() + xText, getY() - yText)
  }

}