package com.tykhe.client.poker.components

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Actor, Group}
import com.tykhe.client.components.{Paytable, GAMESTATE}
import com.tykhe.client.debug.DebugLayer
import com.tykhe.client.main.{Container, HomeScreen}
import com.tykhe.client.model.Model
import com.tykhe.client.poker.components.button.ButtonLayout
import com.tykhe.client.slot.APPCARDSTATE.{INSERTCARD, AUTHENTICATION, AUTHENTICATED}
import com.tykhe.client.slot.{APPCARDSTATE, APPSTATE}
import com.tykhe.client.utils.CardLayout
import com.typesafe.config.Config

import scala.collection.mutable.ListBuffer

class PokerGame(config: Config) extends Group {
  var pokerBackGroud = new PokerBackGround(config.getObject("background").toConfig())
  var pokerDeck = new PokerDeck(config)
  var logo = new PokerBackGround(config.getConfig("gameLogo"))
  var homeScreen: HomeScreen = null
  var payTable = new PokerPayTable(config.getConfig("payTable"))
  //    logo.background.debugFlag = true
  var buttonLayout = new ButtonLayout(config.getObject("buttonLayout").toConfig())
  buttonLayout.setGameState(pokerDeck, GAMESTATE.POKERGAME)
  var paytable = new Paytable(config.getConfig("payTable"), buttonLayout.back)
  var debugLayer: DebugLayer = new DebugLayer(config.getConfig("debug"))
  var cardLayout:CardLayout=null
  addActor(pokerBackGroud)
  addActor(logo)
  addActor(pokerDeck)
  addActor(buttonLayout)
  addActor(debugLayer)
  addActor(payTable)
  pokerDeck.setPokerGame(this)

  def setHome(home: HomeScreen) = {
    this.homeScreen = home
  }

  def addPayTable(): Unit = {
//    addActor(paytable)
//    paytable.addPaytable()
  }

  def removePayTable(): Unit = {
//    removeActor(paytable)
//    paytable.removePaytable()
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)
}