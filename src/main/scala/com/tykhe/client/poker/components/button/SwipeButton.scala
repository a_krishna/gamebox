package com.tykhe.client.poker.components.button

import java.util.ArrayList

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Group, InputEvent}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.tykhe.client.components.LineTemp
import com.tykhe.client.components.buttons.PortraitLoader
import com.tykhe.client.components.sound.Sound
import com.typesafe.config.Config

class SwipeButton(config: Config, buttonLayout: ButtonLayout, name: String) extends Group {
  private var padding: Float = config.getLong("padding")
  var flag: Boolean = false
  var images = new ArrayList[PortraitLoader]()
  var sound: Sound = new Sound(config.getString("sound"))
  var cullingPosition: Float = 0
  var files = config.getStringList("path")
  var values = config.getIntList("value")
  var buttonValue = 0
  var dealFlag = false
  setPosition(config.getLong("x"), config.getLong("y"))
  init
  def init() {
    println("Size of files" + files.size)
    var tmp: Int = 0;
    var paddingY = getY()
    for (file <- files.toArray()) {
      val texture = new PortraitLoader(file.toString())
      texture.setBounds(getX(), getY(), texture.texture.getWidth(), texture.texture.getHeight())
      texture.box = new LineTemp(getX(), getY(), texture.texture.getWidth(), texture.texture.getHeight(), 3, Color.RED)
      texture.setFlag(true)
      texture.boundCorrection = true
      texture.debugFlag = false
      texture.text = config.getString("name")
      addActor(texture)
      images.add(texture)
    }
    images.get(0).boundCorrection = false
    buttonValue = values.get(0)
    addListener(new ClickListener {
      override def clicked(event: InputEvent, x: Float, y: Float) {
        if (!dealFlag) {
          if (tmp == files.size - 1) flag = true
          if (flag) if (tmp == 0) flag = false
          name match {
            case "payLine" => tmp = if (flag == false) tmp + 1 else (files.size()-1) - tmp
            case _ => tmp = if (flag == false) tmp + 1 else tmp - 1
          }

          for (i <- 0 to images.size() - 1) {
            if (tmp == i) {
              images.get(i).boundCorrection = false
              buttonValue = values.get(tmp)
              buttonLayout.compute(name)
            } else {

              images.get(i).boundCorrection = true
            }

          }
          sound.playSound
        }
      }

    })
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
  }

}
