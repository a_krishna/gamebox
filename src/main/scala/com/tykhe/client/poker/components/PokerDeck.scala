package com.tykhe.client.poker.components

import java.util.ArrayList

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.tykhe.client.main.HomeScreen
import com.typesafe.config.Config
import org.tykhe.poker.{Poker, Win}

class PokerDeck(config: Config) extends Group {
  var cards = new java.util.ArrayList[Card]()
  var pokerDeck = config.getObjectList("pokerdeck")
  var poker: Poker = new Poker
  var flag = false
  var pokerGame: PokerGame = null
  var suits = new ArrayList[Suits](config.getInt("suits"))
  for (i <- 0 to pokerDeck.size() - 1) cards.add(new Card(pokerDeck.get(i).toConfig()))
  var paddingX = config.getInt("x")
  for (i <- 0 to (config.getInt("suits") - 1)) addSuits(new Suits(cards, config.getObject("hold").toConfig()))
  def addSuits(suit: Suits) = {
    suit.setBounds(paddingX, config.getInt("y"), cards.get(0).image.getWidth(), cards.get(0).image.getHeight())
    suit.init
    suit.paddingPosy = config.getInt("paddingPosy")
    suits.add(suit)
    addActor(suit)
    paddingX += config.getInt("width") + config.getInt("paddingX")
  }
  def deel() = {
    //draw five card
    if (!flag) {

      var deck = poker.drawFiveCard()
      for (i <- 0 to suits.size() - 1) {
        suits.get(i).fireEvent(DECK.OPEN.toString())
        suits.get(i).searchCard(deck(i))
        suits.get(i).moveUp
      }
      println(Win.gethandResult(deck))
      flag = true
    }
    //check the suits and set the card
    //win result
  }
  def reset() = {
    suits.toArray().foreach(suit => suit.asInstanceOf[Suits].fireEvent(DECK.DEFAULT.toString()))
  }
  def draw(): String = {
    // check hold and set card on unhold suit 
    //win result
    if (flag) {

      val holdCard = new Array[String](5)
      for (i <- 0 to suits.size() - 1) {
        if (!suits.get(i).hold.getFlag) {
          holdCard(i) = suits.get(i).card
        } else {
          val tmp = poker.drawHand(1)
          suits.get(i).searchCard(tmp(0))
          holdCard(i) = tmp(0)
        }
        suits.get(i).moveDown
        suits.get(i).fireEvent(DECK.DRAW.toString())
      }
      val handResult: String = Win.gethandResult(holdCard)
      flag = false
      return handResult
      // reset
    }
    return "NOTHING"
    //credit upate
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)

  def setPokerGame(pokerGame: PokerGame) = {
    this.pokerGame = pokerGame
  }
}