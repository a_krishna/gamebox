package com.tykhe.client.poker.components
object DECK extends Enumeration {
  type DECK = Value
  val DEFAULT, OPEN, HOLD, DRAW = Value
}