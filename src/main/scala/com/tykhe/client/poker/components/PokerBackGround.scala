package com.tykhe.client.poker.components

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.tykhe.client.components.buttons.PortraitLoader
import com.typesafe.config.Config

class PokerBackGround(config: Config) extends Group {

  var background: PortraitLoader = new PortraitLoader(config.getString("path"))
  setBounds(config.getLong("x"), config.getLong("y"), config.getLong("width"), config.getLong("height"))
  background.setBounds(config.getLong("x"), config.getLong("y"), config.getLong("width"), config.getLong("height"))
  addActor(background)
  println("Background =" + getWidth())
  override def draw(batch: Batch, parentAlpha: Float): Unit = super.draw(batch, parentAlpha)
}