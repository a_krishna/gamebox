package com.tykhe.client.poker.components

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.Actor
import com.typesafe.config.Config

class Card(config: Config) extends Actor {
  var name = config.getString("name")
  var image = new Texture(Gdx.files.internal(config.getString("path")))

  // override def draw(batch: Batch, parentAlpha: Float): Unit = batch.draw(image, getX(), getY())
  override def toString(): String = {
    return "[Name= " + name + " Path= " + image.getTextureData() + " ]"
  }
}