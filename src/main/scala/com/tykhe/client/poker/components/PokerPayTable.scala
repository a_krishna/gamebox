package com.tykhe.client.poker.components

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.tykhe.client.components.buttons.PortraitLoader
import com.typesafe.config.Config

class PokerPayTable(config: Config) extends Actor {
  var payTable: PortraitLoader = new PortraitLoader(config.getString("path"))
  setPosition(config.getLong("x"), config.getLong("y"))
  payTable.setPosition(getX(), getY())

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
    batch.draw(payTable.texture, payTable.getX(), payTable.getY())
  }
}