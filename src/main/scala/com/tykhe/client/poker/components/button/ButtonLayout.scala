package com.tykhe.client.poker.components.button

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.{Batch, TextureAtlas}
import com.badlogic.gdx.scenes.scene2d.ui.{Label, Skin}
import com.badlogic.gdx.scenes.scene2d.{Event, Group}
import com.badlogic.gdx.utils.Timer
import com.tykhe.client.components.buttons._
import com.tykhe.client.components.meter._
import com.tykhe.client.components.{GAMESTATE, POKERGAMESTATE, SLOTGAMESTATE}
import com.tykhe.client.jackpot.TotalBet
import com.tykhe.client.main.CASHOUT
import com.tykhe.client.model.Model
import com.tykhe.client.poker.components.PokerDeck
import com.tykhe.client.slot.APPCARDSTATE.INSERTCARD
import com.tykhe.client.slot.SlotGame
import com.tykhe.client.utils.CardLayoutState
import com.typesafe.config.{Config, ConfigFactory}
import org.tykhe.poker.credits.Credits
import org.tykhe.slot.paytable.Paytable
import spine.SpineAnimationCollection

sealed trait TOGGLESTATE

object TOGGLESTATE {

  case object ON extends TOGGLESTATE

  case object OFF extends TOGGLESTATE

}

case class WinGroup() extends Group

class ButtonLayout(config: Config) extends Group {
  GameSetup.setLabelPos(config.getConfig("gameSetup"))
  val winGroup: WinGroup = new WinGroup
  var pokerDeck: PokerDeck = null
  var slotGame: SlotGame = null
  var payTable: Paytable = null
  var slotGameState = SLOTGAMESTATE.FREE
  var pokerGameState = POKERGAMESTATE.FREE
  var slotSpinState = SLOTGAMESTATE.SPIN
  var lastWinResult = 1
  var state = GAMESTATE.HOME
  var timer, winLineTimer: Timer = new Timer
  var buttonLayoutSkin = new ButtonLayoutSkin(config)
  var credits: Credits = new Credits
  var dealButton: StateImageButton = new StateImageButton(config.getObject("dealButton").toConfig(), buttonLayoutSkin.skin, this)
  var cashButton: StateImageButton = new StateImageButton(config.getObject("cashButton").toConfig(), buttonLayoutSkin.skin, this)
  var menuButton: StateImageButton = new StateImageButton(config.getObject("menuButton").toConfig(), buttonLayoutSkin.skin, this)
  var helpButton: StateImageButton = new StateImageButton(config.getObject("helpButton").toConfig(), buttonLayoutSkin.skin, this)
  var back: StateImageButton = new StateImageButton(config.getObject("back").toConfig(), buttonLayoutSkin.skin, this)
  var coin = new SwipeButton(config.getConfig("coin"), this, "coin")
  var denom = new SwipeButton(config.getObject("denom").toConfig(), this, "denom")
  var payLine: SwipeButton = null
  var totalMeter = new Meter(config.getObject("totalmeter").toConfig(), buttonLayoutSkin.skin)
  var winMeter = new Meter(config.getObject("winmeter").toConfig(), buttonLayoutSkin.skin)
  var cashMeter = new Meter(config.getObject("cashmeter").toConfig(), buttonLayoutSkin.skin)
  var winHighlights = new Label("", buttonLayoutSkin.skin)
  winHighlights.setStyle(GenerateFont.decoratedFontStyle(font = (FontProp("font/ostrich-bold.ttf", 60, Color.YELLOW), (FontBorder(Color.valueOf("EDD72F"), 2, false, 2), FontShadow(Color.CLEAR, 0, 0, 0)))))
  var event: Event = null
  var GAMBLE_STATE: TOGGLESTATE = TOGGLESTATE.ON
  var BONUS_STATE: TOGGLESTATE = TOGGLESTATE.ON
  var gamble, bonus: StateImageButton = null
  Model.paymentCard != null match {
    case true => cashMeter.setAmount(Model.paymentCard.balance.toInt)
    case false =>
  }
  setWinHighlights
  addActor(coin)
  addActor(denom)
  addActor(dealButton)
  addActor(cashButton)
  addActor(menuButton)
  addActor(helpButton)
  addActor(totalMeter)
  addActor(winMeter)
  addActor(cashMeter)
  addActor(winHighlights)
  compute("None")

  def dealPressed() = {
    coin.dealFlag = true
    denom.dealFlag = true
    dealButton.stateFlag = true
    cashButton.stateFlag = true
    menuButton.stateFlag = true
    helpButton.stateFlag = true
  }

  def drawPressed() = {
    coin.dealFlag = false
    denom.dealFlag = false
    dealButton.stateFlag = false
    cashButton.stateFlag = false
    menuButton.stateFlag = false
    helpButton.stateFlag = false
  }

  def compute(name: String) = {
    state match {
      case GAMESTATE.HOME => totalMeter.setAmount((coin.buttonValue * denom.buttonValue))
      case GAMESTATE.SLOTGAME =>
        name match {
          case "coin" => totalMeter.setAmount((coin.buttonValue * denom.buttonValue) * payLine.buttonValue)
            TotalBet.coin = coin.buttonValue
            slotGame.onDenomChangeUpdateJackpots()
          case "denom" => totalMeter.setAmount((coin.buttonValue * denom.buttonValue) * payLine.buttonValue)
            TotalBet.denom = denom.buttonValue
            slotGame.onDenomChangeUpdateJackpots()
          case "payLine" =>
            totalMeter.setAmount((coin.buttonValue * denom.buttonValue) * payLine.buttonValue)
            slotGame.payLineState match {
              case "DOWN" =>
                slotGameState match {
                  case SLOTGAMESTATE.FREE => if (slotGame.barrel.getSlotMeter().payLine.size() > 0) {
                    slotGame.barrel.winLineBar("REMOVE-RECREAT", slotGame, slotGame.barrel.getPayLines().getLines().get(lastWinResult), winGroup)
                  }
                  case _ => println("I Shouldn't come here after first spin")
                }
                slotGame.payLineState = "ABOVE"
                slotGame.barrel.setPayLineGroup("ABOVE", slotGame)
              case _ =>
            }
            slotGame.barrel.getPayLines().selectPayline(payLine.buttonValue)
          case _ =>
            totalMeter.setAmount((coin.buttonValue * denom.buttonValue) * payLine.buttonValue)
        }

      case GAMESTATE.POKERGAME => totalMeter.setAmount((coin.buttonValue * denom.buttonValue))
    }
  }

  def mainMenu() {
    state match {
      case GAMESTATE.SLOTGAME => slotGame.homeScreen != null match {
        case true => slotGame.homeScreen.drawHome()
        case false =>
      }
      case GAMESTATE.POKERGAME => pokerDeck.pokerGame.homeScreen != null match {
        case true => pokerDeck.pokerGame.homeScreen.drawHome()
        case false =>
      }
      case GAMESTATE.HOME =>
      case _ =>
    }
  }

  def playDeel() = {
    pokerGameState = POKERGAMESTATE.ON_GAME
    pokerDeck.deel
    Model.updateAmount(Model.paymentCard.balance - totalMeter.credits)
    cashMeter.setAmount(Model.paymentCard.balance.toInt)
    winMeter.reset
    winHighlights.setText("")
  }

  def playDraw() = {
    pokerGameState = POKERGAMESTATE.FREE
    val handResult = pokerDeck.draw
    val winAmt = credits.getCreditForBet(coin.buttonValue, handResult)
    if (winAmt > 0) winMeter.setAmount((denom.buttonValue * winAmt))
    Model.updateAmount(Model.paymentCard.balance + winMeter.credits)
    cashMeter.setAmount(Model.paymentCard.balance.toInt)
    println("Hand Result%%%" + handResult)
    if (handResult.split("_").length < 2) setWinHightlightsPos(config.getLong("highlights.oneText"), handResult) else if (handResult.split("_").length < 3) setWinHightlightsPos(config.getLong("highlights.twoText"), handResult.replace("_", " ")) else setWinHightlightsPos(config.getLong("highlights.x"), handResult.replace("_", " "))
    winMeter.credits = 0
    Model.paymentCard.balance > 0 match {
      case true =>
      case false => changeSlotGameState(SLOTGAMESTATE.EMPTY_CREDITS)
    }
  }

  def setWinHightlightsPos(posX: Float, handResult: String) = {
    winHighlights.setX(posX)
    if (handResult.equals("NOTHING")) winHighlights.setText("") else winHighlights.setText(handResult)
  }

  def setWinHighlights() = {
    winHighlights.setPosition(config.getLong("highlights.x"), config.getLong("highlights.y"))
    winHighlights.setFontScale(config.getLong("highlights.size"))
    //    winHighlights.setColor(config.getLong("highlights.color.r"), config.getLong("highlights.color.g"), config.getLong("highlights.color.b"), config.getLong("highlights.color.a"))
  }

  def cashIn() = {
    cashMeter.setAmount(Model.paymentCard.balance.toInt)
    changeSlotGameState(SLOTGAMESTATE.AMOUNT_CREDITED)
  }


  def cashOut() = {
    cashMeter.setAmount(0)
    changeSlotGameState(SLOTGAMESTATE.EMPTY_CREDITS)
  }

  def attachListenerToCard() = {
    state match {
      case GAMESTATE.SLOTGAME => slotGameState match {
        case SLOTGAMESTATE.FREE =>
          println("SLOT STATE")
          slotGame.fire(CASHOUT)
        case _ =>
      }
      case GAMESTATE.POKERGAME => pokerGameState match {
        case POKERGAMESTATE.FREE => println("POKER STATE")
          pokerDeck.pokerGame.fire(CASHOUT)
        case _ =>
      }
      case _ => println("HOME STATE")
    }
  }

  def spin(event: Event) = {
    Model.paymentCard.balance <= (0) match {
      case true =>
      case false =>
        TotalBet.setBet(_denom = denom.buttonValue, _coin = coin.buttonValue, _line = payLine.buttonValue)
        slotGame.barrel.removeWinLine()
        Model.updateAmount(Model.paymentCard.balance - totalMeter.credits)
        cashMeter.setAmount(Model.paymentCard.balance.toInt)
        slotGame.updateJackpots()
        winMeter.setAmount(0)
        slotSpinState match {
          case SLOTGAMESTATE.SPIN_STOP =>
            if (slotGame.barrel.getSlotMeter().payLine.size() > 0) {
              slotGame.barrel.winLineBar("REMOVE-RECREAT", slotGame, slotGame.barrel.getPayLines().getLines().get(lastWinResult), winGroup)
            }
          case _ =>
        }
        changeSlotGameState(SLOTGAMESTATE.SPIN)
        slotGame.barrel.getSlotMeter().newSpin(coin.buttonValue, denom.buttonValue)
        slotGame.barrel.setPayLineGroup("BELOW", slotGame)
        this.event = event
        slotGame.barrel.spin(this)
        println("Spin Button Pressed")
    }
  }

  def stopComplete(): Boolean = {
    println("Trigger stopCompelete")
    timer.clear()
    var task = new com.badlogic.gdx.utils.Timer.Task {
      override def run = {
        slotGameState match {
          case SLOTGAMESTATE.FREE =>
            slotGame.playBackground
            Model.paymentCard.balance > 0 match {
              case true =>
              case false => changeSlotGameState(SLOTGAMESTATE.EMPTY_CREDITS)
            }
          case _ =>
        }

      }
    }
    timer.scheduleTask(task, 2f)
    //    slotResult
    slotGame.barrel.setPayLineGroup("DOWN", slotGame)
    return false
  }

  def addWinAmount(amt: Float) = winMeter.setAmount(amt.toInt)

  def addCashAmount() = {
    println("Adding Cash ")
    Model.updateAmount(Model.paymentCard.balance + winMeter.credits)
    cashMeter.setAmount(Model.paymentCard.balance.toInt)
  }

  def setGameState(game: Object, state: GAMESTATE): Unit = {
    this.state = state
    state match {
      case GAMESTATE.HOME => println("Home")
      case GAMESTATE.POKERGAME =>
        pokerDeck = game.asInstanceOf[PokerDeck]; setPokerPanel
      case GAMESTATE.SLOTGAME => slotGame = game.asInstanceOf[SlotGame];
        setSlotPanel
        payTable == null match {
          case true =>
            payTable = new Paytable(ConfigFactory.load(config.getString("paytable")))
          case false =>
        }
    }
  }

  def setSlotPanel() = {
    payLine = new SwipeButton(config.getObject("payLine").toConfig(), this, "payLine")
    gamble = new StateImageButton(config.getObject("gamble").toConfig(), buttonLayoutSkin.skin, this)
    bonus = new StateImageButton(config.getObject("bonus").toConfig(), buttonLayoutSkin.skin, this)
    var autoPlay: PortraitLoader = new PortraitLoader(config.getString("autoplay.path"))
    autoPlay.setPosition(config.getLong("autoplay.x"), config.getLong("autoplay.y"))
    addActor(gamble)
    addActor(bonus)
    addActor(payLine)
    addActor(autoPlay)
    slotGame.playBackground
  }

  def setPokerPanel() = {
    var gamble: StateImageButton = new StateImageButton(config.getObject("gamble").toConfig(), buttonLayoutSkin.skin, this)
    addActor(gamble)
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
  }

  def getAnimationColletion: SpineAnimationCollection = slotGame.spineCollection

  def changeSlotGameState(state: SLOTGAMESTATE) = {
    println("CHANGE STATE =" + state)
    this.state match {
      case GAMESTATE.POKERGAME => state match {
        case SLOTGAMESTATE.AMOUNT_CREDITED =>
          dealButton.mainFunctionalState = false
          cashButton.mainFunctionalState = false
          menuButton.mainFunctionalState = false
          helpButton.mainFunctionalState = false
          slotGameState = SLOTGAMESTATE.FREE
        case SLOTGAMESTATE.EMPTY_CREDITS =>
          dealButton.mainFunctionalState = true
          cashButton.mainFunctionalState = true
          CardLayoutState.state = INSERTCARD
          pokerDeck.pokerGame.cardLayout.showPanel(pokerDeck.pokerGame.cardLayout.msgPanel.value._1)
          pokerDeck.pokerGame.addActor(pokerDeck.pokerGame.cardLayout)
        case SLOTGAMESTATE.FREE =>
          dealButton.mainFunctionalState = false
          cashButton.mainFunctionalState = false
          menuButton.mainFunctionalState = false
          helpButton.mainFunctionalState = false
          slotGameState = SLOTGAMESTATE.FREE
        case _ =>
      }
      case GAMESTATE.SLOTGAME =>
        slotGameState = state
        state match {
          case SLOTGAMESTATE.SPIN =>
            dealPressed
            payLine.dealFlag = true
            gamble.stateFlag = true
            bonus.stateFlag = true
            slotGame.stopBackground
            timer.stop()
          case SLOTGAMESTATE.SPIN_STOP =>
            event.getTarget().fire(new ChangeButtonState(dealButton.states.get(0)))
            slotSpinState = state
          case SLOTGAMESTATE.FREE =>
            drawPressed()
            payLine.dealFlag = false
            gamble.stateFlag = false
            bonus.stateFlag = false
            dealButton.mainFunctionalState = false
            cashButton.mainFunctionalState = false
            slotGameState = SLOTGAMESTATE.FREE
            slotGame.onDenomChangeUpdateJackpots()
            timer.start()
          case SLOTGAMESTATE.AMOUNT_CREDITED =>
            drawPressed()
            payLine.dealFlag = false
            gamble.stateFlag = false
            bonus.stateFlag = false
            dealButton.mainFunctionalState = false
            cashButton.mainFunctionalState = false
            menuButton.mainFunctionalState = false
            helpButton.mainFunctionalState = false
            slotGameState = SLOTGAMESTATE.FREE
          case SLOTGAMESTATE.INLINE_ANIMATION =>
          case SLOTGAMESTATE.BONUS =>
          case SLOTGAMESTATE.DOUBLE_UP_GAME =>
          case SLOTGAMESTATE.EMPTY_CREDITS =>
            dealButton.mainFunctionalState = true
            cashButton.mainFunctionalState = true
            CardLayoutState.state = INSERTCARD
            slotGame.cardLayout.showPanel(slotGame.cardLayout.msgPanel.value._1)
            slotGame.addActor(slotGame.cardLayout)
          case _ =>
        }
      case _ =>
    }

  }

  def disableMenus() = {
    menuButton.mainFunctionalState = true
    helpButton.mainFunctionalState = true
  }

  def getBonusState: Boolean = {
    BONUS_STATE match {
      case TOGGLESTATE.ON => true
      case TOGGLESTATE.OFF => false
    }
  }

  def getGambleState: Boolean = {
    GAMBLE_STATE match {
      case TOGGLESTATE.ON => true
      case TOGGLESTATE.OFF => false
    }
  }

  def startBonusAnimation(name: String) = slotGame.addBonusRound(name)

  def creditState(): Boolean = {
    Model.paymentCard != null && Model.paymentCard.balance > 0 match {
      case true =>
        Model.paymentCard.balance >= totalMeter.credits match {
          case true => return true
          case false =>
            return false
        }
      case false => return false
    }
    false
  }

}

class ButtonLayoutSkin(config: Config) {
  var skin = new Skin(Gdx.files.internal(config.getString("skin")),
    new TextureAtlas(Gdx.files.internal(config.getString("atlas"))))
}