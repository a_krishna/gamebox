package spine

import com.badlogic.gdx.{ApplicationAdapter, Gdx}
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.{Batch, PolygonSpriteBatch, TextureAtlas}
import com.badlogic.gdx.scenes.scene2d.{Group, Stage}
import com.badlogic.gdx.utils.Array
import com.esotericsoftware.spine.{AnimationState, AnimationStateData, Event, Skeleton, SkeletonData, SkeletonJson, SkeletonRenderer}
import com.typesafe.config.Config

case class AnimationTracks(tracks: Int, name: String, loop: Boolean)

 trait SpineAnimationState

object SpineAnimationState {

  case object PLAY extends SpineAnimationState

  case object PAUSE extends SpineAnimationState

  case object STOP extends SpineAnimationState

}

class SpineAnimation(config: Config) extends Group {
  val scale = 0.5F
  var skeletonRenderer: SkeletonRenderer = null
  var animationState: AnimationState = null
  var skeleton: Skeleton = null
  var STATE: SpineAnimationState = SpineAnimationState.PAUSE
  var mesh:Boolean=config.getBoolean("mesh")
  var resize,setPoseFlag:Boolean=false
  var rootX,rootY:Float=0F
  val scalex:Float=config.getLong("scaleX")
  val scaley:Float=config.getLong("scaleY")
  val meshTag:Boolean = config.getBoolean("mesh")

  def createPack():Unit = {
    skeletonRenderer = new SkeletonRenderer
    skeleton = new Skeleton(loadPack)
    meshTag match {
      case true => skeleton.setX(config.getInt("padX")); skeleton.setY(config.getInt("padY"))
      case false =>
    }
    config.getBoolean("skin.flag") match {
      case true =>
        val skin = skeleton.getData.findSkin(config.getString("skin.name"))
        skeleton.setSkin(skin)
      case false =>
    }
    animationState = loadanimationState(skeleton.getData)

    loadAnimation(new AnimationTracks(config.getInt("animation.tracks"), config.getString("animation.name"), config.getBoolean("animation.loop")))
    animationState.addListener(new AnimationState.AnimationStateListener {
      override def event(trackIndex: Int, event: Event) = {
      }
      override def complete(trackIndex: Int, loopCount: Int) = {
        if(!setPoseFlag){
          skeleton.setToSetupPose()
        }
      }

      override def start(trackIndex: Int) = {
      }

      override def end(trackIndex: Int) = {
      }
    })

  }

  def changePosition(xCord: Float, yCord: Float) = {
    resize=true
    rootX=xCord
    rootY=yCord
    createPack()
  }
  def changeAnimation(tracks:Int,animationName:String,loop:Boolean)={
    skeleton.setToSetupPose()
    animationState.setAnimation(tracks,animationName,loop)
  }

  def changeAnimationWithoutRefresh(tracks:Int,animationName:String,loop:Boolean)={
    animationState.setAnimation(tracks,animationName,loop)
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
    meshTag match {
      case true =>
        skeletonRenderer.draw(batch.asInstanceOf[PolygonSpriteBatch], skeleton)
      case false =>
        skeletonRenderer.draw(batch, skeleton)
    }
  }
  override  def act(delta:Float)={
    STATE match {
      case SpineAnimationState.PAUSE => skeleton.updateWorldTransform()
      case SpineAnimationState.PLAY =>
        animationState.update(delta)
        skeleton.updateWorldTransform()
      case SpineAnimationState.STOP =>
    }
    animationState.apply(skeleton)
  }

  def getConfig: Config = config

  def loadPack: SkeletonData = {
    resize match {
      case true=>
        val data:SkeletonData=loadSkeletonJson.readSkeletonData(Gdx.files.internal(config.getString("path") + ".json"))
        val rootBone=data.findBone("root")
        rootBone.setX(rootX)
        rootBone.setY(rootY)
        data
      case false=>
        loadSkeletonJson.readSkeletonData(Gdx.files.internal(config.getString("path") + ".json"))
    }

  }

  def loadSkeletonJson: SkeletonJson = {
    val skeletonJson = new SkeletonJson(new TextureAtlas(Gdx.files.internal(config.getString("path") + ".atlas")))
    skeletonJson.setScale(config.getInt("scale") * scale / 1080)
    skeletonJson
  }

  def loadanimationState(skeletonData: SkeletonData): AnimationState = new AnimationState(new AnimationStateData(skeletonData))

  def loadAnimation(animConfig: AnimationTracks) = animationState.setAnimation(animConfig.tracks, animConfig.name, animConfig.loop)

}


class RunAnimation(config: Config) extends ApplicationAdapter {
  val spineAnimation = new Array[SpineAnimation]()
  var stage: Stage = null

  override def create() = {
    stage = new Stage()
    for (i <- 0 to config.getConfigList("spine").size() - 1) {
      val spine = new SpineAnimation(config.getConfigList("spine").get(i))
      spine.createPack()
      stage.addActor(spine)
      spineAnimation.add(spine)
    }
    Gdx.input.setInputProcessor(stage)
  }

  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act()
    stage.draw()
  }
}
