package spine.secondary.game

import java.security.SecureRandom
import java.util.Map.Entry
import com.tykhe.client.components.sound.Sound
import spine.{SpineState, SpineScene, SpineAnimation}

import scala.collection.JavaConverters._
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Group}
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.esotericsoftware.spine._
import com.typesafe.config.{ConfigValue, ConfigObject, Config}

case object BoneSettings {
  def prepareColor(colorName: String): Color = colorName match {
    case "WHITE" => Color.WHITE
    case "BLUE" => Color.BLUE
    case "BLACK" => Color.BLACK
    case "RED" => Color.RED
    case "GREEN" => Color.GREEN
    case "ORANGE" => Color.ORANGE
    case "MAGENTA" => Color.MAGENTA
    case "CYAN" => Color.CYAN
    case "YELLOW" => Color.YELLOW
    case "GRAY" => Color.GRAY
    case "LIGHTGRAY" => Color.LIGHT_GRAY
    case "DARKGRAY" => Color.DARK_GRAY
  }

  def getBoundsSpineAttachment(bone: Bone, boneConfig: BoneConfig): Bound = {
    if (boneConfig.flag) {
      new Bound(bone.getX + bone.getParent.getX, bone.getY + bone.getParent.getY, boneConfig.width, boneConfig.height)
    } else {
      //   println(bone+" "+boneConfig)
      new Bound(bone.getX, bone.getY, boneConfig.width, boneConfig.height)
    }
  }

  def getGameMeter(name: String, meters: Seq[GameMeters]): SecGameMeters = {
    meters.foreach { meter => if (name.equals(meter.name)) {
      return meter.secGameMeters
    }
    }
    null
  }

  def getGameAction(name: String, actions: Seq[GameActions]): BoundBox = {
    actions.foreach { action => if (action.name.equals(name)) {
      return action.actionOne
    }
    }
    null
  }

  def getPayBonusSeq(config: Config, name: String): Seq[PayBonus] = {
    val data: Array[PayBonus] = new Array[PayBonus](config.getObjectList(name).size())
    for (i <- 0 to data.length - 1) {
      val conf = config.getConfigList(name).get(i)
      val payConf = conf.getConfig(PayBonus.getClass.getCanonicalName.split("\\$").last)
      data(i) = new PayBonus(payConf.getString("animationName"), payConf.getLong("amount"))
    }
    data.toSeq
  }

  def getGameMetersSeq(config: Config, name: String): Seq[GameMeters] = {
    val data: Array[GameMeters] = new Array[GameMeters](config.getObjectList(name).size())
    for (i <- 0 to data.length - 1) {
      val conf = config.getConfigList(name).get(i)
      val typeConf = conf.getConfig(GameMeters.getClass.getCanonicalName.split("\\$").last)
      val boneConf = typeConf.getConfig(BoneConfig.getClass.getCanonicalName.split("\\$").last)
      val labelConf = typeConf.getConfig(LabelConfig.getClass.getCanonicalName.split("\\$").last)
      data(i) = new GameMeters(typeConf.getString("name"), new BoneConfig(boneConf.getString("slot"), boneConf.getBoolean("flag"), boneConf.getLong("x"), boneConf.getLong("y"), boneConf.getLong("width"), boneConf.getLong("height")), new LabelConfig(labelConf.getString("skin"), labelConf.getDouble("font").toFloat, labelConf.getLong("credit"), labelConf.getBoolean("positionFlag"), prepareColor(labelConf.getString("color"))))
    }
    data.toSeq
  }

  def getGameActionsSeq(config: Config, name: String): Seq[GameActions] = {
    val data: Array[GameActions] = new Array[GameActions](config.getObjectList(name).size())
    for (i <- 0 to data.length - 1) {
      val conf = config.getConfigList(name).get(i)
      val typeConf = conf.getConfig(GameActions.getClass.getCanonicalName.split("\\$").last)
      val boneConf = typeConf.getConfig(BoneConfig.getClass.getCanonicalName.split("\\$").last)
      data(i) = new GameActions(typeConf.getString("name"), new BoneConfig(boneConf.getString("slot"), boneConf.getBoolean("flag"), boneConf.getLong("x"), boneConf.getLong("y"), boneConf.getLong("width"), boneConf.getLong("height")))
    }
    data.toSeq
  }

}

case class Settings(config: Config) {
  lazy val sounds: Map[String, Sound] = {
    val list: Iterable[ConfigObject] = config.getObjectList("sounds").asScala
    (for {
      item: ConfigObject <- list
      entry: Entry[String, ConfigValue] <- item.entrySet().asScala
      key = entry.getKey
      uri = new Sound(entry.getValue.unwrapped().toString)
    } yield (key, uri)).toMap
  }
}

case class BoneConfig(slot: String, flag: Boolean, x: Float, y: Float, width: Float, height: Float)

case class LabelConfig(skin: String, font: Float, credit: Float, positionFlag: Boolean, color: Color)

case class GameMeters(name: String, boneConfig: BoneConfig, labelConfig: LabelConfig) {
  val secGameMeters: SecGameMeters = new SecGameMeters(new Skin(Gdx.files.internal(labelConfig.skin + ".json"), new TextureAtlas(Gdx.files.internal(labelConfig.skin + ".atlas"))))
  secGameMeters.credit = labelConfig.credit
  secGameMeters.setLabelProperties("font/digital/digital-7 (mono).ttf", labelConfig.font, labelConfig.color)

  def setPosition(spineAnimation: SpineAnimation): Unit = {
    labelConfig.positionFlag match {
      case true =>
        secGameMeters.setMeterPosition(new Bound(boneConfig.x, boneConfig.y, boneConfig.width, boneConfig.height))
      case false => secGameMeters.setMeterPosition(BoneSettings.getBoundsSpineAttachment(spineAnimation.skeleton.findBone(boneConfig.slot), boneConfig))
    }
  }
}

case class GameActions(name: String, boneConfig: BoneConfig) {
  var actionOne: BoundBox = null

  def setActions(spineAnimation: SpineAnimation): Unit = {
    actionOne = new BoundBox(BoneSettings.getBoundsSpineAttachment(spineAnimation.skeleton.findBone(boneConfig.slot), boneConfig))
    actionOne.setName(name)
  }
}

case class PayBonus(animationName: String, amount: Float)

class SecondaryGame(config: Config, spineScene: SpineScene) extends Group {
  var ranDom = 0
  var triggerCount: Int = config.getInt("triggerCount.count")
  var count = 0
  var clickCounter = 1 - 2
  val random = new SecureRandom()
  val pickNGameRandom = new PickNGameRandom(10, 5)
  val sounds: Map[String, Sound] = new Settings(config).sounds
  val animations: Seq[PayBonus] = BoneSettings.getPayBonusSeq(config, "animations")
  var gameMeters: Seq[GameMeters] = BoneSettings.getGameMetersSeq(config, "gameMeters")
  val actions: Seq[GameActions] = BoneSettings.getGameActionsSeq(config, "gameActions")
  spineScene.spineAnimation.setPoseFlag = true
  addActor(spineScene)
  gameMeters.foreach { meters => meters.setPosition(spineScene.spineAnimation)
    addActor(meters.secGameMeters)
  }
  actions.foreach { action => action.setActions(spineScene.spineAnimation)
    addActor(action.actionOne)
    action.actionOne.addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float): Unit = {
        if (getCount == 0) {
          if (event.getListenerActor.getName.equals("Sling")) {
            val ran = random.nextInt((3 - 1) + 1) + 1
            ranDom = ran
            if (ran < animations.length) {
              spineScene.spineAnimation.changeAnimation(0, animations(ran).animationName, false: Boolean)
            } else {
              spineScene.spineAnimation.changeAnimation(0, "miss", false: Boolean)
            }
            sounds.apply(event.getListenerActor.getName).setSoundSetting(1.0f, 1.0f, 1.0f)
            sounds.apply(event.getListenerActor.getName).playSound()
          } else {
            val actorName = event.getListenerActor.getName
            if (actorName.split("_").head.equals("box")) {
              BoneSettings.getGameAction(actorName, actions).disableTouch()
              if (pickNGameRandom.count == (-1)) {
                pickNGameRandom.regenerate()
                gameMeters(actorName.split("_").last.toInt).secGameMeters.addAmount(pickNGameRandom.getNumberPerClick(actorName.split("_").last.toInt) - 1)
                sounds.apply(actorName.split("_").head).setSoundSetting(1.0f, 1.0f, 1.0f)
                sounds.apply(actorName.split("_").head).playSound()
                spineScene.spineAnimation.changeAnimation(0, actorName, false: Boolean)
              } else {
                val num = pickNGameRandom.getNumberPerClick(actorName.split("_").last.toInt - 1)
                if (pickNGameRandom.count == (pickNGameRandom.getMaxPick - 1)) {
                  actions.foreach(act => act.actionOne.disableTouch())
                  sounds.apply("win").setSoundSetting(1.0f, 1.0f, 1.0f)
                  sounds.apply("win").playSound()
                }
                gameMeters(actorName.split("_").last.toInt).secGameMeters.addAmount(num)
                sounds.apply(actorName.split("_").head).setSoundSetting(1.0f, 1.0f, 1.0f)
                sounds.apply(actorName.split("_").head).playSound()
                spineScene.spineAnimation.changeAnimationWithoutRefresh(0, actorName, false: Boolean)
              }
              clickCounter = 1 - 2
            }
          }
        }
      }
    })
  }

  def addSecGame(): Unit = {
    //  addActor(spineScene)
    //  gameMeters.foreach{game=>addActor(game.secGameMeters)}
    //  actions.foreach{action=>addActor(action.actionOne)}
    startPlaying()
  }

  def startPlaying(): Unit = {
    spineScene.changeState(SpineState.PLAY)
  }

  def stopPlaying(): Unit = {
    spineScene.changeState(SpineState.STOP)
    reCreate()
  }

  def getCount: Int = {
    clickCounter += 1
    clickCounter
  }


  def reCreate(): Unit = {
    gameMeters.foreach { meters => meters.setPosition(spineScene.spineAnimation)
      removeActor(meters.secGameMeters)
    }
    gameMeters = BoneSettings.getGameMetersSeq(config, "gameMeters")
    spineScene.spineAnimation.changeAnimation(0, animations(0).animationName, false: Boolean)
    gameMeters.foreach { meters => meters.setPosition(spineScene.spineAnimation)
      addActor(meters.secGameMeters)
    }
    count = 0
    clickCounter = 1 - 2
    pickNGameRandom.count = 1 - 2
  }

  var mangoBonusCredit = 0.0f
  spineScene.spineAnimation.animationState.addListener(new AnimationState.AnimationStateListener {
    override def event(trackIndex: Int, event: Event) = {
      event.getString match {
        case "WinCreditUpdated" => println(event.getString)
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          println(meter + " " + event.getData.getName)
          meter.addAmount(0)
          meter.flag = true
          mangoBonusCredit = meter.credit
          actions.foreach(action => action.actionOne.disableTouch())
        case "WinPossibilityOneUpdated" => println(event.getString)
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          meter.addAmount(50)
          meter.flag = true
          mangoBonusCredit = meter.credit
          actions.foreach(action => action.actionOne.disableTouch())
        case "WinPossibilityTwoUpdated" => println(event.getString)
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          meter.addAmount(100)
          meter.flag = true
          mangoBonusCredit = meter.credit
          actions.foreach(action => action.actionOne.disableTouch())
        case "StoneHit" => println(event.getString)
          sounds.apply(event.getString).playSound()
        case "MangoFall" =>
          println(event.getString)
          sounds.apply(event.getString).setSoundSetting(1.0f, 1.0f, 1.0f)
          sounds.apply(event.getString).playSound()
        case "MangoTwoFall" =>
          println(event.getString)
          sounds.apply(event.getString).setSoundSetting(1.0f, 1.0f, 1.0f)
          sounds.apply(event.getString).playSound()
        case "Miss" =>
        case "HIT" =>
        case "Good" =>
          sounds.apply(event.getString).setSoundSetting(1.0f, 1.0f, 1.0f)
          sounds.apply(event.getString).playSound()
        case "HardLuck" =>
          sounds.apply(event.getString).playSound()
        case "Excellent" =>
          sounds.apply(event.getString).setSoundSetting(1.0f, 1.0f, 1.0f)
          sounds.apply(event.getString).playSound()
        case "ShutterOpenStart" =>
          sounds.apply(event.getData.getName.split("_").head).setSoundSetting(1.0f, 1.0f, 1.0f)
          sounds.apply(event.getData.getName.split("_").head).playSound()
        case "ShutterOpened" =>
          sounds.apply(event.getData.getName.split("_").head).stopSound()
        case "RemainingMeter" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          println(event.getData.getName + " " + meter.credit)
          if (!spineScene.spineAnimation.animationState.getCurrent(0).getAnimation.getName.equals(animations(0).animationName)) {
            meter.addAmount(meter.credit - 1)
          } else {
            meter.addAmount(meter.credit)
          }

          meter.flag = true
        case "PickNGameCreditUpdated" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          meter.showTempAmount()
          println(meter + " " + event.getData.getName + "  " + meter.credit)
          meter.flag = true
        case "BoxOneOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxTwoOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxThreeOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxFourOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxFiveOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxSixOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxSevenOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxEightOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxNineOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "BoxTenOpened" =>
          val meter = BoneSettings.getGameMeter(event.getData.getName, gameMeters)
          val winMeter = BoneSettings.getGameMeter("WinCredit", gameMeters)
          meter.flag = true
          winMeter.addTempAmount(meter.credit)
        case "IncreaseCount" =>
          increaseCompeletCount()
        case _ => println("Nothing " + event.getString + " " + event.getData.toString)
      }
    }

    def increaseCompeletCount(): Unit = {
      count += 1
      println("Bonus Complet Trigger Count =" + count + "   " + triggerCount)
    }

    override def complete(trackIndex: Int, loopCount: Int) = {
      println("Completed Second Listener")
      if (spineScene.spineAnimation.animationState.getTracks.get(0).getAnimation.getName.equals(animations(0).animationName)) {
        actions.foreach(action => action.actionOne.enableTouch())
      }
      sounds.foreach(snd => snd._2.stopSound())
      if (count >= triggerCount) {
        if (triggerCount != 2)
          fire(new BonusCompletedEvent(BoneSettings.getGameMeter("WinCredit", gameMeters).credit))
        else
          fire(new BonusCompletedEvent(mangoBonusCredit))
      }
    }

    override def start(trackIndex: Int) = {
    }

    override def end(trackIndex: Int) = {
    }
  })
}
