package spine.secondary.game

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Group, Touchable}

case class Bound(x:Float,y:Float,width:Float,height:Float)
class BoundBox(bound:Bound) extends  Group{
  setBounds(bound.x,bound.y,bound.width,bound.height)
  disableTouch()
  def enableTouch() = {
    setTouchable(Touchable.enabled)
    println("Touch Enabled")
  }
  def disableTouch() = {
    setTouchable(Touchable.disabled)
    println("Touch Disabled")
  }
  override  def draw(batch:Batch,parentAlpha:Float)=super.draw(batch,parentAlpha)
}
