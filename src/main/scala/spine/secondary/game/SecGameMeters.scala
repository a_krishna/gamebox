package spine.secondary.game

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.{Label, Skin}
import com.tykhe.client.components.meter.GenerateFont


class SecGameMeters(skin:Skin)  extends  Actor{
val label:Label=new Label("",skin)
  var flag:Boolean=false
  var credit:Float=0.0f
  var tmpCredit:Float=0.0f
  def setMeterPosition(bound:Bound): Unit ={
    label.setPosition(bound.x,bound.y)
  }
  def setLabelProperties(fontName:String,font:Float,color:Color): Unit ={
    label.setStyle(GenerateFont.fontStyle(fontName,font.toInt,color))
  }
  def addAmount(amt:Float): Unit = {
      credit = amt
      label.setText(credit.toString)
  }
  def addTempAmount(tmp:Float): Unit ={
    tmpCredit=tmp
  }
  def showTempAmount(): Unit ={
    credit+=tmpCredit
    label.setText(credit.toString)
  }
  def getSkin:Skin=skin

  override def draw(batch:Batch,parentAlpha:Float):Unit={
    super.draw(batch,parentAlpha)
    if(flag){
      label.draw(batch,parentAlpha)
    }
  }

}
