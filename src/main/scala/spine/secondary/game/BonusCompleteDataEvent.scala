package spine.secondary.game

import com.badlogic.gdx.scenes.scene2d
import com.esotericsoftware.spine.Event


class BonusCompleteDataEvent(name:String,amount:Float) extends scene2d.Event {
 val bonusName=name
  val amountWin=amount
}
