package spine.secondary.game

import java.security.SecureRandom

import com.badlogic.gdx.Gdx


class PickNGameRandom(size:Int,maxPick:Int) {
val numbers:Array[Int]=new Array[Int](size)
  val random=new SecureRandom()
  var count:Int=1-2

  def getNumberPerClick(num:Int): Int = {
    count+=1
    if(count<maxPick)
     numbers(getPick(num))
    else
      0
  }
  def getPick(num:Int):Int={
    num >= size match {
      case true=> num -1
      case false=> num
    }
  }

  def regenerate(): Unit ={
    for(i<- 0 to size-1){
      val randomNum=random.nextInt((100 - 1) + 1) + 1
      numbers(i)=randomNum
    }
    count=1-2
  }
  def getMaxPick:Int=maxPick
}
