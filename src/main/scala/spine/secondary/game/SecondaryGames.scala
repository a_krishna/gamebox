package spine.secondary.game

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Event, EventListener, Group}
import com.typesafe.config.Config
import spine.SpineScene

import scala.collection.mutable



class SecondaryGames() extends  Group{
var secGames=new mutable.HashMap[String,SecondaryGame]
var refName:String=null
  def addSecondaryGame(name:String,config:Config,spineScene: SpineScene): Unit ={
    secGames.put(name,new SecondaryGame(config,spineScene))
  }
  def addEventListener() = {
    secGames.foreach{game=>game._2.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        event.isInstanceOf[BonusCompletedEvent] match {
          case true=>println(event.getClass.getCanonicalName+" Triggered")
            fire(new BonusCompleteDataEvent(game._1,event.asInstanceOf[BonusCompletedEvent].amountWon))
            true
          case false=>
            false
        }
      }
    })}
  }

  def addBonusActor(name:String): Unit ={
    println(this.getClass.getCanonicalName+"  "+"Adding Actor"+"   "+name+"   "+secGames.size)
    refName=name
    secGames.iterator.foreach(sec=>println(sec._1))
    addActor(secGames.apply(name))
    secGames.apply(name).addSecGame()
  }

  def removeBonusActor():Unit={
    secGames.apply(refName).stopPlaying()
    removeActor(secGames.apply(refName))
//    secGames.apply(refName).reCreate()
  }
  override  def draw(batch:Batch,parentAlpha:Float):Unit= super.draw(batch,parentAlpha)

}
