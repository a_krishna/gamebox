package spine

import com.typesafe.config.Config

import scala.collection.mutable

class SpineAnimationCollection(config : Config) {
  val spineCollection =new mutable.HashMap[String,SpineScene]()
  animationLoader()
  def loadScene(name : String) : SpineScene = spineCollection.get(name).get
  def animationLoader() = {
    println(this.getClass.getCanonicalName)
    for(i<- 0 to config.getConfigList(this.getClass.getCanonicalName).size()-1){
      val scene=config.getConfigList(this.getClass.getCanonicalName).get(i)
      spineCollection.put(scene.getString("name"),new SpineScene(scene.getConfig(config.getClass.getCanonicalName)))
    }
  }
}
