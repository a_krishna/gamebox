package spine

import com.badlogic.gdx.graphics.g2d.Batch
import com.tykhe.client.components.sound.Sound
import com.typesafe.config.Config


sealed trait SpineState
object SpineState{
  case object PLAY extends SpineState
  case object PAUSE extends  SpineState
  case object STOP extends SpineState
  case object PLAYMUTE extends SpineState
}
class SpineScene(config:Config) extends  com.badlogic.gdx.scenes.scene2d.Group{
 val spineSound=new Sound(config.getString("sound.path"))
 val spineAnimation=new SpineAnimation(config)
  var STATE:SpineState=SpineState.PAUSE
 spineAnimation.createPack()
  addActor(spineAnimation)
  addActor(spineSound)

  def PLAY() = changeState(SpineState.PLAY)
  def PLAYMUTE() = changeState(SpineState.PLAYMUTE)
  def PAUSE() = changeState(SpineState.PAUSE)
  def STOP() = changeState(SpineState.STOP)
  def changeState(state:SpineState)={
    STATE=state
    STATE match {
      case SpineState.PAUSE=>
        spineAnimation.STATE=SpineAnimationState.PAUSE
        spineSound.stopSound()
      case SpineState.PLAY=>
        spineAnimation.STATE=SpineAnimationState.PLAY
        config.getBoolean("sound.flag") match {
          case true=>
            spineSound.flag=true
            spineSound.playSound()
          case false=>
            spineSound.playMusic()
        }
      case SpineState.PLAYMUTE=>
        spineAnimation.STATE=SpineAnimationState.PLAY
        spineSound.stopSound()
      case SpineState.STOP=>
        spineAnimation.STATE=SpineAnimationState.STOP
        spineSound.stopSound()
    }
  }
  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
  }
}