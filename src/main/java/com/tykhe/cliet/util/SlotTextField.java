package com.tykhe.cliet.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.typesafe.config.Config;

public class SlotTextField extends Actor {

	private TextField field;
	private Skin skin;

	public SlotTextField(Config config) {
		System.out.println("Created SlotTextField");
		this.skin = new Skin(Gdx.files.internal(config.getString("path")));
		// skin=new Skin();
		field = new TextField(config.getString("name"), this.skin);
		field.setBounds(config.getLong("x"), config.getLong("y"),
				config.getLong("width"), config.getLong("height"));
	}

	public TextField getField() {
		return field;
	}

	public void setField(TextField field) {
		this.field = field;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);

		field.draw(batch, parentAlpha);
	}

}
