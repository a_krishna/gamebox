package com.tykhe.client.threedim.demo;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import java.awt.*;

public class MainExecution {

	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		cfg.title = "ThreeDimensionTest";
		cfg.vSyncEnabled = true;
		// cfg.width = screenDimension.width;
		// cfg.height = screenDimension.height;
		// cfg.useGL20 = true;
		cfg.fullscreen = false;
		cfg.width = 1024;
		cfg.height = 1500;
		new LwjglApplication(new ThreeDimDemo(), cfg);
	}
}
