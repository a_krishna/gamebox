package com.tykhe.client.logger;

public class CustomTrace {
public static boolean FLAG=false;

public static void trace(String msg){
	if(FLAG){
		System.out.println(msg);
	}
}
public static void traceNoNewLine(String msg){
	if(FLAG){
		System.out.print(msg);
	}
}

public static void traceInt(int msg){
	if(FLAG){
		System.out.println(msg);
	}
}
public static void traceFloat(Float msg){
	if(FLAG){
		System.out.println(msg);
	}
}
public static void traceNewLine(){
	if(FLAG){
		System.out.println();
	}
}

public static void traceArrayString(String[] msg){
	if(FLAG){
		System.out.print("[ ");
		for (String string : msg) {
			System.out.print(string+" , ");
		}
		System.out.println("]");
	}
}
}
