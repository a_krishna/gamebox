package com.tykhe.client.slot.components;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.tykhe.client.components.ReelTestUpdate;
import com.tykhe.client.components.SLOTGAMESTATE;
import com.tykhe.client.components.buttons.GameSetup;
import com.tykhe.client.core.ProcessRandom;
import com.tykhe.client.poker.components.button.ButtonLayout;
import com.tykhe.client.slot.BoxHandler;
import com.tykhe.client.slot.PayLineWin;
import com.tykhe.client.slot.SlotMeter;
import com.tykhe.cliet.util.SlotTextField;
import com.typesafe.config.Config;
import java.util.Arrays;
import java.util.Map;


public class Barrel extends Group {
	private float xStrips;
	private float xStripEsp;
	private Array<ReelStrip> strips;
	private PayLines payLines;
	private SlotMeter slotMeter = new SlotMeter();
	boolean spinFlag, flag, initlaize,winHighlights = false;
	int count, reel, drumStop = 0;
	float delay = 1f;
	float interval = 1f;
	int repeatCount = 3;
	int reelDecelerateCount = 1;
	int payLineSize=0;
	Timer reeltimer, drumTimer;
	ReelSound reelSound;
	SlotTextField field;
	ReelTestUpdate reelTestUpdate;
	String filePath;
	ButtonLayout buttonLayout = null;
	int refBonusCount=0;
	int[] stripsLength;
	BoxHandler boxHandler;
	public int med_speed=3;
	private boolean slotTectFieldFlag=false;

	public Barrel(Config config) {
		xStripEsp = config.getLong("xStripEsp");
		xStrips = config.getLong("xStrips");
		reeltimer = new Timer();
		drumTimer = new Timer();
		strips = new Array<ReelStrip>();
		reelSound = new ReelSound(config.getConfig("reelSound"));
		init(config);
		setY(config.getLong("y"));
		payLines = new PayLines(config);
		payLines.setWinLinePos(strips);
		slotTectFieldFlag=config.getBoolean(SlotTextField.class.getSimpleName().concat(
				".flag"));
		if (slotTectFieldFlag) {
			field = new SlotTextField(config.getConfig(SlotTextField.class
					.getSimpleName()));
		}
	}

	public void initSlotTextField(Group actor) {
		ClickListener listener = new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (event.getListenerActor() == field.getField()) {
					System.out.println("hello -------------");
					field.getField().setText("");
					flag = true;
				}
			}
		};
		if(slotTectFieldFlag) {
			field.getField().addListener(listener);
			actor.addActorAfter(this, field.getField());
		}
	}

	public void addWinLineBar(String place, Group actor) {
		switch (place) {
		case "ADD":
			getPayLines().addWinLineStrip(actor);
			break;
		case "REMOVE":
			// actor.removeActor(lineStrip);
			break;
		default:
			break;
		}
	}

	public void winLineBar(String state, Group actor, Payline payline,
			Group winLineGroup) {
		switch (state) {
		case "ADD":
			payline.drawLineWin(winLineGroup, this, state);
			actor.addActorAfter(this, winLineGroup);
			break;
		case "REMOVE":
			// payline.drawLineWin(winLineGroup, this, state);
			System.out.println("Actor Removing");
			actor.removeActor(winLineGroup);
			break;
		case "REMOVE-RECREAT":
			actor.removeActor(winLineGroup);
			for (PayLineWin lineWin : getSlotMeter().payLine()) {
				System.out.println(getPayLines()+"-----------"+lineWin);
				
				getPayLines().addActor(getPayLines().getLines().get(lineWin.getLineNum()));
			}
			break;
		default:
			break;
		}
	}

	public void setPayLineGroup(String place, Group actor) {
		switch (place) {
		case "ABOVE":
			actor.addActorAfter(this, payLines);
			break;
		case "BELOW":
			actor.removeActor(payLines);
			payLines.setWinLineStripDefault(this);
			break;
		case "DOWN":
			actor.addActorBefore(this, payLines);
			break;
		case "REVOKE":
			actor.addActorAfter(this, payLines);
			break;
		default:
			break;
		}
	}

	public void init(Config config) {
		stripsLength=new int[config.getInt("strips")];
		for (int i = 0; i < config.getInt("strips"); i++) {
			ReelStrip reelStrip = new ReelStrip(config.getConfig("reelStrip"
					+ i));
			stripsLength[i]=config.getInt("reelStrip"+i+".reelsCount");
			reelStrip.setX(xStrips);
			reelStrip.setY(config.getLong("yStrips"));
			reelStrip.stopCount = i;
			reelStrip.setReelSound(reelSound);
			reelStrip.init();
			xStrips += xStripEsp;
			strips.add(reelStrip);
			addActor(reelStrip);
		}
		this.addListener(new ReelStopCompeleted(){
			@Override
			public boolean handle(Event event) {
				spinFlag=getButtonLayout().stopComplete();
				buttonLayout.changeSlotGameState(SLOTGAMESTATE.SPIN_STOP);
				addWinResultComplete();
				return super.handle(event);
			}
		});
	}


	public float getxStrips() {
		return xStrips;
	}

	public void setxStrips(float xStrips) {
		this.xStrips = xStrips;
	}

	public float getxStripEsp() {
		return xStripEsp;
	}

	public void setxStripEsp(float xStripEsp) {
		this.xStripEsp = xStripEsp;
	}

	public Array<ReelStrip> getStrips() {
		return strips;
	}

	public void setStrips(Array<ReelStrip> strips) {
		this.strips = strips;
	}

	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}

	public void spin(ButtonLayout buttonLayout) {
		setButtonLayout(buttonLayout);
		if (!spinFlag) {
			reel = 0;
			spinFlag = true;
			for (int i = 0; i < strips.size; i++) {
				System.out.println("i am in loading strips");
				if (i < 1) {
					strips.get(i).getReelSound().play(0);
				}
				strips.get(i).setMed_speed(med_speed);
				strips.get(i).decelerate(1);
				strips.get(i).flag = false;
				strips.get(i).reelStop = true;
				strips.get(i).velocityLock = false;
			}
			if (flag) {
				flag = false;
				if (field.getField().getText().equals("")) {
					updateStop(generateRandom());
				} else {
					String[] stops = field.getField().getText().split(",");
					System.out.println("Update Symbol"
							+ Arrays.toString(compareSymbol(stops)));
					updateStop(compareSymbol(stops));
				}
			} else {
				updateStop(generateRandom());
			}
		}

	}

	public String[] compareSymbol(String[] symbols) {
		String[] gen = new String[symbols.length];
		for (int i = 0; i < strips.size; i++) {
			for (int j = 0; j < strips.get(i).textures.size; j++) {
				if (symbols[i].equals(strips.get(i).textures.get(j).reelName)) {
					gen[i] = ""
							+ imageReelPos(j, 1, strips.get(i).textures.size);
				}
			}
		}
		return gen;
	}

	public int imageReelPos(int imageStop, int reelPos, int totalReelSize) {
		return imageStop + reelPos >= totalReelSize ? totalReelSize
				- (imageStop + reelPos) : imageStop + reelPos;
	}

	public String[] generateRandom() {
		return ProcessRandom.reelStop(stripsLength);
	}

	public void defaultSet() {
		reeltimer.start();
		reeltimer.scheduleTask(new Task() {
			public void run() {
				if (drumStop < 3) {
					System.out.println("count =" + count);
					strips.get(drumStop).reelStop = false;
					spinFlag = drumStop == 2 ? checkReelStopEvent()
							: true;
					drumStop++;
				}
			}
		}, delay);
	}

	public Boolean checkReelStopEvent(){
		getStrips().get(getStrips().size-1).handleReelStop=true;
		return false;

	}


	public void reelStop() {
		System.out.println("i am in reelstop");
		if (reel > 2) {
			drumTimer.stop();
		} else {
			System.out.println("start" + reel);
			defaultSet();
			reeltimer.stop();
			reeltimer.start();
			reel++;
		}
	}



	public void stopSpin() {
		strips.get(reel).reelStop = false;
		spinFlag = reel == 2 ? buttonLayout.stopComplete() : true;
		reel++;
	}

	public void updateStop(String[] stops) {
		reel = 0;
		drumStop = 0;
		for (int i = 0; i < stops.length; i++) {
			strips.get(i).imageStop = Integer.parseInt(stops[i]);
		}
		System.out.println("++++----------" + Arrays.toString(stops));
		drumTimer.start();
		drumTimer.scheduleTask(new Task() {

			@Override
			public void run() {
				System.out.println("i am in updateStop");
				reelStop();
			}
		}, delay, interval, repeatCount - 1);
	}

	public ButtonLayout getButtonLayout() {
		getPayLines().setWinLine(this, buttonLayout.payTable());
		// buttonLayout.slotSpinResult();
		return buttonLayout;
	}

	public void addWinResultComplete(){
		buttonLayout.changeSlotGameState(SLOTGAMESTATE.INLINE_ANIMATION);
		playInLineAnim(0, payLineSize);
	}
	public void stopInlineAnim(Payline payLine,int count){
		payLine.removeWinLineStrip(this);
		buttonLayout.addCashAmount();
		count++;
		System.out.println(buttonLayout.BONUS_STATE() + "      " + payLine.getWin().getWinType());
		if((payLine.getWin().getWinType().equals("Bonus")) && (buttonLayout.getBonusState())){
			refBonusCount=count;
          buttonLayout.startBonusAnimation(payLine.getWin().getName());
		}else{
			if(count > payLineSize){
				showWinLine();
				buttonLayout.changeSlotGameState(SLOTGAMESTATE.FREE);
			}else{
				playInLineAnim(count++, payLineSize);
			}
		}

	}
	public void bonusCompeleteEvent(int amount){
		buttonLayout.addWinAmount(amount);
		buttonLayout.addCashAmount();
		System.out.println("Reference Count "+refBonusCount+"    PaylineSize ="+payLineSize);
		if(refBonusCount > payLineSize){
			showWinLine();
			buttonLayout.changeSlotGameState(SLOTGAMESTATE.FREE);
		}else{
			playInLineAnim(refBonusCount++, payLineSize);
		}
	}
	public void playInLineAnim(final int count, final int size){
		if(count<=size){
			final Payline payline = payLines.getLines().get(count);
			if ((payline!=null) && (payline.isWinLine()) && (payline.getWin()!=null)) {
					payline.drawWinLineStrip(this);
					try {
						buttonLayout.addWinAmount(slotMeter.getAmountByName(payline.getName()));
					}catch (IndexOutOfBoundsException exe){
						System.err.println("Index out Of bound exeception Count:"+count+" Meter Line :"+slotMeter.payLine().size());
					}
					Timer  timer=new Timer();
					timer.scheduleTask(new Timer.Task() {
						@Override
						public void run() {
							stopInlineAnim(payline,count);
						}
					}, GameSetup.inlineAnimationTime());
					timer.start();
			} else {
				int countInc = count + 1;
				playInLineAnim(countInc,size);
			}
		}else{
			buttonLayout.changeSlotGameState(SLOTGAMESTATE.FREE);
		}
	}

	public void showWinLine(){
		for(Map.Entry<Integer, Payline> payline:getPayLines().getLines().entrySet()){
			if((payline.getValue()!=null) && (payline.getValue().isWinLine()) && (payline.getValue().getWin()!=null)){
					payline.getValue().showWinLine(this);
			}
		}
	}
	public void removeWinLine(){
		for(Map.Entry<Integer, Payline> payline:getPayLines().getLines().entrySet()){
			if((payline.getValue()!=null) && (payline.getValue().isWinLine()) && (payline.getValue().getWin()!=null)){
				payline.getValue().removeWinLine(this);
			}
		}
	}
	public void playBackInLineAnim(final int count, final int size){
		if(count<=size) {
			final Payline payline = payLines.getLines().get(count);
			if ((payline!=null) && (payline.isWinLine()) &&(payline.getWin()!=null)) {
				payline.playMuteWinLineStrip(this);
				Timer  timer=new Timer();
				timer.scheduleTask(new Timer.Task() {
					@Override
					public void run() {
						stopInlineAnim(payline,count);
						int countInc = count + 1;
						playBackInLineAnim(countInc,size);
					}
				}, 3f);
				timer.start();
			} else {
				int countInc = count + 1;
				playBackInLineAnim(countInc,size);
			}
		}else{
			playBackInLineAnim(0
					,size);
		}
	}

	public void setButtonLayout(ButtonLayout buttonLayout) {
		if (this.buttonLayout == null)
			this.buttonLayout = buttonLayout;
	}

	public PayLines getPayLines() {
		return payLines;
	}

	public void setPayLines(PayLines payLines) {
		this.payLines = payLines;
	}

	public SlotMeter getSlotMeter() {
		return slotMeter;
	}

	public void setBoxHandler(BoxHandler boxHandler){
		this.boxHandler=boxHandler;
	}

}

