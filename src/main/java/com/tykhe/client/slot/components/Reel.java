package com.tykhe.client.slot.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.typesafe.config.Config;

public class Reel {
	String reelName;
	Texture reelTexture;
	boolean flag=false;

	public Reel(Config config) {
		reelName = config.getString("name");
		reelTexture = new Texture(Gdx.files.internal(config.getString("reelTexture")));
	}

	public void setFlag(boolean flag){
		this.flag=flag;
	}
}
