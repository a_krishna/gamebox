package com.tykhe.client.slot.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import spine.SpineScene;

public class WinLine extends Actor {

	private Texture winHightlight;
	private float posX;
	private float posY;
	private boolean flag = false;
	private float scale = 0;
	private String winName=null;
	private SpineScene spineScenes=null;
	private boolean winSetFlag=false;
	private  int reelPos=0;
	int winAnimOccurance=0;
	boolean rootFlag=false;
	public WinLine(String path, float scale,boolean rootFlag) {
		winHightlight = new Texture(Gdx.files.internal(path));
		this.scale = scale;
		this.rootFlag=rootFlag;
	}

	public void setPositionOfImage(float posX, float posY) {
		this.posX = posX;
		this.posY = posY + scale;
		System.out.println("WinLine Position x=" + this.posX + "    Y="
				+ this.posY);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		if (rootFlag) {
			if (flag && winSetFlag) {
				batch.draw(winHightlight, posX, posY);
			}
		}

	}


    public void addSpineScene(SpineScene spineScene,int pos){
		this.spineScenes=spineScene;
		this.reelPos=pos;
	}

	public void playInlineAnimation(Group group,Barrel barrel){
		if(winAnimOccurance==0){
			flag=true;
			winSetFlag=true;
			group.addActor(spineScenes);
			group.addActor(this);
			barrel.getStrips().get(reelPos).setFlagInlinePlay(winName);
			spineScenes.PLAY();
			winAnimOccurance++;
		}else{
			playMuteInlineAnimation(group,barrel);
		}
	}

	public void StopInlineAnimation(Group group,Barrel barrel){
		winSetFlag=false;
		spineScenes.STOP();
		group.removeActor(spineScenes);
		group.removeActor(this);
		barrel.getStrips().get(reelPos).setFlagInlineStop();
	}

	public void playMuteInlineAnimation(Group group,Barrel barrel){
		flag=true;
		winSetFlag=true;
		group.addActor(spineScenes);
		group.addActor(this);
		barrel.getStrips().get(reelPos).setFlagInlinePlay(winName);
		spineScenes.PLAYMUTE();
	}
	public void showWinBar(Group group,Barrel barrel){
		flag=true;
		winSetFlag=true;
        group.addActor(this);
	}
	public void removeWinBar(Group group,Barrel barrel){
		winSetFlag=false;
		group.removeActor(this);
	}

	public SpineScene getSpinScenes(){
		return  spineScenes;
	}
    public void setWinName(String name){
		winName=name;
	}
	public String getWinName(){
		return winName;
	}

	public float getPosX(){
		return posX;
	}
	public float getPosY(){
		return posY;
	}
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void setWinSetFlag(boolean flag){
		this.winSetFlag=flag;
	}
	public boolean isWinSetFlag(){
		return winSetFlag;
	}

	@Override
	public String toString() {
		return "" + flag;
	}
}
