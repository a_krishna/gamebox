package com.tykhe.client.slot.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.typesafe.config.Config;

public class ReelSound extends Actor {

	private Sound spinSound;
	private Sound stopSpinSound;
	private float pitch;
	private float volume;
	private float pan;
	long stopID;

	public ReelSound(Config config) {
		spinSound = Gdx.audio.newSound(Gdx.files.internal(config
				.getString("spinSound")));
		stopSpinSound = Gdx.audio.newSound(Gdx.files.internal(config
				.getString("stopSpinSound")));
		volume = config.getLong("volume");
		pitch = config.getLong("pitch");
		pan = config.getLong("pan");
	}

	public void init() {

	}

	public Sound getSpinSound() {
		return spinSound;
	}

	public Sound getStopSpinSound() {
		return stopSpinSound;
	}

	public void play(int type) {
		switch (type) {
		case 0:
			long spinID = spinSound.play();
			spinSound.setLooping(spinID, true);
			spinSound.setVolume(spinID, volume);
			spinSound.setPitch(spinID, pitch);
			spinSound.setPan(spinID, pan, volume);
			spinSound.loop();
			break;
		case 1:
			spinSound.stop();
			if (stopID != 0) {
				stopSpinSound.stop(stopID);
			} else {
				stopSpinSound.stop();
			}
			long stopId = stopSpinSound.play();
			stopID = stopId;
			stopSpinSound.setVolume(stopId, volume);
			stopSpinSound.setPitch(stopId, pitch);
			stopSpinSound.setPan(stopId, pan, volume);
			System.out.println("played stopspin" + stopID);
			break;

		default:
			break;
		}
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	public float getPan() {
		return pan;
	}

	public void setPan(float pan) {
		this.pan = pan;
	}

}
