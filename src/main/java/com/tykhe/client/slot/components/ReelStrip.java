package com.tykhe.client.slot.components;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.tykhe.client.logger.CustomTrace;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigObject;

import java.util.ArrayList;
import java.util.List;

public class ReelStrip extends Group {
    public float x;
    private float y;
    private float min_speed;
    private float med_speed;
    private float max_speed;
    private float actorPadding;
    public Array<Reel> textures;
    private List<Float> yPos;
    private ReelSound reelSound;
    float velocity = 0;
    int pos = 0;
    int count = 0;
    int size = 0;
    int imageStop = 10;
    int stopCount = 0;
    float yFirstPos = 0;
    float imageHeight = 0;
    int displacement = 0;
    boolean flag = true;
    boolean reelStop = true;
    boolean velocityLock = false;
    public boolean inlineFlag = false;
    boolean handleReelStop,triggerReelStop=false;
    List<? extends ConfigObject> filePath;
    boolean drawStop = false;

    public ReelStrip(Config config) {
        textures = new Array<Reel>();
        filePath = config.getObjectList("reels");
        min_speed = config.getLong("min_speed");
        med_speed = config.getLong("med_speed");
        max_speed = config.getLong("max_speed");
        actorPadding = config.getLong("actorPadding");
        x = config.getLong("x");
        y = config.getLong("y");
        yPos = new ArrayList<Float>();
    }

    public void init() {
        if (filePath != null) {
            int height = (int) getY();
            for (int i = 0; i < filePath.size(); i++) {
                Reel reel = new Reel(filePath.get(i).toConfig());
                imageHeight = reel.reelTexture.getHeight();
                if (i == filePath.size() - 1) {
                    pos = height;
                }
                getyPos().add((float) height);
                setBounds(x, height, reel.reelTexture.getWidth(),
                        reel.reelTexture.getHeight());
                textures.add(reel);
                height += (reel.reelTexture.getHeight() + actorPadding);
            }
            size = getyPos().size();
            displacement = (int) (getyPos().get(1) - getyPos().get(0));
            yFirstPos = displacement * 2;
        }

    }

    public void setFlagInlinePlay(String name){
        for(Reel reel:textures){
            if(reel.reelName.equals(name)){
//                System.out.println("Found Stop"+name);
                reel.setFlag(true);
            }
        }
    }
    public void setFlagInlineStop(){
        for(Reel reel:textures){
            if(reel.flag){
                reel.setFlag(false);
            }
        }
    }

    public void draw(Batch batch, float delta) {
        super.draw(batch, delta);
        if (drawStop) {
            drawStop = false;
            boundCorruptPosition(getIndex(imageStop));
        }
        // && yPos.get(i - 1) > 50
        for (int i = 1; i <= filePath.size(); i++) {
            if (getyPos().get(i - 1) < 900 && getyPos().get(i - 1) > 50) {
                if (!textures.get(i - 1).flag){
                    batch.draw(textures.get(i - 1).reelTexture, x,
                            getyPos().get(i - 1));
                }
            }
        }
    }

    public void reelStop() {
        if (yFirstPos == getyPos().get(imageStop)) {
            CustomTrace.trace("i came");
            reelSound.play(1);
            count = 0;
            flag = true;
        }
    }

    public void act(float delta) {
        if (!flag) {
            for (int i = 1; i <= filePath.size(); i++) {
                if (getyPos().get(i - 1) <= -(imageHeight + actorPadding)) {
                    float temp = 0;
                    if (i == 1) {
                        temp = getyPos().get(size - 1);
                        temp = temp - velocity;// med_speed;
                    } else if (i > 1) {
                        temp = getyPos().get(i - 2);
                    }
                    float setY = temp + (imageHeight + actorPadding);
                    getyPos().set(i - 1, (float) setY);
                    if (!reelStop) {
                        if (!velocityLock) {
                            float temp2 = velocity;
                            velocity = velocity
                                    - (getyPos().get(getIndex(imageStop)) > 0 ? (getyPos()
                                    .get(getIndex(imageStop)) > displacement ? (getyPos()
                                    .get(getIndex(imageStop)) < yFirstPos ? (yFirstPos - getyPos()
                                    .get(getIndex(imageStop))) : 0)
                                    : 0)
                                    : 0);
                            velocity = velocity < 0 ? temp2 : velocity;
                            velocityLock = (temp2 == velocity) ? false : true;
                            CustomTrace.trace("ImageStopPos= "
                                    + getyPos().get(getIndex(imageStop))
                                    + " Disp= " + displacement + " yFirstPos= "
                                    + yFirstPos);
                            CustomTrace.traceNoNewLine("Velocity varience"
                                    + velocity + "=");
                            CustomTrace.traceFloat(temp2);
                            if (velocity == 0) {
                                boundPos(getIndex(imageStop));
                                for (float value : getyPos()) {
                                    CustomTrace.traceNoNewLine(value + "  ");
                                }
                            }
                            reelSound.play(velocity == 0 ? 1 : 2);
                        }
                        if (yFirstPos == getyPos().get(getIndex(imageStop))) {
                            for (float value : getyPos()) {
                                CustomTrace.traceNoNewLine(value + "  ");
                            }
                            CustomTrace.traceNewLine();
                            CustomTrace
                                    .trace("i am in------------------------------");
                            boundCorruptPosition(imageStop);
                            reelSound.play(1);
                            flag = true;
                            drawStop = true;
                            // barrel.stopSpin();
                        }
                    }

                } else if (drawStop) {
                    CustomTrace.trace("i am in draw");
                } else {
                    getyPos().set(i - 1, getyPos().get(i - 1) - velocity);
                }
            }

        }else{
            if(handleReelStop){
                handleReelStop=false;
                getParent().fire(new Event());
            }
        }
    }

    public int getIndex(int pos) {
        return pos - 1 < 0 ? yPos.size() + (pos - 1) : pos - 1;
    }

    public void boundCorruptPosition(int stopAt) {
        int value = stopAt - 3;
        CustomTrace.trace("value need to fix" + value);
        float temp = getyPos().get(size - 1);
        for (int i = 0; i < value; i++) {
            temp += displacement;
            getyPos().set(i, temp);
        }
    }

    public void boundPos(int stopAt) {
        int tempSize = size;
        int stopAtPrev = stopAt > 0 ? (stopAt - 1) : (size - 1);
        int stopPrev = stopAtPrev > 0 ? (stopAtPrev - 1)
                : (stopAtPrev > 1 ? (stopAt - 1) : (size - 1));
        getyPos().set(stopAt, yFirstPos);
        getyPos().set(stopAtPrev, yFirstPos - displacement);
        getyPos().set(stopPrev, yFirstPos - yFirstPos);
        float temp = yFirstPos;
        for (int i = stopAt + 1; i < size; i++) {
            if (i != stopAt && i != stopAtPrev && i != stopPrev) {
                temp += displacement;
                getyPos().set(i, temp);
                if (i == size - 1 | i > size - 1) {
                    CustomTrace.trace("****i came at reset****");
                    tempSize = stopAt - 2;
                    for (int j = 0; j <= tempSize; j++) {
                        if (j != stopAt && j != stopAtPrev && j != stopPrev) {
                            CustomTrace.trace("***********i am inside");
                            temp += displacement;
                            getyPos().set(j, temp);
                        }
                    }
                }
            }
        }
    }

    public void decelerate(int speed) {
        med_speed += speed;
        CustomTrace.trace(displacement + "  " + med_speed);
        velocity = (int) (displacement / med_speed);
        CustomTrace.traceFloat(velocity);
    }

    public void velocityForNormalStop() {
        velocity = velocity
                - (getyPos().get(imageStop - 1) > 0 ? (getyPos().get(
                imageStop - 1) > displacement ? (getyPos().get(
                imageStop - 1) < yFirstPos ? (yFirstPos - getyPos()
                .get(imageStop - 1)) : 0) : 0) : 0);
        CustomTrace.trace("Normal velocity" + velocity);
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getMin_speed() {
        return min_speed;
    }

    public void setMin_speed(float min_speed) {
        this.min_speed = min_speed;
    }

    public float getMed_speed() {
        return med_speed;
    }

    public void setMed_speed(float med_speed) {
        this.med_speed = med_speed;
    }

    public float getMax_speed() {
        return max_speed;
    }

    public void setMax_speed(float max_speed) {
        this.max_speed = max_speed;
    }

    public float getActorPadding() {
        return actorPadding;
    }

    public void setActorPadding(float actorPadding) {
        this.actorPadding = actorPadding;
    }

    public ReelSound getReelSound() {
        return reelSound;
    }

    public void setReelSound(ReelSound reelSound) {
        this.reelSound = reelSound;
    }

    public List<Float> getyPos() {
        return yPos;
    }

    // if (getyPos().get(i - 1) == inlineAnimation.ipY) {
    // if (!inlineFlag) {
    // batch.draw(textures.get(i - 1).reelTexture, x,
    // getyPos().get(i - 1));
    // } else {
    // CustomTrace.trace("Rendere animation X=" + x + " Y="
    // + getyPos().get(i - 1));
    // inlineAnimation.draw(batch, delta);
    // }
    // } else {
    // batch.draw(textures.get(i - 1).reelTexture, x, getyPos()
    // .get(i - 1));
    // }
}
