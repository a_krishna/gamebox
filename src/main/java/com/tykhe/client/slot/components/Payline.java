package com.tykhe.client.slot.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.typesafe.config.Config;
import org.tykhe.slot.paytable.Win;
import spine.SpineScene;

import java.util.Arrays;

public class Payline extends Actor {
	private String name;
	private String[] position;
	private boolean selection;
	private boolean winLine;
	private Win win;
	private boolean state=true;
	private Texture line;
	private float posX, posY;
	private WinLineStrip lineStrip;
	private String[] reelPosition;

	public Payline(String name, String[] pos, boolean selection,
			boolean winLine, String path, String[] reelPosition, Config config) {
		this.name = name;
		this.position = pos;
		this.setSelection(selection);
		this.setWinLine(winLine);
		this.reelPosition = reelPosition;
		line = new Texture(Gdx.files.internal(path));
		lineStrip = new WinLineStrip(config.getConfig("winLineStrip"));
	}

	public void setLinePosition(float x, float y) {
		posX = x;
		posY = y;
	}


	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		if (isSelection() && state) {
			batch.draw(line, posX, posY);
		}
	}

	public void winHighlights(String[] reels, Barrel barrel) {
		for (int i = 0; i < reels.length; i++) {
			if (reels[i].equals("*")) {
//				System.out.println("I am in * TO PREDICT");
			} else {
				final int pos=i;
//				System.out.println("Set WINLINE TRUE "+reels[i]);
				final WinLine lineWin=lineStrip.getWinLines().get(i);
				lineWin.setWinName(reels[i]);
				lineWin.winAnimOccurance=0;
				String[] res=position[i].split(":");

				if(barrel.boxHandler !=null && barrel.boxHandler.boxes().apply(Integer.parseInt(res[0])).apply(Integer.parseInt(res[1])) !=null ){
					int next= Integer.parseInt(res[1]) == 0 ? barrel.boxHandler.boxes().length()-1 : Integer.parseInt(res[1]) == barrel.boxHandler.boxes().length()-1 ? 0 : Integer.parseInt(res[1]);
					barrel.boxHandler.boxes().apply(Integer.parseInt(res[0])).apply(next).disable();
				}
				// Reel Symbols Highlights
				lineWin.setFlag(true);
				final SpineScene spineScene=barrel.buttonLayout.getAnimationColletion().loadScene(reels[i] + i);
				spineScene.spineAnimation().changePosition(lineWin.getPosX()-spineScene.spineAnimation().scalex(),lineWin.getPosY()-spineScene.spineAnimation().scaley());
				spineScene.setName(reels[i] + i);
				lineWin.addSpineScene(spineScene, pos);
			}
		}
	}

	public void setDefaultWinHighlights(Barrel group) {
		int i=0;
		for (WinLine iterable_element : lineStrip.getWinLines()) {

			if (iterable_element.isFlag()) {
				System.out.println("Default Flag for WinLine Strip");
				// Reel Symbol Highlights Disable
				iterable_element.setFlag(false);
				SpineScene spineScene=iterable_element.getSpinScenes();
				spineScene.STOP();
				String[] res=position[i].split(":");
				if(group.boxHandler !=null && group.boxHandler.boxes().apply(Integer.parseInt(res[0])).apply(Integer.parseInt(res[1])) !=null ){
					int next= Integer.parseInt(res[1]) == 0 ? group.boxHandler.boxes().length()-1 : Integer.parseInt(res[1]) == group.boxHandler.boxes().length()-1 ? 0 : Integer.parseInt(res[1]);
					group.boxHandler.boxes().apply(Integer.parseInt(res[0])).apply(next).enable();
				}
				group.removeActor(spineScene);
			}
			group.getStrips().get(i).setFlagInlineStop();
			i++;
		}

	}

	public void setPosForLineStrip(Array<ReelStrip> strips) {
		for (String pos : reelPosition) {
			System.out.println(Arrays.toString(pos.split(":")));
			lineStrip
					.getWinLines()
					.get(Integer.parseInt(pos.split(":")[0]))
					.setPositionOfImage(
							strips.get(Integer.parseInt(pos.split(":")[0])).x,
							strips.get(Integer.parseInt(pos.split(":")[0]))
									.getyPos()
									.get(Integer.parseInt(pos.split(":")[1])));
		}
	}

	public void drawLineWin(Group group, Actor actor, String state) {
		switch (state) {
		case "ADD":
			// group.addActorBefore(actor, this);
			group.addActor(this);
			for (WinLine line : getLineStrip().getWinLines()) {
				// group.addActorAfter(actor, line);
				group.addActorAfter(this, line);
			}
			getLineStrip().playSound();
			break;
		case "REMOVE":
			group.removeActor(this);
			for (WinLine line : getLineStrip().getWinLines()) {
				group.removeActor(line);
			}
			break;
		default:
			break;
		}

	}

	public void drawWinLineStrip(Barrel barrel){
		for(WinLine winLine:getLineStrip().getWinLines()){
			if(winLine.isFlag()){
				getLineStrip().playSound();
				winLine.playInlineAnimation(barrel, barrel);
			}
		}
	}
	public void removeWinLineStrip(Barrel barrel){
		for(WinLine winLine:getLineStrip().getWinLines()){
			if(winLine.isFlag()){
				winLine.StopInlineAnimation(barrel, barrel);
			}
		}
	}
   public void showWinLine(Barrel barrel){
	   for(WinLine winLine:getLineStrip().getWinLines()){
		   if(winLine.isFlag()){
			   winLine.showWinBar(barrel,barrel);
		   }
	   }
   }

	public void removeWinLine(Barrel barrel){
		for(WinLine winLine:getLineStrip().getWinLines()){
			if(winLine.isFlag()){
				winLine.removeWinBar(barrel,barrel);
			}
		}
	}

	public void playMuteWinLineStrip(Barrel barrel){
		for(WinLine winLine:getLineStrip().getWinLines()){
			if(winLine.isFlag()){
				winLine.playMuteInlineAnimation(barrel, barrel);
			}
		}
	}


	public int getLinesPos(int line, int size) {
		return line + 1 >= size ? (line + 1) - size : line + 1;
	}

	public String getName() {
		return name;
	}

	public String[] getPosition() {
		return position;
	}

	public boolean isSelection() {
		return selection;
	}

	public void enable(){
		state=true;
	}

	public  void disable(){
		state=false;
	}

	public void setSelection(boolean selection) {
		this.selection = selection;
	}

	public boolean isWinLine() {
		return winLine;
	}

	public void setWinLine(boolean winLine) {
		this.winLine = winLine;
	}

	public Win getWin() {
		return win;
	}

	public void setWin(Win win) {
		this.win = win;
	}

	public WinLineStrip getLineStrip() {
		return lineStrip;
	}

	@Override
	public String toString() {
		return "Payline{" +
				"name='" + name + '\'' +
				", position=" + Arrays.toString(position) +
				", selection=" + selection +
				", winLine=" + winLine +
				", win=" + win +
				", line=" + line +
				", lineStrip=" + lineStrip +
				", reelPosition=" + Arrays.toString(reelPosition) +
				'}';
	}
}
