package com.tykhe.client.slot.components;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.tykhe.client.jackpot.Jackpots;
import com.tykhe.client.poker.components.button.ButtonLayout;
import com.tykhe.client.slot.SlotGame;
import com.typesafe.config.Config;
import org.tykhe.slot.paytable.Paytable;
import org.tykhe.slot.paytable.Win;
import spine.SpineAnimationCollection;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class PayLines extends Group {

	private Map<Integer, Payline> lines;

	public PayLines(Config config) {
		lines = new HashMap<Integer,Payline>();
		int i = 1;
		for (Config configLine : config.getConfigList("payLines")) {
			Payline payline = new Payline(configLine.getString(Payline.class
					.getCanonicalName().concat(".name")), configLine
					.getStringList(
							Payline.class.getCanonicalName().concat(".pos"))
					.toArray(
							new String[configLine.getStringList(
									Payline.class.getCanonicalName().concat(
											".pos")).size()]),
					configLine.getBoolean(Payline.class.getCanonicalName()
							.concat(".selection")),
					configLine.getBoolean(Payline.class.getCanonicalName()
							.concat(".winLine")),
					configLine.getString(Payline.class.getCanonicalName()
							.concat(".path")), configLine.getStringList(
							Payline.class.getCanonicalName().concat(
									".reelPosition")).toArray(
							new String[configLine.getStringList(
									Payline.class.getCanonicalName().concat(
											".reelPosition")).size()]), config);
			payline.setLinePosition(
					configLine.getLong(Payline.class.getCanonicalName().concat(
							".posX")),
					configLine.getLong(Payline.class.getCanonicalName().concat(
							".posY")));
			addActor(payline);
			lines.put(i, payline);
			i++;
		}

	}

	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}

	public void selectPayline(int line) {
		if (lines.get(getLinesPos(line)).isSelection()) {
			for (Entry<Integer, Payline> iterable_element : lines.entrySet()) {
				if (line != iterable_element.getKey()) {
					iterable_element.getValue().setSelection(false);
					iterable_element.getValue().disable();
				} else {
					iterable_element.getValue().enable();
					iterable_element.getValue().setSelection(true);
				}
			}
		} else {
			System.out.println("Pay Line Selection " + line);
			for (Entry<Integer, Payline> iterable_element : lines.entrySet()) {
				if(iterable_element.getValue().isSelection()){
					iterable_element.getValue().enable();
				}
			}
			lines.get(line).enable();
			lines.get(line).setSelection(true);
		}
	}

	public void setWinLinePos(Array<ReelStrip> strips) {
		for (Entry<Integer, Payline> iterable_element : lines.entrySet()) {
			iterable_element.getValue().setPosForLineStrip(strips);
		}
	}

	public int getLinesPos(int line) {
		return line + 1 <= lines.size() ? line + 1 : line;
	}

	public void setDefault() {
		System.out.println("Setting Default in Payline");
		for (Entry<Integer, Payline> iterable_element : lines.entrySet()) {
			if (iterable_element.getValue().isWinLine()) {
				iterable_element.getValue().setWinLine(false);
				// iterable_element.getValue().setDefaultWinHighlights();
				iterable_element.getValue().setWin(null);
			}
		}
	}

	public void setWinLineStripDefault(Barrel barrel) {
		for (Entry<Integer, Payline> iterable_element : lines.entrySet()) {
			iterable_element.getValue().setDefaultWinHighlights(barrel);
		}
	}

	public void setWinLine(Barrel barrel, Paytable paytable) {
		int count=0;
		for (Entry<Integer, Payline> iterable_element : lines.entrySet()) {
			if (iterable_element.getValue().isSelection()) {
				String[] result = new String[iterable_element.getValue()
						.getPosition().length];
				int i = 0;
				for (String position : iterable_element.getValue()
						.getPosition()) {
					String[] temp = position.split(":");
					result[i] = barrel.getStrips().get(
							Integer.parseInt(temp[0])).textures
							.get(returnReelPos(
									barrel.getStrips().get(
											Integer.parseInt(temp[0])).imageStop,
									Integer.parseInt(temp[1]),
									barrel.getStrips().get(
											Integer.parseInt(temp[0])).textures.size)).reelName;
					i++;
				}
				System.out.println(iterable_element.getValue().getName() + " "
						+ Arrays.toString(result));
				iterable_element.getValue().setWin(paytable.getResult(result));
				Win _result=paytable.getResult(result);
				if (_result != null) {
					System.out.println("PayTable Result " + paytable.getResult(result));
					if(_result.getWinType().equals("Progressive")){
						barrel.getSlotMeter().addWinAmount(Jackpots.getAmount(_result.getName()),iterable_element.getValue().getName(),iterable_element.getKey());
						Jackpots.reset(_result.getName());
					}else{
						barrel.getSlotMeter().addWinAmount(
								iterable_element.getValue().getWin().getAmount(),
								iterable_element.getValue().getName(),iterable_element.getKey());
					}
					//ENABLE PAYLINE PER WINLINE
					iterable_element.getValue().enable();
					// Win HighLights Per  Payline
					iterable_element.getValue().winHighlights(
							paytable.getCombinationWin(result),barrel);
					// Enabling Win Highlights
					iterable_element.getValue().setWinLine(true);
					count=iterable_element.getKey();
				}else{
					iterable_element.getValue().disable();
				}
			}
		}
			barrel.payLineSize=count;
		}

	public static int returnReelPos(int imageStop, int reelPos,
			int totalReelSize) {
		return imageStop - reelPos < 0 ? totalReelSize + (imageStop - reelPos)
				: imageStop== totalReelSize && reelPos ==0 ? imageStop - (reelPos+1) : imageStop-reelPos;
	}

	public int getPaylineCount() {
		int count = 0;
		for (Entry<Integer, Payline> iterable_element : lines.entrySet()) {
			if (iterable_element.getValue().isSelection()) {
				count++;
			}
		}
		return count;
	}

	public void addWinLineStrip(Group group) {
		for (Entry<Integer, Payline> entry : lines.entrySet()) {
			if (entry.getValue().isWinLine()) {
				for (WinLine winLine : entry.getValue().getLineStrip()
						.getWinLines()) {

					group.addActor(winLine);
				}
				// addActor(entry.getValue().getLineStrip());
				entry.getValue().getLineStrip().playSound();
			}
		}
	}

	public Map<Integer, Payline> getLines() {
		return lines;
	}

	public void setLines(Map<Integer, Payline> lines) {
		this.lines = lines;
	}
}
