package com.tykhe.client.slot.components;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.tykhe.client.components.sound.Sound;
import com.typesafe.config.Config;
import spine.SpineAnimationCollection;

public class WinLineStrip extends Group {

	private Array<WinLine> winLines;
	private Sound sound;
	public WinLineStrip(Config config) {
		winLines = new Array<WinLine>();
		sound = new Sound(config.getString("sound"));
		for (int i = 0; i < config.getInt("size"); i++) {
			WinLine line = new WinLine(config.getString("path"),
					config.getLong("scale"),config.getBoolean("flag"));
			addActor(line);
			winLines.add(line);
		}
		addActor(sound);
	}


	public void playSound() {
		sound.playSound();
	}

	public void stopSound() {
		sound.stopSound();
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}

	public Array<WinLine> getWinLines() {
		return winLines;
	}

}
