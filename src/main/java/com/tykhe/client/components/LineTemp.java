package com.tykhe.client.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Logger;

public class LineTemp extends Actor {
	final static Logger logger = new Logger(LineTemp.class.getSimpleName());
	ShapeRenderer renderer=null;
	int tmp = 5;
	float length = 5;
	float width;
	float height;
	Color color;
	float lineX;
	float lineY;
	float lineX1;
	float lineY1;
	float hypotenous;
	float radius = 10;
	float angel;
	boolean flag = false;
	boolean lineFlag = true;
	boolean debugDot = true;
	boolean lineDotFlag = true;

	public LineTemp() {
		renderer = new ShapeRenderer();

	}

	public LineTemp(float width, float height, float length) {
		renderer = new ShapeRenderer();
		this.width = width;
		this.height = height;
		this.length = length;
	}

	public LineTemp(float width, float height, float length, Color color) {
		renderer = new ShapeRenderer();
		this.width = width;
		this.height = height;
		this.length = length;
		this.color = color;
	}

	public LineTemp(float x, float y, float width, float height, float length,
			Color color) {
		renderer = new ShapeRenderer();
		this.width = width;
		this.height = height;
		this.length = length;
		this.color = color;
		setPosition(x, y);
	}

	public void setPosition(float x, float y) {
		setX(x);
		setY(y);
	}

	public void setLineTwo(float lineX1, float lineY1) {
		this.lineX1 = lineX1;
		this.lineY1 = lineY1;
	}

	public void setLineConnection(float lineX, float lineY, float hypotenous,
			float angel) {
		this.angel = angel;
		this.lineX = lineX;
		this.lineY = lineY;
		this.hypotenous = hypotenous;
	}

	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		System.out.println("LINETEMP");
		if (!flag) {
			drawRect();
			System.out.print("RECT");
		}
		if (!lineFlag) {
			drawRect(lineX, lineY, hypotenous, length, 0, 0, angel, color);
		}
		if (!debugDot) {
			drawDot(lineX, lineY, Color.RED);
			drawDot(lineX1, lineY1, Color.GREEN);
		}
		if (!lineDotFlag) {
			drawDot(lineX1, lineY1, color);
		}
	}

	public void act(float delta) {

	}

	public void drawRect(float x, float y, float width, float height,
			Color color) {
		renderer.begin(ShapeType.Filled);
		renderer.setColor(color);
		renderer.rect(x, y, width, height);
		renderer.rect(x, y, height, width);
		renderer.rect(x, y + width, width, height);
		renderer.rect(((x + width) - height), y, height, width);

		renderer.end();
	}

	public void drawRect(float x, float y, float width, float height,
			float originX, float originY, float rotation, Color color) {
		renderer.begin(ShapeType.Filled);
		renderer.setColor(color);
//		renderer.rect(x, y, width, height, originX, originY, rotation);
//		renderer.rect(x, y, originX - x, originY - y);
		renderer.end();
	}

	public ShapeRenderer getShapeRender(){
		return  renderer;
	}
	public void drawDot(float x, float y, Color color) {
		renderer.begin(ShapeType.Filled);
		renderer.setColor(color);
		renderer.circle(x, y, radius);
		// renderer.rect(x, y, hypotenous, length, lineX1, lineY1, 0);
		renderer.end();
	}

	public void drawTriangle(float x, float y, float x1, float y1, float x2,
			float y2) {
		renderer.begin(ShapeType.Filled);
		renderer.setColor(color);
		renderer.triangle(x, y, x1, y1, x2, y2);
		renderer.end();
	}

	public void drawRect() {
		float x = getX();
		float y = getY();
		float height1 = length;
		renderer.begin(ShapeType.Filled);
		renderer.setColor(color);
		renderer.rect(x, y, width, height1);
		renderer.rect(x, y, height1, height);
		renderer.rect(x, y + height, width, height1);
		renderer.rect(((x + width) - height1), y, height1, height);
		renderer.end();
	}

	public void drawPayLine() {
		renderer.begin(ShapeType.Filled);
		renderer.setColor(Color.CYAN);
//		renderer.rect(400, 200, length, 5, 0, 0, tmp, Color.RED, Color.RED,
//				Color.RED, Color.RED);

		renderer.end();
		tmp = tmp > 360 ? 5 : (tmp + 5);
		length = length > 360 ? (length - 5) : (length + 5);
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public void setLineFlag(boolean lineFlag) {
		this.lineFlag = lineFlag;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getRadius() {
		return radius;
	}

	public void setLineDotFlag(boolean lineDotFlag) {
		this.lineDotFlag = lineDotFlag;
	}
}
