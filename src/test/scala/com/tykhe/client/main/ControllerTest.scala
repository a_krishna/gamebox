package com.tykhe.client.main

import akka.actor.{Props, ActorSystem}
import com.tykhe.client.controller.Controller
import com.tykhe.client.utils.Initialize


object ControllerTest extends App{
val system = ActorSystem("SLOT-APPLICATION")
  val controller = system.actorOf(Props(new Controller("SLOT")),"controller")
  controller ! Initialize
}
