package com.tykhe.client.main

import java.awt.{Dimension, Toolkit}

import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.tykhe.client.components.buttons.ButtonApp
import com.tykhe.client.test.PropTest

object Main {

  def main(args: Array[String]) {
    val cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration()
    val screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize()
    cfg.title = "PROP TEST";
    cfg.vSyncEnabled = true;
    cfg.width=screenDimension.getWidth.toInt;
    cfg.height=screenDimension.getHeight.toInt;
    // cfg.useGL20 = true;
    cfg.fullscreen = false
    new LwjglApplication(new PropTest(1680,1050), cfg)
  }
}