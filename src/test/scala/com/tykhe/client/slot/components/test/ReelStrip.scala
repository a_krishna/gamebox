package com.tykhe.client.slot.components.test

import java.util.ArrayList

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.typesafe.config.Config

class ReelStrip(config: Config) extends Actor {
  private var reelStrip: ArrayList[Reel] = new ArrayList[Reel]()
  private var yPos: ArrayList[Float] = new ArrayList[Float]()
  var reels = config.getObjectList("reels")
  var min_speed: Float = config.getLong("min_speed")
  var med_speed: Float = config.getLong("med_speed")
  var max_speed: Float = config.getLong("max_speed")
  var actorPadding: Float = config.getLong("actorPadding")
  var velocity, yFirstPos, imageHeight: Float = 0
  var pos, count, size, imageStop, stopCount, displacement: Int = 0
  var flag, reelStop: Boolean = true
  var velocityLock, drawStop: Boolean = false
  setPosition(config.getLong("x"), config.getLong("y"))
  def init(): Unit = {
    var height: Int = getY().toInt
    for (i <- 0 to reels.size() - 1) {
      var reel: Reel = new Reel(reels.get(i).toConfig())
      imageHeight = reel.reelTexture.getHeight()
      if (i == reelStrip.size() - 1) {
        pos = height
      }
      yPos.add(height)
      setBounds(getX(), height, reel.reelTexture.getWidth(), reel.reelTexture.getHeight())
      println("Reel " + reel.reelName + " PosX= " + getX() + " PosY= " + height)
      reelStrip.add(reel)
      height += (reel.reelTexture.getHeight() + actorPadding).toInt
    }
    size = yPos.size()
    displacement = (yPos.get(1) - yPos.get(0)).toInt
    yFirstPos = displacement * 2
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
    if (drawStop) {
      drawStop = false
      boundCorruptPosition(imageStop - 1)
    }
    for (i <- 1 to reels.size()) {
      if (yPos.get(i - 1) < 725 && yPos.get(i - 1) > 50) {
        batch.draw(reelStrip.get(i - 1).reelTexture, getX(), yPos.get(i - 1))
      }
    }
  }
  def decelerate(speed: Int): Unit = {
    med_speed += speed;
    System.out.println(displacement);
    velocity = (displacement / med_speed).toInt
    System.out.println(velocity);
  }
  override def act(delta: Float): Unit = {
    super.act(delta)
    if (!flag) {
      for (i <- 1 to reelStrip.size()) {
        if (yPos.get(i - 1) <= -(imageHeight + actorPadding)) {
          var temp: Float = 0
          if (i == 1) {
            temp = yPos.get(size - 1)
            temp = temp - velocity // med_speed
          } else if (i > 1) {
            temp = yPos.get(i - 2)
          }
          var setY: Float = temp + (imageHeight + actorPadding)
          yPos.set(i - 1, setY)
          if (!reelStop) {
            if (!velocityLock) {
              var temp2: Float = velocity
              velocity = velocity - (if (yPos.get(imageStop - 1) > 0) if (yPos.get(imageStop - 1) > displacement) if (yPos.get(imageStop - 1) < yFirstPos) yFirstPos - yPos.get(imageStop - 1) else 0 else 0 else 0)
              velocity = if (velocity < 0) temp2 else velocity
              velocityLock = if (temp2 == velocity) false else true
              System.out.print("Velocity varience" + velocity
                + "=")
              System.out.println(temp2)
              if (velocity == 0) {
                boundPos(imageStop - 1)
                yPos.toArray().foreach(value => println(value + " "))
              }

            }
            if (yFirstPos == yPos.get(imageStop - 1)) {
              yPos.toArray().foreach(value => println(value + " "))
              System.out.println()
              System.out
                .println("i am in------------------------------")
              boundCorruptPosition(imageStop)
              flag = true
              drawStop = true
            }
          }

        } else if (drawStop) {
          System.out.println("i am in draw")
        } else {
          yPos.set(i - 1, yPos.get(i - 1) - velocity)
        }
      }

    }
  }
  def boundCorruptPosition(stopAt: Int): Unit = {
    val value: Int = stopAt - 3
    System.out.println("value need to fix" + value)
    var temp: Float = yPos.get(size - 1)
    for (i <- 0 to value - 1) {
      temp += displacement
      yPos.set(i, temp)
    }
  }
  def boundPos(stopAt: Int): Unit = {
    var tempSize: Int = size;
    var stopAtPrev = if (stopAt > 0) stopAt - 1 else size - 1
    var stopPrev = if (stopAtPrev > 0) (stopAtPrev - 1)
    else (if (stopAtPrev > 1) (stopAt - 1) else (size - 1))
    yPos.set(stopAt, yFirstPos);
    yPos.set(stopAtPrev, yFirstPos - displacement);
    yPos.set(stopPrev, yFirstPos - yFirstPos);
    var temp: Float = yFirstPos;
    for (i <- stopAt + 1 to size) {
      if (i != stopAt && i != stopAtPrev && i != stopPrev) {
        temp += displacement;
        yPos.set(i, temp);
        if (i == size - 1 | i > size - 1) {
          System.out.println("****i came at reset****");
          tempSize = stopAt - 2;
          for (j <- 0 to tempSize) {
            if (j != stopAt && j != stopAtPrev && j != stopPrev) {
              System.out.println("***********i am inside");
              temp += displacement;
              yPos.set(j, temp);
            }
          }
        }
      }
    }
  }
}