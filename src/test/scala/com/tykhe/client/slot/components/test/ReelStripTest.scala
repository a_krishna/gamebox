package com.tykhe.client.slot.components.test

import java.awt.{Dimension, Toolkit}
import java.util.ArrayList

import com.badlogic.gdx.{ApplicationAdapter, Gdx}
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.Timer
import com.badlogic.gdx.utils.Timer.Task
import com.typesafe.config.{Config, ConfigFactory}

class BarrelTest(config: Config) extends ApplicationAdapter {
  var stage: Stage = null
  var strips: ArrayList[ReelStrip] = new ArrayList[ReelStrip]
  var reelStrip: ReelStrip = null
  var enterKeyPressed: Boolean = false;
  var shiftKeyPressed: Boolean = false;
  var delay, interval: Float = 1f
  var repeatCount = 4;
  var count, reel, drumStop: Int = 0
  var reelDecelerateCount = 1
  var reeltimer: Timer = null;
  var drumTimer: Timer = null;
  override def create() = {
    stage = new Stage
    Gdx.input.setInputProcessor(stage)
    reeltimer = new Timer
    drumTimer = new Timer
    reelStrip = new ReelStrip(config.getConfig("reelStrip"))
    reelStrip.setX(config.getLong("xStrips"))
    reelStrip.stopCount = 1
    reelStrip.init();
    strips.add(reelStrip)
    stage.addActor(reelStrip)
    println("Strips=  " + strips.size())
  }
  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    //    stage.act()
    stage.act(Gdx.graphics.getDeltaTime())
    stage.draw()
    if (Gdx.input.isKeyPressed(Keys.ENTER)) {
      if (!enterKeyPressed) {
        enterKeyPressed = true;
        shiftKeyPressed = false;
        System.out.println("Enter KeyPressed");
        //reelStrip.med_speed = 3
        reelStrip.decelerate(1)
        reelStrip.flag = false
        reelStrip.reelStop = true
        reelStrip.velocityLock = false
      }

    } else if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT)) {
      if (!shiftKeyPressed) {
        shiftKeyPressed = true;
        enterKeyPressed = false;
        System.out.println("Right Shift Pressed");
        var stops = new Array[String](1)
        stops(0) = "2"
        stops.foreach(stop => println(stop))
        updateStop(stops)
      }
    }
  }

  def reelStop(): Unit = {
    System.out.println("i am in reelstop");
    if (reel > 2) {
      drumTimer.stop();
    } else {
      System.out.println("start" + reel);
      defaultSet();
      reeltimer.stop();
      reeltimer.start();
      reel += 1
    }
  }

  def defaultSet(): Unit = {
    reeltimer.scheduleTask(new Task() {

      override def run(): Unit = {
        if (drumStop < 1) {
          System.out.println("count =" + count);
          // if (count > reelDecelerateCount) {
          // strips.get(drumStop).velocityForNormalStop();
          strips.get(drumStop).reelStop = false;
          // reelDecelerateCount--;
          drumStop += 1
          // reeltimer.stop();
          // } else {
          // strips.get(drumStop).decelerate(count++);
          // }
        }
      }
    }, delay);
  }
  def updateStop(stops: Array[String]): Unit = {
    reel = 0;
    drumStop = 0;
    for (i <- 0 to stops.length - 1) {
      strips.get(i).imageStop = Integer.parseInt(stops(i));
    }
    System.out.println("----------");
    drumTimer.scheduleTask(new Task() {

      override def run(): Unit = {
        System.out.println("i am in updateStop");
        reelStop();
      }
    }, delay, interval, repeatCount - 1);
    drumTimer.start();
  }
}

object ReelStripTest {
  def main(args: Array[String]) {
    var appConfig: Config = ConfigFactory.load("slot")
    println(appConfig.toString())
    var cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration();
    var screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize();
    cfg.title = "Buttons";
    cfg.vSyncEnabled = true;
    // cfg.width = screenDimension.width;
    // cfg.height = screenDimension.height;
    // cfg.useGL20 = true;
    if (!cfg.fullscreen) {
      cfg.width = 1280
      cfg.height = 1024
    }
    new LwjglApplication(new BarrelTest(appConfig.getConfig("slot")), cfg)
  }
}