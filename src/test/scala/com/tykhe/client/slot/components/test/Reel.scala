package com.tykhe.client.slot.components.test

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.typesafe.config.Config

class Reel(config: Config) {
  var reelName: String = config.getString("name")
  var reelTexture: Texture = new Texture(Gdx.files.internal(config.getString("reelTexture")))
}