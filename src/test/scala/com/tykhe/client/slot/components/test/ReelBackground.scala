package com.tykhe.client.slot.components.test

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.typesafe.config.Config

class ReelBackground(config: Config) {
  var backgroundTexture: Texture = new Texture(Gdx.files.internal(config.getString("reelBackground")))
}