package com.tykhe.client.test

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.viewport.StretchViewport
import com.badlogic.gdx.{Gdx, ApplicationAdapter}
import com.badlogic.gdx.scenes.scene2d.Stage
import com.tykhe.client.components.buttons.MessagePanel
import com.tykhe.client.utils.Prop

class PropTest(width:Int,height:Int) extends ApplicationAdapter{
  var stage:Stage=null
  var flag =true
  var count=0
  var temp:Array[String]=null
  var msgPanel:MessagePanel=null
  override def create()={
    stage=new Stage(new StretchViewport(width,height),new SpriteBatch())
//    msgPanel=Prop.messagePanel
//    temp=Prop.MESSAGE.drop(1).toArray
    stage.addActor(Prop.pinPanel)
    Gdx.input.setInputProcessor(stage)
  }
  override def render()={
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act(Gdx.graphics.getDeltaTime)
    stage.draw()
    if(Gdx.input.isKeyPressed(Keys.A)){
      flag match {
        case true=>
          flag=false
          msgPanel.addSubMsg(temp(count))
        case false=>
      }
    }else if(Gdx.input.isKeyPressed(Keys.S)){
      flag match {
        case false=>
          flag=true
          count + 1 < temp.size match {
            case true=>count+=1
            case false=>count=0
          }
        case true=>
      }
    }

  }
  override def resize(width:Int,height:Int)={
    stage.getViewport.setScreenSize(width,height)
  }
  override def dispose()={
    stage.dispose()
  }
}
