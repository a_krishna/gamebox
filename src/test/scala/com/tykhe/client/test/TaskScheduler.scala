package com.tykhe.client.test

import com.badlogic.gdx.utils
import com.badlogic.gdx.utils.Timer

/**
 * Created by aravind on 23/1/15.
 */
class TaskScheduler {
 var timer:Timer=null
  var flag:Boolean=false
  def create()={
    timer=Timer.instance()
  }
  def scheduleTask(task:Timer.Task,delay:Float,interval:Float,count:Int): Unit ={
    timer.scheduleTask(task,delay,interval,count)
  }
  def startTask={
    if(!flag){
      timer.start()
    }

  }
  def stopTask={
    if(flag)
      timer.stop()
  }
}
class WinLine(name:String){
  def getName():String=name
}
class WinList{
  val winList:Array[WinLine]=new Array[WinLine](5)
  var count:Int=0
  def addWinLine(winLine: WinLine): Unit ={
    winList.update(count,winLine)
    count+=1
  }
  def getWinLine():WinLine={
    count+=1
    if(count>=winList.length)
      count=winList.length-1
  return winList(count)
  }
}