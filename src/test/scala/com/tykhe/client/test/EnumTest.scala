package com.tykhe.client.test

object EnumTest extends App {

  object DECK extends Enumeration {
    type DECK = Value
    val DEFAULT, OPEN, HOLD = Value
  }

  override def main(args: Array[String]): Unit = {
		  DECK.values filter isWorkingDay foreach println
  }

  import com.tykhe.client.test.EnumTest.DECK._
  def isWorkingDay(d: DECK) = !(d == DEFAULT)
}