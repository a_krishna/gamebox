package com.tykhe.client.test

import com.tykhe.client.poker.components.Card
import com.typesafe.config.{Config, ConfigFactory}

object ConfigTest {
  def getMap(config: Config) = {
    println("Name= " + config.getString("name") + "---" + "Path= " + config.getString("path"))
  }
  def main(arg: Array[String]) {
    var appConfig: Config = ConfigFactory.load("poker.conf")
    var files = appConfig.getObjectList("sample.files")
    var temp = new java.util.ArrayList[Card]()
    val cache = collection.mutable.Map[String, String]()
    for (i <- 0 to files.size() - 1) {
      cache.put(files.get(i).toConfig().getString("name"), files.get(i).toConfig().getString("path"))
    }
    println(cache.size)
    cache.foreach(cach => println(cach.toString))
  }
}
class Temp(config: Config) {
  var name = config.getString("name")
  var path = config.getString("path")
  override def toString(): String = {
    return "[Name= " + name + " Path= " + path + " ]"
  }
}