package com.tykhe.client.test

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor

class ImageLoader(filePath: String) extends Actor {
  var texture: Texture = new Texture(Gdx.files.internal(filePath))

  override def draw(batch: Batch, parentAlpha: Float): Unit = batch.draw(texture, getX(), getY())
}