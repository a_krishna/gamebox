package com.tykhe.client.test

import java.awt.{Toolkit, Dimension}
import javax.swing.Renderer

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.utils.Timer.Task
import com.badlogic.gdx.{Gdx, ApplicationAdapter}
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.scenes.scene2d.{Group, Stage}
import com.tykhe.client.components.buttons.ButtonApp

/**
 * Created by aravind on 21/1/15.
 */
object ExamineClick {

  def main(args: Array[String]) {
    var cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration();
    var screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize();
    cfg.title = "Buttons";
    cfg.vSyncEnabled = true;
    // cfg.width = screenDimension.width;
    // cfg.height = screenDimension.height;
    // cfg.useGL20 = true;
    cfg.fullscreen = false;
    cfg.width = 800;
    cfg.height = 600;
    new LwjglApplication(new Click(), cfg)
  }
}
class TextureLine extends  Group{
  var clickTest=new ClickTest()
  val timerTask=new TaskScheduler()
  val winList=new WinList()


  def create()={
    clickTest.addTexture()
    clickTest.setBounds(50,200,clickTest.texture.getWidth,clickTest.texture.getHeight)
    clickTest.setListener()
    addActor(clickTest)
    winList.addWinLine(new WinLine("PROGRESSIVE"))
    winList.addWinLine(new WinLine("LINE"))
    winList.addWinLine(new WinLine("BONUS"))
    winList.addWinLine(new WinLine("LINE"))
    winList.addWinLine(new WinLine("BONUS"))
    timerTask.create()
    winList.count-=1
    timerTask.scheduleTask(new Task {
      override def run(): Unit = {
        println("TASK started")
        val winLine:WinLine=winList.getWinLine()
        if(winLine.getName().equals("BONUS")){
          println(winLine.getName()+" TO STOP TASK")
          timerTask.flag=true
          timerTask.stopTask
        }else{
          println(winLine.getName()+" TO START TASK")
          if(timerTask.flag){
            timerTask.flag=false
            timerTask.startTask
          }
        }
      }
    },1f,2f,winList.winList.length)
    timerTask.startTask
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch,parentAlpha)
  }

}
class Click extends ApplicationAdapter{
  val GL_COLOR_BUFFER_BIT = 0x00004000;
  var stage:Stage=null
  var textLine:TextureLine=null
  override def create()={
   stage=new Stage()
    Gdx.input.setInputProcessor(stage)
   textLine=new TextureLine()
    textLine.create()
    stage.addActor(textLine)
  }
  override def render()={
    Gdx.gl.glClearColor(0, 0, 0, 1)
    Gdx.gl.glClear(GL_COLOR_BUFFER_BIT)
    stage.act()
    stage.draw()
  }

}