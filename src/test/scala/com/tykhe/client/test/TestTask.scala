package com.tykhe.client.test

import com.badlogic.gdx.utils
import com.badlogic.gdx.utils.Timer
import com.badlogic.gdx.utils.Timer.Task

/**
 * Created by aravind on 23/1/15.
 */
object TestTask {

  def main(args: Array[String]) {
    val timerTask=new TaskScheduler()
    val winList=new WinList()
    winList.addWinLine(new WinLine("PROGRESSIVE"))
    winList.addWinLine(new WinLine("LINE"))
    winList.addWinLine(new WinLine("BONUS"))
    winList.addWinLine(new WinLine("LINE"))
    winList.addWinLine(new WinLine("BONUS"))

    timerTask.create()
    winList.count-=1
    timerTask.scheduleTask(new Task {
      override def run(): Unit = {
        val winLine=winList.getWinLine()
        if(winLine.getName().equals("BONUS")){
          timerTask.flag=true
          timerTask.stopTask
        }else{
          println(winLine.getName())
          if(timerTask.flag){
            timerTask.startTask
          }
        }
      }
    },10,10,winList.winList.length)
  }
}
