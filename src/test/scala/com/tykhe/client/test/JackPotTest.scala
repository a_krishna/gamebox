package com.tykhe.client.test

import com.tykhe.client.jackpot.{TotalBet, LoadJP}
import com.typesafe.config.ConfigFactory
import org.tykhe.slot.paytable.Paytable

object JackPotTest extends App{
  val paytable=new Paytable(ConfigFactory.load("paytable"))
  com.tykhe.client.jackpot.Jackpots.defaultJPvalue=paytable.wins.filter(_.typeOfWin=="Progressive")
  com.tykhe.client.jackpot.Jackpots.jps=LoadJP._load(ConfigFactory.load("jackpots.conf"))
  com.tykhe.client.jackpot.Jackpots.jps.foreach(println)
  com.tykhe.client.jackpot.Jackpots.loadPJP()
  TotalBet.setBet(10,1,1)
  com.tykhe.client.jackpot.Jackpots.update()
  com.tykhe.client.jackpot.Jackpots.progrssiveJackpots.foreach(println)
  TotalBet.setBet(20,5,1)
  com.tykhe.client.jackpot.Jackpots.update()
  com.tykhe.client.jackpot.Jackpots.progrssiveJackpots.foreach(println)
  TotalBet.setBet(50,5,1)
  com.tykhe.client.jackpot.Jackpots.update()
  com.tykhe.client.jackpot.Jackpots.progrssiveJackpots.foreach(println)
  TotalBet.setBet(100,5,1)
  com.tykhe.client.jackpot.Jackpots.update()
  com.tykhe.client.jackpot.Jackpots.progrssiveJackpots.foreach(println)
  TotalBet.setBet(200,5,5)
  com.tykhe.client.jackpot.Jackpots.update()
  com.tykhe.client.jackpot.Jackpots.progrssiveJackpots.foreach(println)
  TotalBet.setBet(100,5,3)
  com.tykhe.client.jackpot.Jackpots.update()
  com.tykhe.client.jackpot.Jackpots.progrssiveJackpots.foreach(println)

  println("TreasureChest "+"%2f".format(com.tykhe.client.jackpot.Jackpots.progrssiveJackpots("TreasureChest") * 500* 5).toDouble)
  com.tykhe.client.jackpot.Jackpots.reset("TreasureChest")
  println("King "+"%2f".format(com.tykhe.client.jackpot.Jackpots.progrssiveJackpots("King") * 500*5).toDouble)
  com.tykhe.client.jackpot.Jackpots.reset("King")
  println("Queen "+"%2f".format(com.tykhe.client.jackpot.Jackpots.progrssiveJackpots("Queen") * 500*5).toDouble)
  com.tykhe.client.jackpot.Jackpots.reset("Queen")
  println("Jack "+"%2f".format(com.tykhe.client.jackpot.Jackpots.progrssiveJackpots("JACK") * 500*5).toDouble)
  com.tykhe.client.jackpot.Jackpots.reset("JACK")
  com.tykhe.client.jackpot.Jackpots.progrssiveJackpots.foreach(println)


}
