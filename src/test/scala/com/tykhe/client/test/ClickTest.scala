package com.tykhe.client.test

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.graphics.{Color, Texture}
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{Stage, InputListener, Group, InputEvent}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.tykhe.client.components.LineTemp

class ClickTest extends Group {

  var texture: Texture = null

  def addTexture()={
    texture=new Texture(Gdx.files.internal("denom/denomfour.png"))
//    setBounds(getX, getY, texture.getWidth-50, texture.getHeight-50)
  }
  def setMouseOverListener() ={
    addListener(new InputListener{
     override def mouseMoved(event:InputEvent,x:Float,y:Float):Boolean={
       println("X  ="+x+" Y   ="+y)
       return true
     }

    })
  }

  def setListener() {
    addListener(new ClickListener {
      override def clicked(event: InputEvent, x: Float, y: Float): Unit = println("clicked X="+x+" Y="+y)
    })
  }
  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch,parentAlpha)
    batch.draw(texture, getX(), getY())
  }



}