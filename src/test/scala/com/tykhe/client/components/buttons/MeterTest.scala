package com.tykhe.client.components.buttons

import java.awt.{Toolkit, Dimension}
import java.lang.Float
import java.util

import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.Color
import com.typesafe.config.{ConfigFactory}

import scala.collection.immutable.Seq


object MeterTest {
  def main(args: Array[String]) {
    var cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration();
    var screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize();
    val appConfig=ConfigFactory.load("meter.conf")
    cfg.title = "Buttons";
    val num="(\\d{0,9})".r
    cfg.vSyncEnabled = true;
    cfg.fullscreen = false;
    cfg.width = 800;
    cfg.height = 600;
    val meters=new MeterApp(appConfig)
    new LwjglApplication(meters, cfg)
    while(true){
      Console.readLine() match {
        case "4"=>
          (1000 to 9999 ).toArray[Int].foreach(i=>meters.meter.addAmount(i))
        case "2"=>
          (10 to 99 ).toArray[Int].foreach(i=>meters.meter.addAmount(i))
        case "1"=>
          (100 to 999 ).toArray[Int].foreach(i=>meters.meter.addAmount(i))
        case "7"=>
          (1000000 to 9999999 ).toArray[Int].foreach(i=>meters.meter.addAmount(i))
        case "5"=>
          (10000 to 99999 ).toArray[Int].foreach(i=>meters.meter.addAmount(i))
        case _=> System.exit(0)
      }
    }
  }


}
