package com.tykhe.client.components.buttons

import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.scenes.scene2d
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.{Gdx, ApplicationAdapter}
import com.badlogic.gdx.graphics.{GL20, Color}
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{ui, Stage, Group}
import com.badlogic.gdx.scenes.scene2d.ui.{Skin, Label}
import com.typesafe.config.Config
import sun.font.FreetypeFontScaler

/**
 * Created by aravind on 17/3/15.
 */

class Meter(config:Config,labelStyle: LabelStyle) extends Group {
  val labels=new Array[Label](config.getInt("meter.size"))
  var amount=0;
  val charArray=Array[Char](1)

  def prepareMeter(): Unit = {
    var sumX=config.getInt("meter.x")
    println(config.getDouble("meter.fontSize"))
    println(config.getLong("meter.fontSize")/2.0f)
    for(i<- 0 to labels.length-1) {
      charArray(0)=getChar(0)
      val label = new Label(charArray, new Skin(Gdx.files.internal(config.getString("meter.skin"))))
      label.setStyle(labelStyle)
      label.setPosition(sumX, config.getInt("meter.y"))
//      label.setFontScale(config.getLong("meter.fontSize"))
      label.setColor(1,1,0.1f,0.1f)
      label.setText(charArray)
      labels.update(i, label)
      addActor(label)
      sumX += config.getInt("meter.padX")
    }
  }


  def addAmount( amt:Int): Unit = {
    amount+=amt
      var denom=amount
      labels.reverseIterator.foreach {
        label =>
          denom == 0 match {
            case true => label.setColor(1, 1, 0.1f, 0.1f)
            case false => label.setColor(Color.WHITE)
          }
          charArray(0)=getChar(denom % 10)
          label.setText(charArray)
          denom /= 10
      }
  }
  override def draw(batch:Batch,parentAlpha:Float)=super.draw(batch,parentAlpha)
  def getChar(num:Int):Char={
    num match {
      case 0=>'0'
      case 1=>'1'
      case 2=>'2'
      case 3=>'3'
      case 4=>'4'
      case 5=>'5'
      case 6=>'6'
      case 7=>'7'
      case 8=>'8'
      case 9=>'9'

    }
  }

}
class MeterApp(config: Config) extends  ApplicationAdapter{
  val GL_COLOR_BUFFER_BIT = 0x00004000;
  var stage:Stage=null
   var meter:Meter=null
  var reelTestUpdate:ReelTestUpdate=null
  override  def create(): Unit ={
    stage=new Stage()
    Gdx.input.setInputProcessor(stage)
    val fontParameter=new FreeTypeFontParameter()
    val fontGenerator=new FreeTypeFontGenerator(Gdx.files.internal("digital/digital-7 (mono).ttf"))
    fontParameter.size=70
    val labelStyle=new LabelStyle(fontGenerator.generateFont(fontParameter),Color.RED)
    fontGenerator.dispose()
    meter=new Meter(config,labelStyle)
    meter.prepareMeter()
    reelTestUpdate=new ReelTestUpdate(new Skin(Gdx.files.internal("ui/uiskin.json")),config.getConfig("meter.reels"));
    stage.addActor(meter)
    stage.addActor(reelTestUpdate)
  }
  override  def render():Unit={ Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act()
    stage.act(Gdx.graphics.getDeltaTime())
    stage.draw()
  }


}