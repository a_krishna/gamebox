package com.tykhe.client.components.buttons

import com.badlogic.gdx.{ApplicationListener, Gdx}
import com.badlogic.gdx.scenes.scene2d.Stage
import com.tykhe.client.components.meter.CreditMeter
import com.tykhe.client.test.{ClickTest, ImageLoader}

class ButtonApp extends ApplicationListener {
  val GL_COLOR_BUFFER_BIT = 0x00004000;
  var stage: Stage = null
  var slotButton: SlotButton = null
  var creditMeter: CreditMeter = null

  def create() {
    stage = new Stage
    Gdx.input.setInputProcessor(stage)
    var files = List("assets/buttons/coin1.png", "assets/buttons/coin2.png", "assets/buttons/coin3.png", "assets/buttons/coin4.png", "assets/buttons/coin5.png")
    var files1 = List("meter/cash-a.png", "meter/cash-b.png")
    creditMeter = new CreditMeter
    creditMeter.setPosition(0, 0, 128, 128)
    creditMeter.init(files1)
    slotButton = new SlotButton
    slotButton.setPadding(200)
    slotButton.setPosition(0, 0, 256, 256)
    slotButton.init(files)
    stage.addActor(slotButton)
    //    stage.addActor(creditMeter)
    val clickTest: ClickTest = new ClickTest
    clickTest.setListener
    stage.addActor(clickTest)

  }
  def addTest(file: String) = {
    stage.addActor(new ImageLoader(file))
  }

  def dispose() {}
  def pause() {}

  def render() {
    Gdx.gl.glClearColor(0, 0, 0, 1)
    Gdx.gl.glClear(GL_COLOR_BUFFER_BIT)
    stage.act()
    stage.draw()
  }

  def resize(arg0: Int, arg1: Int) {}

  def resume() {}
}