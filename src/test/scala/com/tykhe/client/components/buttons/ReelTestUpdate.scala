package com.tykhe.client.components.buttons

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.utils

import collection.JavaConversions._
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.{InputEvent, ui, Actor, Group}
import com.badlogic.gdx.scenes.scene2d.ui.{Button, Skin, SelectBox}
import com.badlogic.gdx.scenes.scene2d.utils.{ClickListener, ChangeListener}
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent
import com.typesafe.config.Config

/**
 * Created by aravind on 25/3/15.
 */
class ReelStopBox(skin:Skin,config:Config) extends Group{
  val sb=new SelectBox[String](skin);
  val datas:utils.Array[String]=new utils.Array[String]()
  for(i <- 0 to config.getStringList("data").size()-1){
    datas.add(config.getStringList("data").get(i))
  }
  sb.setItems(datas)
  var reelName=sb.getItems.get(0)
  config.getStringList("data")
  sb.setSelected(sb.getItems.get(0))
  sb.setBounds(config.getLong("x"),config.getLong("y"),config.getLong("width"),config.getLong("height"))
  addActor(sb)
  sb.addListener(new ChangeListener() {
    override def changed(event: ChangeEvent, actor: Actor) ={
      reelName=sb.getSelected
    }
  })
  override def draw(batch:Batch,parentAlpha:Float)=super.draw(batch,parentAlpha)
}
class ReelTestUpdate(skin: Skin,config:Config) extends Group{
  val reelsStopBox:Array[ReelStopBox]=new Array[ReelStopBox](config.getInt("size"))
  val postButton:Button=new ui.Button(skin)
  var reelStopName=""
  postButton.add(config.getString("button.name"))
  postButton.setBounds(config.getLong("button.x"),config.getLong("button.y"),config.getLong("button.width"),config.getLong("button.height"))
  for( i <- 0 to reelsStopBox.size-1){
    reelsStopBox(i)=new ReelStopBox(skin,config.getConfig("reel"+i))
    addActor(reelsStopBox(i))
  }
  addActor(postButton)
  postButton.addListener(new ClickListener(){
    override def clicked(event:InputEvent,x:Float,y:Float): Unit ={
      reelsStopBox.foreach(reels=>reelStopName+=reels.reelName+",")
      println(reelStopName.dropRight(1))
      reelStopName=""
    }
  })
  override def draw(batch:Batch,parentAlpha:Float)=super.draw(batch,parentAlpha)
}
