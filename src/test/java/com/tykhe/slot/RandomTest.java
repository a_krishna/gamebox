package com.tykhe.slot;

import com.tykhe.client.logger.CustomTrace;

import java.util.Random;

public class RandomTest {
	public static  String[] generateRandom() {
		String[] stops = new String[3];
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		for (int i = 0; i < 3; i++) {
			Integer randomNum = 0;
			randomNum = rand.nextInt((13 - 1) + 1) + 1;
			stops[i] = randomNum.toString();
		}
		return stops;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			CustomTrace.traceArrayString(generateRandom());
		}
	}
}
