package com.tykhe.slot;

public class Test {

	public static int[] a={0,0,0,1,1,1,1,1,1,0,0,0,0,0,1,1,0,1};
	public static int[] b={0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0};
	public static int returnReelPos(int imageStop, int reelPos,
			int totalReelSize) {
		return imageStop - reelPos < 0 ? totalReelSize + (imageStop - reelPos)
				: imageStop - reelPos;
	}

	public static int imageReelPos(int imageStop, int reelPos,
			int totalReelSize) {
		return imageStop + reelPos >= totalReelSize ? totalReelSize - (imageStop + reelPos)
				: imageStop + reelPos;
	}
	public static void main(String[] args) {
		//int a = 14;
		//int b = 3;
//		for (int i = 0; i < a; i++) {
//			for (int j = 0; j < b; j++) {
//				System.out.println(i + "----" + j + "------"
//						+ imageReelPos(i, j, a));
//			}
//		}
		System.out.print(intArrayBin(7, 0, a)+" ");
		System.out.print(Integer.toHexString(binToHex(intArrayBin(7, 0, a))) + " ");
		printArray(decToBinary(binToHex(intArrayBin(7, 0, a))));
		System.out.print(intArrayBin(15, 8, a)+" ");
		System.out.print(Integer.toHexString(binToHex(intArrayBin(15, 8, a)))+" ");
		printArray(decToBinary(binToHex(intArrayBin(15, 8, a))));
		System.out.print(intArrayBin(7, 0, b)+" ");
		System.out.print(Integer.toHexString(binToHex(intArrayBin(7, 0, b)))+" ");
		printArray(decToBinary(binToHex(intArrayBin(7, 0, b))));
		System.out.print(intArrayBin(15, 8, b)+" ");
		System.out.print(Integer.toHexString(binToHex(intArrayBin(15, 8, b)))+" ");
		printArray(decToBinary(binToHex(intArrayBin(15, 8, b))));
	}

	public static int intArrayBin(int startBit,int endBit,int[] array){
		int sum=0;
		int count=1;
		for(int i=startBit;i>=endBit;i--){
			sum+=(array[i]*count);
			count*=10;
		}
		return sum;
	}
	public static int binToHex(int binNum){
		 int hexaDec=0,remainder,j=1;
		while (binNum!=0){
			remainder=binNum%10;
			hexaDec=hexaDec+remainder*j;
			j*=2;
			binNum/=10;
		}
		return hexaDec;
	}
	public static int[] decToBinary(int dec){
		int[] binary=new int[8];
		int i=7,quotient=dec;
		while(i>=0 && quotient!=0){
			binary[i--]=quotient%2;
			quotient/=2;
		}
		return binary;
	}
	public static void printArray(int[] array){
 	   for(int i=0;i<array.length;i++){
			System.out.print(array[i]);
	   }
		System.out.println();
	}

}
