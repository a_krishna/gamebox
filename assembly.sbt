import sbtassembly.Plugin.AssemblyKeys._

Project.inConfig(Compile)(baseAssemblySettings)

mainClass in (Compile, assembly) := Some("com.tykhe.client.main.MainGameActor")

jarName in (Compile, assembly) := s"${name.value}-${version.value}-dist.jar"

mergeStrategy in (Compile, assembly) <<= (mergeStrategy in (Compile, assembly)) {
  (old) => {
    case PathList(ps @ _*) if ps.last endsWith ".html" =>MergeStrategy.first
    case "META-INF/MANIFEST.MF" => MergeStrategy.discard
    case "poker.conf" => MergeStrategy.discard
    case "paytable.conf" => MergeStrategy.discard
    case "fruit_slot.conf" => MergeStrategy.discard
    case "roulette.conf" => MergeStrategy.discard
    case "slot.conf" => MergeStrategy.discard
    case x => old(x)
  }
}
