name  :=  "client-components"

version  :=  "3.2"

scalaVersion  :=  "2.11.6"


libraryDependencies ++= Seq(
  "commons-collections" % "commons-collections" % "3.2.1",
  "com.typesafe" % "config" % "1.2.0",
  "com.tykhe.random" % "random-core" % "1.0.0",
  "com.typesafe.akka" % "akka-actor_2.11" % "2.4-M2",
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.4-M2",
  "io.spray" % "spray-client_2.11" % "1.3.2",
  "io.spray" % "spray-json_2.11" % "1.3.2",
  "com.tykhe.nemesis" %% "device-crt" % "1.0.0-SNAPSHOT",
  "com.tykhe.nemesis" %% "device-usb" % "1.0.0-SNAPSHOT",
  "com.esotericsoftware.spine" % "spine-libgdx" % "1.0-SNAPSHOT",
  "org.tykhe.poker" %% "poker-core" % "1.1",
  "org.tykhe.slot"  %% "slot-core" % "1.0",
  "com.badlogicgames.gdx" % "gdx-freetype" % "1.6.4",
  "com.badlogicgames.gdx" % "gdx-freetype-platform" % "1.6.4" classifier "natives-desktop",
  "org.testng" % "testng" % "6.1.1" % "test"
)

//"org.usb4java"%"usb4java"%"1.2.0","org.scream3r" % "jssc" % "2.8.0",
